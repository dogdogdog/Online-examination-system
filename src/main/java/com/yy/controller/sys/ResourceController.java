package com.yy.controller.sys;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yy.entity.base.Result;
import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Resource;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.ResourceService;


/**
 * 菜单管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/resource")
public class ResourceController {
	@Autowired
	private ResourceService resourceService;
	/**
	* 菜单管理首页
	* @return
	*/
	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/system/resource/index";
	}
	//获取表格数据
	@RequestMapping("/list")
	@ResponseBody
	public ResultTable<Resource> List(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String id = request.getParameter("id");
		String username = request.getParameter("username");
		ResultTable<Resource> rt = resourceService.getLists(page, limit, id,username);
		return rt;
	}
	//新增
	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response){
		String code=request.getParameter("code");
		if (code.equals("page")) {
			return "/system/resource/add";
		} else if (code.equals("add")) {
			String f = request.getParameter("form");
			int j = resourceService.setInsert(f);
//			Result<Resource> rl = new Result<Resource>();
			if (j>0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
//			try {
//				response.setContentType("text/html; charset=utf-8");
//				response.getWriter().print(JSON.toJSONString(rl));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		return null;
	}
	/**
	 * 根据选中的节点获取节点下的内容
	 * 
	 * @param request
	 * @param response
	 * @author liyingying
	 */
	@RequestMapping("/tree")
	public String tree(HttpServletRequest request, HttpServletResponse response) {
		String tree = request.getParameter("tree");
		try {
			if (tree.equals("dyc")) {
				String id = request.getParameter("id");
				String name = request.getParameter("name");
				String pid = request.getParameter("pid");
				String j = resourceService.tree(id, name, pid);
				response.setContentType("text/html; charset=utf-8");
				response.getWriter().print(j);
			}
			if (tree.equals("edit")) {
				String j = resourceService.tree("", "", "");
				response.setContentType("text/html; charset=utf-8");
				response.getWriter().print(j);
			}
			if (tree.equals("treepage")) {
				return "/system/resource/tree";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @author liyingying
	 */
	@RequestMapping("/edit")
	public String update(Map<String, Object> map,HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String update = request.getParameter("update");
		if (update.equals("page")) {
			String id = request.getParameter("id"); // 取得ID
			Resource list = resourceService.findById(id);
//			for (Resource l : list) {
//				session.setAttribute("icon", l.getIcon());
//				break;
//			}
			map.put("u", list);
			return "/system/resource/edit";
		} else if (update.equals("edit")) {
			String f = request.getParameter("form");
			int j = resourceService.getUpdate(f);
//			Result<Resource> rl = new Result<Resource>();
			if (j >0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
//			try {
//				response.setContentType("text/html; charset=utf-8");
//				response.getWriter().print(JSON.toJSONString(rl));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		return null;
	}
	/**
	* 删除
	* @return
	*/
	@RequestMapping(value = "/del")
	public void del(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String code = request.getParameter("code");
		if (code.equals("del")) {
			String ids = request.getParameter("id");
			int r = resourceService.getDictDelete(ids);
			if (r >0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		} else if (code.equals("dels")) {

		}
	}
}
