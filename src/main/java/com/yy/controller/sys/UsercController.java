package com.yy.controller.sys;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Users;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.RoleService;
import com.yy.service.sys.UserService;

@Controller
@RequestMapping("user")
public class UsercController {
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;

	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/system/user/index";
	}

	// 获取表格数据
	@RequestMapping("/list")
	@ResponseBody
	public ResultTable<Users> List(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String id = request.getParameter("id");
		String username = request.getParameter("username");
		ResultTable<Users> rt = userService.getLists(page, limit, id, username);
		return rt;
	}

	// 新增
	@RequestMapping("/add")
	public String add(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("page")) {
			String id = request.getParameter("id");
			map.put("id", id);
			return "/system/user/add";
		} else if (code.equals("add")) {
			String data = request.getParameter("data");
			int j = userService.setInsert(data);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
		return null;
	}

	/**
	 * 修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @author liyingying
	 */
	@RequestMapping("/edit")
	public String update(Map<String, Object> map, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) {
		String update = request.getParameter("update");
		if (update.equals("page")) {
			String id = request.getParameter("id"); // 取得ID
			Users list = userService.findById(id);
			map.put("u", list);
			return "/system/user/edit";
		} else if (update.equals("edit")) {
			String f = request.getParameter("form");
			int j = userService.getUpdate(f);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		} else if (update.equals("editpass")) {
			String id = request.getParameter("id");
			String f = request.getParameter("form");
			int j = userService.getUpdatepass(id, f);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		}
		return null;
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	@RequestMapping(value = "/del")
	public void del(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String code = request.getParameter("code");
		if (code.equals("del")) {

		} else if (code.equals("dels")) {
			String ids = request.getParameter("id");
			int r = userService.getDictDelete(ids);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}

	// 用户授权
	@RequestMapping("/grant")
	public String grant(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String sq = request.getParameter("sq"); // 取得代码
		// 根据当前登录的工厂代码查询角色id 和名称
		if (sq.equals("qxsz")) {
			String s = "";
			String jsonlist = roleService.listjs(s);
			response.setContentType("text/html; charset=utf-8");
			response.getWriter().print(jsonlist);
		} else if (sq.equals("qrsz")) {
			String userid = request.getParameter("uid"); // 取得用户id
			String roleid = request.getParameter("rid"); // 取得角色id
			String uname = request.getParameter("uname"); // 取得角色名称
			// String userid1 = userid.substring(1, userid.length() - 1);
			int list = userService.execute(userid, roleid, uname);
			if (list > 0) {
				throw new YyException(ResultEnum.sucess_grant_failed);
			} else {
				throw new YyException(ResultEnum.error_grant_upload);
			}
		}
		return null;
	}
}
