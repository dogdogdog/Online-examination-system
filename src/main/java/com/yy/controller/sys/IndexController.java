package com.yy.controller.sys;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.yy.entity.base.Result;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.sys.Users;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.ResourceService;
import com.yy.service.sys.UserService;
import com.yy.utils.GlobalConstant;

@Controller
public class IndexController {
	private Producer captchaProducer = null;
	@Autowired
	private UserService userService;
	@Autowired
	private ResourceService resourceService;

	@Autowired
	public void setCaptchaProducer(Producer captchaProducer) {
		this.captchaProducer = captchaProducer;
	}

	/**
	 * 用户登录界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login")
	public String login(Map<String, Object> map, HttpServletRequest request) {
		return "/login";
	}

	/**
	 * 后台首页界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/main")
	public String main(Map<String, Object> map, HttpServletRequest request) {
		return "/index/main";
	}

	/**
	 * 用户登录请求
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/SignIn")
	@ResponseBody
	public Result<Users> SignIn(HttpServletRequest request, HttpServletResponse response) {
		// 从session中取出servlet生成的验证码text值
		String kaptchaExpected = (String) request.getSession()
				.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
		String name = request.getParameter("r");
		// String username = request.getParameter("username");
		// String password = request.getParameter("password");
		// String code = request.getParameter("code");
		Result<Users> s = userService.signin(kaptchaExpected, name, request);
		// Result<Users> s= userService.signin(kaptchaExpected, name,
		// request,username,password,code);
		return s;
	}

	/**
	 * 首页界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String index(HttpServletRequest request, Map<String, Object> map) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		if (sessionInfo == null) {
			return "/login";
		}
		if (!sessionInfo.getPermissions().equals("1")) {
			return "/login";
		}
		map.put("username", sessionInfo.getUsername());
		return "index/index";
	}

	/**
	 * session
	 * 
	 * @return
	 */
	@RequestMapping(value = "/session")
	public void session(HttpServletRequest request, Map<String, Object> map) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		if (sessionInfo == null) {
			throw new YyException(ResultEnum.no_login);
		}
		map.put("username", sessionInfo.getUsername());
		// return "reception/test/test";
		throw new YyException(ResultEnum.SUCCESS);
	}

	/**
	 * 首页界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/llogin")
	public String llogin(HttpServletRequest request, Map<String, Object> map) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		if (sessionInfo == null) {
			return "/login";
		}
		map.put("username", sessionInfo.getUsername());
		return "reception/test/test";
	}

	/**
	 * 退出(并清空session)
	 * 
	 * @return
	 */
	@RequestMapping(value = "/logout")
	public String Signout(HttpServletRequest request, HttpServletResponse response) {
		// 退出
		// SecurityUtils.getSubject().logout();
		HttpSession session = request.getSession();
		session.invalidate();
		return "/login";
	}

	/**
	 * 验证码
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 * @author liyingying
	 */
	@RequestMapping("/kaptcha.jpg")
	public void kaptcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("image/jpeg");
		String capText = captchaProducer.createText();
		request.getSession().setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
		BufferedImage bi = captchaProducer.createImage(capText);
		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(bi, "jpg", out);
		try {
			out.flush();
		} finally {
			out.close();
		}
	}

	/*
	 * 获取菜单内容
	 */
	@RequestMapping("/tree")
	public void tree(HttpServletRequest request, HttpServletResponse response) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		String v = resourceService.list(sessionInfo);
		response.setContentType("text/html; charset=utf-8");
		// System.out.println(v);
		try {
			response.getWriter().print(v);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param session
	 * @param key
	 * @return 返回查询的session结果
	 * @author liyingying
	 */
	@ResponseBody
	@RequestMapping("/getredis")
	public String getredis(HttpSession session, @RequestParam("key") String key) {
		String value = (String) session.getAttribute(key);
		if (value == null || "".equals(value)) {
			return "NO VALUE SESSION";
		}
		return value;
	}

}
