package com.yy.controller.sys;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.Result;
import com.yy.entity.base.ZtreeNode;
import com.yy.entity.sys.Organization;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.OrganizationService;

/**
 * 角色管理
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/organization")
public class OrganizationController {

	@Autowired
	private OrganizationService organizationService;

	/**
	 * 角色管理首页
	 * 
	 * @return
	 */
	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/system/organization/index";
	}

	/**
	 * 获取组织机构表的树形
	 * 
	 * @param request
	 * @param response
	 * @param tree
	 * @return
	 */
	@RequestMapping("/ztree")
	@ResponseBody
	public List<ZtreeNode> ztree(HttpServletRequest request, HttpServletResponse response, ZtreeNode tree) {
		String id = request.getParameter("id");
		String t = request.getParameter("t");
		String pid = request.getParameter("pId");
		List<ZtreeNode> treeList = organizationService.ztree(id, t, pid);
		
		return treeList;
	}

	@RequestMapping("/add")
	public void add(HttpServletRequest request, HttpServletResponse response) {
		// Result<Organization> l=new Result<Organization>();
		String code = request.getParameter("code");
		if (code.equals("add")) {
			String form = request.getParameter("form");
			int r = organizationService.add(form);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
	}
	/**
	 * 删除
	 * @param request
	 * @param response
	 */
	@RequestMapping("/del")
	public void del(HttpServletRequest request, HttpServletResponse response) {
		// Result<Organization> l=new Result<Organization>();
		String code = request.getParameter("code");
		if (code.equals("del")) {
			String id = request.getParameter("id");
			int r = organizationService.del(id);
			if (r >0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}
	/**
	 * 删除
	 * @param request
	 * @param response
	 */
	@RequestMapping("/edit")
	public String edit(Map<String, Object> map,HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("edit")) {
			String id = request.getParameter("id");
			int r = organizationService.edit(id);
			if (r >0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		}else if (code.equals("page")) {
			String id = request.getParameter("id");
			Organization l=organizationService.editpage(id);
			map.put("o", l);
			return "/system/organization/edit";
		}
		return null;
	}
}
