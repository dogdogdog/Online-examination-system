package com.yy.controller.sys;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Notice;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.NoticeService;

@Controller
@RequestMapping("/notice")
public class NoticeController {

	@Autowired
	private NoticeService noticeService;

	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/system/Notice/index";
	}

	// 读取数据库内容
	@GetMapping(value = "/list")
	@ResponseBody
	public ResultTable<Notice> List(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");
		ResultTable<Notice> l = noticeService.getAll(page, limit, f, request);
		return l;
	}

	@GetMapping(value = "/addpage")
	public String addpage(Map<String, Object> map, HttpServletRequest request) {
		return "/system/Notice/add";
	}

	@RequestMapping("/add")
	public void add(HttpServletRequest request, HttpServletResponse response) {
		String form = request.getParameter("form");
		String fbnr = request.getParameter("fbnr");
		int r = noticeService.add(form, fbnr);
		if (r > 0) {
			throw new YyException(ResultEnum.sucess_create_failed);
		} else {
			throw new YyException(ResultEnum.error_create_failed);
		}
	}
	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/del")
	public void del(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("dels")) {
			String id = request.getParameter("id");
			int r = noticeService.del(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}
	//读取公告信息
	@GetMapping(value = "/listgg")
	@ResponseBody
	public ResultTable<Notice> listgg(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String code = request.getParameter("code");
		ResultTable<Notice> l = noticeService.getAllGg(code, request);
		return l;
	}
	
	//修改状态信息
	@RequestMapping("/codeedit")
	public void codeedit(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String code = request.getParameter("code");
		int r = noticeService.codeedit(id,name, code);
		if (r > 0) {
			throw new YyException(ResultEnum.sucess_update_failed);
		} else {
			throw new YyException(ResultEnum.error_update_failed);
		}
	}
	//消息记录详细查询
	@GetMapping(value = "/listxx")
	public String listxx(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String id = request.getParameter("id");
		Notice l = noticeService.getAllXXJL(id);
		map.put("o", l);
		return "/system/Notice/noticequery";
	}
}
