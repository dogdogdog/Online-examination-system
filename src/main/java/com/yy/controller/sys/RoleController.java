package com.yy.controller.sys;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Role;
import com.yy.entity.sys.Users;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.ResourceService;
import com.yy.service.sys.RoleService;

/**
 * 角色管理
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/role")
public class RoleController {

	@Autowired
	private RoleService roleService;
	@Autowired
	private ResourceService resourceService;
	/**
	 * 角色管理首页
	 * 
	 * @return
	 */
	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/system/role/index";
	}

	// 获取表格数据
	@RequestMapping("/list")
	@ResponseBody
	public ResultTable<Role> List(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String name = request.getParameter("name");
		ResultTable<Role> rt = roleService.getLists(page, limit, name);
		return rt;
	}

	// 新增
	@RequestMapping("/add")
	public String add(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("page")) {
			return "/system/role/add";
		} else if (code.equals("add")) {
			String f = request.getParameter("form");
			int j = roleService.setInsert(f);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
		return null;
	}

	/**
	 * 修改
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @author liyingying
	 */
	@RequestMapping("/edit")
	public String update(Map<String, Object> map, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) {
		String update = request.getParameter("update");
		if (update.equals("page")) {
			String id = request.getParameter("id"); // 取得ID
			Role list = roleService.findById(id);
			map.put("u", list);
			return "/system/role/edit";
		} else if (update.equals("edit")) {
			String f = request.getParameter("form");
			int j = roleService.getUpdate(f);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		}
		return null;
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	@RequestMapping(value = "/del")
	public void del(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String code = request.getParameter("code");
		if (code.equals("del")) {
			String ids = request.getParameter("id");
			int r = roleService.getDictDelete(ids);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		} else if (code.equals("dels")) {

		}
	}

	// 角色授权
	@RequestMapping("/grant")
	public void grant(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String cd = request.getParameter("cd"); // 取得ID
		if (cd.equals("qxs")) {
			String jsid = request.getParameter("jsid"); // 取得ID
			String listtoString = resourceService.grantquery(jsid);
			response.setContentType("text/html; charset=utf-8");
			response.getWriter().print(listtoString);
		} else if (cd.equals("qdqx")) {
			String jsid = request.getParameter("jsid"); // 取得角色ID
			String cdid = request.getParameter("code"); // 取得菜单ID
			String cdname = request.getParameter("cdname"); // 取得菜单名称
			int list = resourceService.grantsave(jsid, cdid, cdname);
			if (list > 0) {
				throw new YyException(ResultEnum.sucess_grant_failed);
			} else {
				throw new YyException(ResultEnum.error_grant_upload);
			}
		}
	}
}
