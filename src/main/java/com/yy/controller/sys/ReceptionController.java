package com.yy.controller.sys;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yy.entity.base.SessionInfo;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.exam.QuestSettingionsService;
import com.yy.utils.GlobalConstant;

/**
 * @ClassName: ReceptionController
 * @Description: TODO前台主界面
 * @author 李盈盈
 * @date 2017年9月21日 下午5:04:06
 *
 */
@Controller
public class ReceptionController {
	@Autowired
	private QuestSettingionsService questSettingionsService;

	/**
	 * 用户登录界面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/home")
	public String login(Map<String, Object> map, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		if (sessionInfo == null) {
			return "/login";
		}
		map.put("username", sessionInfo.getUsername());
		return "/reception/index";
	}

	// 工具类
	@RequestMapping(value = "/tool")
	public void tool(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String code = request.getParameter("code");
		if (code.equals("tcontent")) {
			String formal = request.getParameter("formal");
			// 判断当前是否存在考试内容
			SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
			int d = questSettingionsService.tcontent(sessionInfo.getUsercode(), formal);
			if (d > 0) {
				// 当前存在考试内容
				if (d == 1) {
					throw new YyException(ResultEnum.test_content_success);
				} else {
					throw new YyException(ResultEnum.test_content_successs);
				}
			} else {
				// 当前不存在考试内容
				throw new YyException(ResultEnum.test_content_error);
			}
		} else if (code.equals("tcontents")) {
			String formal = request.getParameter("formal");
			SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
			String d = questSettingionsService.tcontents(sessionInfo.getUsercode(), formal);
			response.setContentType("text/html; charset=utf-8");
			response.getWriter().print(d);
		}
	}
}
