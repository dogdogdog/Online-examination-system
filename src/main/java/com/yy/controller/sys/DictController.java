package com.yy.controller.sys;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.base.ZtreeNode;
import com.yy.entity.sys.Dict;
import com.yy.entity.sys.DictData;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.DictService;

@Controller
@RequestMapping("/dict")
public class DictController {

	@Autowired
	private DictService dictService;

	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/system/dict/index";
	}

	@RequestMapping("/ztree")
	@ResponseBody
	public List<ZtreeNode> ztree(HttpServletRequest request, HttpServletResponse response, ZtreeNode tree) {
		String id = request.getParameter("id");
		String pid = request.getParameter("pId");
		List<ZtreeNode> treeList = dictService.ztree(id, pid);
		return treeList;
	}

	@RequestMapping("/add")
	public void add(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("add")) {
			String form = request.getParameter("form");
			int r = dictService.add(form);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/del")
	public void del(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("del")) {
			String id = request.getParameter("id");
			int r = dictService.del(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}

	/**
	 * 编辑
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/edit")
	public String edit(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("edit")) {
			String id = request.getParameter("id");
			int r = dictService.edit(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		} else if (code.equals("page")) {
			String id = request.getParameter("id");
			Dict l = dictService.editpage(id);
			map.put("o", l);
			return "/system/dict/edit";
		}
		return null;
	}

	// 获取表格数据
	@RequestMapping("/list")
	@ResponseBody
	public ResultTable<DictData> List(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String pid = request.getParameter("pid");
		ResultTable<DictData> rt = dictService.getLists(page, limit, pid);
		return rt;
	}

	@RequestMapping("/adddata")
	public void adddata(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("add")) {
			String form = request.getParameter("form");
			int r = dictService.adddata(form);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/deldata")
	public void deldata(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("del")) {
			String id = request.getParameter("id");
			int r = dictService.deldata(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}

	/**
	 * 编辑
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/editdata")
	public String editdata(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("edit")) {
			String id = request.getParameter("id");
			int r = dictService.editdata(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		} else if (code.equals("page")) {
			String id = request.getParameter("id");
			DictData l = dictService.editdatapage(id);
			map.put("o", l);
			return "/system/dict/editdata";
		}
		return null;
	}

	/**
	 * 工具类
	 * 
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/code")
	@ResponseBody
	public List<Select> code(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("professional_code")) {
			String name = request.getParameter("name");
			List<Select> s = dictService.professional(name);
			return s;
		} else if (code.equals("other")) {
			String name = request.getParameter("name");
			List<Select> s = dictService.professional(name);
			return s;
		}
		return null;
	}
}
