package com.yy.controller.base;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.yy.utils.FileUtil;
import com.yy.utils.Tools;

/**
 * 文件上传
 * 
 * @author 李盈盈
 *
 */
@Controller
@RequestMapping("/upload")
public class FileUploadController {

	/**
	 * 单个文件上传的方法
	 * 
	 */
	/*
	 * @RequestMapping(value = "/add", method = RequestMethod.POST)
	 * 
	 * @ResponseBody public String upload(@RequestParam("file") MultipartFile
	 * file, HttpServletRequest request) { if (!file.isEmpty()) { try { //
	 * 这里只是简单例子，文件直接输出到项目路径下。 // 实际项目中，文件需要输出到指定位置，需要在增加代码处理。 //
	 * 还有关于文件格式限制、文件大小限制，详见：中配置。 ServletContext sc =
	 * request.getSession().getServletContext(); System.out.println("sc:" + sc);
	 * // 上传位置 String path = sc.getRealPath("/Study/video") + "\\"; // //
	 * 设定文件保存的目录" // String path = "d:/Study/video/"; if (!new
	 * File(path).exists()) { new File(path).mkdir(); }
	 * System.out.println("path:" + path); File imageFile = new File(path,
	 * file.getOriginalFilename()); FileOutputStream fops = new
	 * FileOutputStream(file.getOriginalFilename());
	 * 
	 * BufferedOutputStream out = new BufferedOutputStream( new
	 * FileOutputStream(new File(file.getOriginalFilename())));
	 * 
	 * BufferedOutputStream out = new BufferedOutputStream(fops);
	 * out.write(file.getBytes()); out.flush(); out.close(); } catch
	 * (FileNotFoundException e) { e.printStackTrace(); return "上传失败," +
	 * e.getMessage(); } catch (IOException e) { e.printStackTrace(); return
	 * "上传失败," + e.getMessage(); } return "上传成功"; } else { return
	 * "上传失败，因为文件是空的."; } }
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		String fileName = file.getOriginalFilename();
		String filePath = "";
		// 获取文件后缀名
		String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
		long str1 = Calendar.getInstance().getTimeInMillis();
		if (!file.isEmpty()) {
			try {
				// 文件保存路径
				Resource resource = new ClassPathResource("config/system.xml");
				File filer = resource.getFile();
				SAXReader reader = new SAXReader();
				Document doc = reader.read(filer); // 加载xml文件
				Node node = doc.selectSingleNode("//learning/video");
				String ps = node.getStringValue();
				System.out.println(ps);
				filePath = ps + "\\" + str1 + "." + prefix;
				File file1 = new File(filePath);
				File f2 = new File(ps);
				if (!f2.exists()) {
					f2.getParentFile().mkdirs();
				}
				if (!file1.getParentFile().exists()) {
					file1.getParentFile().mkdir();
				}

				file.transferTo(file1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// 返回json
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("msg", "上传成功");
		map.put("url", str1 + "." + prefix);
		String json = JSON.toJSONString(map);
		System.out.println(json);
		return map;

	}

	// 图片上传
	@RequestMapping(value = "/images", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> images(@RequestParam("file") MultipartFile file, HttpServletRequest request)
			throws Exception {
		String fileName = file.getOriginalFilename();
		String filePath = "";
		String ps = "";
		// 获取文件后缀名
		String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
		long str1 = Calendar.getInstance().getTimeInMillis();
		// 獲取XML文件
		Resource resource = new ClassPathResource("config/system.xml");
		File filer = resource.getFile();
		// BufferedReader br = new BufferedReader(new InputStreamReader(new
		// FileInputStream(filer)));
		// System.out.println(filer);
		try {
			SAXReader reader = new SAXReader();
			Document doc = reader.read(filer); // 加载xml文件
			// List list = doc.selectNodes("//learning/picture");
			Node node = doc.selectSingleNode("//learning/picture");
			ps = node.getStringValue();
			// System.out.println("node1 = " + node.getStringValue());
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (!file.isEmpty()) {
			try {
				// 文件保存路径
				// ServletContext sc = request.getSession().getServletContext();
				// String path = sc.getRealPath("/Study/images") + "\\";
				filePath = ps + "\\" + str1 + "." + prefix;
				File file1 = new File(filePath);
				File f2 = new File(ps);
				if (!f2.exists()) {
					f2.getParentFile().mkdirs();
				}
				if (!file1.getParentFile().exists()) {
					file1.getParentFile().mkdir();
				}
				file.transferTo(file1);
				// 压缩图片
				// File srcfile = new File(filePath);
				FileUtil.reduceImg(filePath, filePath, 750, 550, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// 返回json
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("code", 0);
		map.put("msg", "上传成功");
		map.put("url", str1 + "." + prefix);
		// String json = JSON.toJSONString(map);
		// System.out.println(json);
		return map;

	}

	/**
	 * 多文件上传 主要是使用了MultipartHttpServletRequest和MultipartFile
	 */
	@RequestMapping(value = "/upload/batch")
	public @ResponseBody String batchUpload(HttpServletRequest request) {
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		MultipartFile file = null;
		BufferedOutputStream stream = null;

		for (int i = 0; i < files.size(); ++i) {
			file = files.get(i);
			if (!file.isEmpty()) {
				try {
					byte[] bytes = file.getBytes();
					stream = new BufferedOutputStream(new FileOutputStream(new File(file.getOriginalFilename())));
					stream.write(bytes);
					stream.close();
				} catch (Exception e) {
					stream = null;
					return "You failed to upload " + i + " => " + e.getMessage();
				}
			} else {
				return "You failed to upload " + i + " because the file was empty.";
			}
		}
		return "upload successful";
	}

	/**
	 * 视频流读取
	 * 
	 * @param id
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/video")
	// @ResponseBody
	public void video(String id, HttpServletResponse response, HttpServletRequest request) {
		String ps = "";
		try {
			Resource resource = new ClassPathResource("config/system.xml");
			File filer = resource.getFile();
			SAXReader reader = new SAXReader();
			Document doc = reader.read(filer); // 加载xml文件
			// List list = doc.selectNodes("//learning/picture");
			Node node = doc.selectSingleNode("//learning/video");
			ps = node.getStringValue();
			// System.out.println("node1 = " + node.getStringValue());
			File file = new File(ps + "\\" + id);
			FileInputStream in = new FileInputStream(file);
			// ServletOutputStream out = response.getOutputStream();

			// byte[] b = null;
			// while (in.available() > 0) {
			// b = new byte[in.available()];
			// in.read(b, 0, b.length);
			// out.write(b, 0, b.length);
			// }
			// in.close();
			// out.flush();
			// out.close();
			BufferedInputStream bis = new BufferedInputStream(in, 1000);
			ServletOutputStream out = response.getOutputStream();
			byte b[] = new byte[10240];
			while (bis.available() > 0) {
				b = new byte[bis.available()];
				bis.read(b, 0, b.length);
				out.write(b, 0, b.length);
			}
			bis.close();
			in.close();
			out.flush();
			out.close();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * 圖片流读取
	 * 
	 * @param id
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/dqimages")
	public void dqimages(String id, HttpServletResponse response, HttpServletRequest request) throws Exception {
		if (Tools.notEmpty(id)) {
			// ServletContext sc = request.getSession().getServletContext();
			// String path = sc.getRealPath("/Study/images") + "\\";
			String ps = "";
			try {
				Resource resource = new ClassPathResource("config/system.xml");
				File filer = resource.getFile();
				SAXReader reader = new SAXReader();
				Document doc = reader.read(filer); // 加载xml文件
				// List list = doc.selectNodes("//learning/picture");
				Node node = doc.selectSingleNode("//learning/picture");
				ps = node.getStringValue();
				// System.out.println("node1 = " + node.getStringValue());
				File file = new File(ps + "\\" + id);
				FileInputStream in = new FileInputStream(file);
				ServletOutputStream out = response.getOutputStream();
				BufferedInputStream bis = new BufferedInputStream(in, 500);
				byte b[] = new byte[1024];
				while (bis.available() > 0) {
					if (bis.available() > 1024) {
						b = new byte[1024];
					} else {
						b = new byte[in.available()];
					}
					in.read(b, 0, b.length);
					out.write(b, 0, b.length);
				}
				bis.close();
				in.close();
				out.flush();
				out.close();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}
