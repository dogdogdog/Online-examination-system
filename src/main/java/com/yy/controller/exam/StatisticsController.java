package com.yy.controller.exam;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/statistics")
public class StatisticsController {
	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/exam/Statistics/statistics";
	}
}
