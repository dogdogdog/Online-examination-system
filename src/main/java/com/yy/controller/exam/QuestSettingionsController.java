package com.yy.controller.exam;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.ResultTable;
import com.yy.entity.exam.ExSetting;
import com.yy.entity.exam.ExSettingN;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.exam.QuestSettingionsService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;

@Controller
@RequestMapping("/questionssetting")
public class QuestSettingionsController {

	@Autowired
	private QuestSettingionsService questSettingionsService;

	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/exam/qsetting/index";
	}

	// 获取表格数据
	@RequestMapping("/listsetting")
	@ResponseBody
	public ResultTable<ExSetting> listSetting(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = "";
		try {
			f = request.getParameter("f");// 获取form表单的内容f =
											// request.getParameter("f");//
											// 获取form表单的内容
		} catch (Exception e) {
			// TODO: handle exception
		}
		ResultTable<ExSetting> rt = questSettingionsService.getListsSetting(page, limit, f);
		return rt;
	}

	// 获取表格数据(題類型)
	@RequestMapping("/listsettings")
	@ResponseBody
	public ResultTable<ExSettingN> listsettings(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String id = "";
		try {
			id = request.getParameter("id");// 获取form表单的内容f =
											// request.getParameter("f");//
											// 获取form表单的内容
		} catch (Exception e) {
			// TODO: handle exception
		}
		ResultTable<ExSettingN> rt = questSettingionsService.getListsSettings(page, limit, id);
		return rt;
	}

	@RequestMapping("/add")
	public String addsetting(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("addsetting")) {
			String form = request.getParameter("form");
			int r = questSettingionsService.add(form);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else if (r == -1) {
				throw new YyException(ResultEnum.error_create_failedks);
			} else if (r == -2) {
				throw new YyException(ResultEnum.error_create_failedbe);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		} else if (code.equals("pagesetting")) {
			return "/exam/qsetting/add";
		}
		return null;
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/del")
	public void del(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("dels")) {
			String id = request.getParameter("id");
			int r = questSettingionsService.del(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		} else if (code.equals("ldels")) {
			String id = request.getParameter("id");
			int r = questSettingionsService.ldels(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}

	/**
	 * 编辑
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/edit")
	public String edit(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("edit")) {
			String id = request.getParameter("id");
			int r = questSettingionsService.edit(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		} else if (code.equals("page")) {
			String id = request.getParameter("id");
			ExSetting l = questSettingionsService.editpage(id);
			map.put("s", DateUtil.DateToString(l.getStart_ex(), DateStyle.YYYY_MM_DD_HH_MM_SS));
			map.put("e", DateUtil.DateToString(l.getEnd_ex(), DateStyle.YYYY_MM_DD_HH_MM_SS));
			String s = DateUtil.DateToString(l.getQ_start(), DateStyle.YYYY_MM_DD_HH_MM_SS);
			String e = DateUtil.DateToString(l.getE_end(), DateStyle.YYYY_MM_DD_HH_MM_SS);
			map.put("tksj", s + " - " + e);
			map.put("o", l);
			return "/exam/qsetting/edit";
		} else if (code.equals("bfid")) {
			String id = request.getParameter("id");
			int r = questSettingionsService.editId(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		} else if (code.equals("pages")) {
			String id = request.getParameter("id");
			ExSettingN l = questSettingionsService.editpages(id);
			map.put("o", l);
			return "/exam/qsetting/edits";
		}
		return null;
	}
}
