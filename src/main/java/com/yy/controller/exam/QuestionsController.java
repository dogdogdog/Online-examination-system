package com.yy.controller.exam;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yy.entity.base.Result;
import com.yy.entity.base.ResultTable;
import com.yy.entity.exam.Questions;
import com.yy.entity.sys.Dict;
import com.yy.entity.sys.DictData;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.exam.OptionsService;
import com.yy.service.exam.QuestionsService;

@Controller
@RequestMapping("/questions")
public class QuestionsController {

	@Autowired
	private QuestionsService questionsService;
	@Autowired
	private OptionsService optionsService;
	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/exam/questions/index";
	}

	@RequestMapping("/add")
	public String add(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("add")) {
			String form = request.getParameter("form");
			int r = optionsService.add(form);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}else if (code.equals("page")) {
			return "/exam/questions/add";
		}else if (code.equals("pageoption")) {
			return "/exam/questions/addoption";
		}
		return null;
	}
	@RequestMapping("/addpart")
	@ResponseBody
	public Result<Questions> addpart(HttpServletRequest request, HttpServletResponse response) {
		String rt = request.getParameter("rt");
		String zy = request.getParameter("zy");
		String lx = request.getParameter("lx");
		int rs=questionsService.addyi(rt,zy,lx);
//		if (lx.equals("选择题")) {
//			Result<Questions> lResult=new Result<Questions>();
//			lResult.setCode(rs);
//			return lResult;
//		}else{
			if (rs >0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
//		}
	}
	// 获取表格数据
	@RequestMapping("/list")
	@ResponseBody
	public ResultTable<Questions> List(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");//获取form表单的内容
		ResultTable<Questions> rt = questionsService.getLists(page, limit, f);
		return rt;
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/del")
	public void del(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("dels")) {
			String id = request.getParameter("id");
			int r = questionsService.del(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}

	/**
	 * 编辑
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/edit")
	public String edit(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("edit")) {
			String id = request.getParameter("id");
			int r = questionsService.edit(id);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		} else if (code.equals("page")) {
			String id = request.getParameter("id");
			Questions l = questionsService.editpage(id);
			map.put("o", l);
			return "/exam/questions/edit";
		}if (code.equals("bfid")) {
			String id = request.getParameter("id");
			String key = request.getParameter("key");
			String value = request.getParameter("value");
			int r = questionsService.editId(id,key,value);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		}
		return null;
	}
	//工具集方法
	@RequestMapping("/code")
	public String code(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("choicelist")) {
			String id = request.getParameter("id");
			String l=questionsService.choicelist(id);
			map.put("id", id);
			map.put("l", l);
			return "/exam/questions/editchoice";
		} else if (code.equals("choiceedit")) {
			String f = request.getParameter("f");
			int l=questionsService.choiceedit(f);
			if (l > 0) {
				throw new YyException(ResultEnum.sucess_update_failed);
			} else {
				throw new YyException(ResultEnum.error_update_failed);
			}
		}if (code.equals("choicepage")) {
			
		}
		return null;
	}
	
	
}
