package com.yy.controller.exam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.entity.base.EchartData;
import com.yy.entity.base.Series;
import com.yy.service.exam.StatisticalService;

/**
 * 考试图表返回实例
 * 
 * @author 李盈盈
 *
 */
@Controller
@RequestMapping("/statistical")
public class StatisticalController {

	@Autowired
	private StatisticalService statisticalService;

	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/exam/statistical/index";
	}

	@RequestMapping("/showEchartLine")
	@ResponseBody
	public EchartData lineData(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("开始分析考试图形");
		// 获取查询的考试名称和时间
		String name = request.getParameter("name");
		String sj = request.getParameter("sj");
		List<String> category = new ArrayList<String>();
		HashMap<String, Object> markLine = new HashMap<String, Object>();
		HashMap<String, Object> markLinee = new HashMap<String, Object>();
		List<Long> serisData = new ArrayList<Long>();
		List<Ex_Formal_Assessment> list = statisticalService.findAll(name, sj);
		for (Ex_Formal_Assessment totalNum : list) {
			category.add(totalNum.getUsername());
			// System.out.println();
			serisData.add(Long.valueOf(totalNum.getAccuracy_rate()));
		}
		System.out.println("serisData:" + serisData);
		List<String> legend = new ArrayList<String>(Arrays.asList(new String[] { "考试项目" }));// 数据分组

		List<Series> series = new ArrayList<Series>();// 纵坐标
		// series.add(new Series("考试分数", "bar", serisData));
		// markLinee.put("type", "average");
		// markLinee.put("name", "平均值");
		// String j = JSON.toJSONString(markLinee);
		// markLine.put("data", "[" + j + "]");

		series.add(new Series("考试分数", "line", serisData, markLine));
		System.out.println(JSON.toJSONString(series));
		EchartData data = new EchartData(legend, category, series);
		return data;
	}
}
