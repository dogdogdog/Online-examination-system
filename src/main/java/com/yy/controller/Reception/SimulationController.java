package com.yy.controller.Reception;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.entity.Reception.Ex_Simulation_Answer;
import com.yy.entity.Reception.Ex_Simulation_Assessment;
import com.yy.entity.Reception.SimulationTopic;
import com.yy.entity.base.Result;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.exam.ExSetting;
import com.yy.entity.exam.ExSettingN;
import com.yy.service.exam.QuestSettingionsService;
import com.yy.service.exam.SimulationService;
import com.yy.utils.GlobalConstant;
import com.yy.utils.JsBase64;
import com.yy.utils.ResultUtil;

/**
 * @ClassName: SimulationController
 * @Description: TODO 模擬考試管理類
 * @author 李盈盈
 * @date 2017年9月22日 上午1:14:13
 *
 */
@Controller
@RequestMapping("/simulation")
public class SimulationController {
	@Autowired
	private QuestSettingionsService questSettingionsService;
	@Autowired
	private SimulationService simulationService;

	// 模拟考试存在多条记录
	@GetMapping(value = "/monikss")
	public String monikss(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// SessionInfo sessionInfo = (SessionInfo)
		// request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		// 獲得ID
		String id = request.getParameter("mw");
		String ids = JsBase64.decode(id, "Unicode");
		// System.out.println(ids);
		int c = questSettingionsService.kscssy(ids, request);
		if (0 < c) {
			List<SimulationTopic> l = questSettingionsService.miniList(ids, request);// 隨機查出題
			ExSetting tm = questSettingionsService.miniListTm(ids);
			List<ExSettingN> tmn = questSettingionsService.miniListTmn(ids);
			map.put("tm", tm);
			map.put("t", JSON.toJSONString(l));
			map.put("tmn", JSON.toJSONString(tmn));
			map.put("syks", "");
		} else {
			ExSetting tm = questSettingionsService.miniListTm(ids);
			map.put("tm", tm);
			map.put("t", "");
			map.put("tmn", "");
			map.put("syks", "考试次数使用完毕!");
		}
		return "/reception/test/SimulationTest";
	}

	// 模拟考试时只存在一条记录
	@GetMapping(value = "/moniks")
	public String moniks(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		String ids = questSettingionsService.queryid(sessionInfo.getUsercode());
		// 獲得ID
		int c = questSettingionsService.kscssy(ids, request);
		if (0 < c) {
			List<SimulationTopic> l = questSettingionsService.miniList(ids, request);// 隨機查出題
			ExSetting tm = questSettingionsService.miniListTm(ids);
			List<ExSettingN> tmn = questSettingionsService.miniListTmn(ids);
			map.put("tm", tm);
			map.put("t", JSON.toJSONString(l));
			map.put("tmn", JSON.toJSONString(tmn));
			map.put("syks", "");
		} else {
			ExSetting tm = questSettingionsService.miniListTm(ids);
			map.put("tm", tm);
			map.put("t", "");
			map.put("tmn", "");
			map.put("syks", "考试次数使用完毕!");
		}
		return "/reception/test/SimulationTest";
	}

	// 模拟考试提交数据
	@PostMapping(value = "/Submit")
	@ResponseBody
	public Result<Object> Submit(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String nr = request.getParameter("sj");
		int ids = simulationService.Submit(nr);
		return ResultUtil.error(23, String.valueOf(ids));
	}

	@GetMapping(value = "/Testsimulation")
	@ResponseBody
	public ResultTable<Ex_Simulation_Assessment> Testsimulation(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");
		String qt = request.getParameter("qt");
		ResultTable<Ex_Simulation_Assessment> l = questSettingionsService.ksjl(page, limit, qt, f, request);
		return l;
	}

	@GetMapping(value = "/Testanswer")
	public String Testanswer(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String ids = request.getParameter("id");
		List<Ex_Simulation_Answer> l = simulationService.miniListjl(ids, request);//
		Ex_Simulation_Assessment khjl = simulationService.ksjlxx(ids, request);
		System.out.println(JSON.toJSONString(l));
		map.put("khjl", khjl);
		map.put("ksjl", JSON.toJSONString(l));
		return "/reception/test/Simulationanswer";
	}
	
	
	//模拟考试后台记录
	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/exam/Assessment/Simulation";
	}
	
	@GetMapping(value = "/SimulationTest")
	@ResponseBody
	public ResultTable<Ex_Simulation_Assessment> FormalTest(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");
		ResultTable<Ex_Simulation_Assessment> l = simulationService.kskhjl(page, limit, f, request);
		return l;
	}
	
	/**
	 * 工具类
	 * 
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/code")
	@ResponseBody
	public List<Select> code(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("testname")) {
			String name = request.getParameter("name");
			List<Select> s = simulationService.testname(name);
			return s;
		}
		return null;
	}
}
