package com.yy.controller.Reception;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.yy.entity.Reception.Ex_Formal_Answer;
import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.entity.Reception.SimulationTopic;
import com.yy.entity.base.Result;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.exam.ExSetting;
import com.yy.entity.exam.ExSettingN;
import com.yy.service.exam.FormalService;
import com.yy.utils.GlobalConstant;
import com.yy.utils.JsBase64;
import com.yy.utils.ResultUtil;

/**
 * @ClassName: FormalController
 * @Description: TODO正式考试类
 * @author 李盈盈
 * @date 2017年9月29日 下午8:20:22
 *
 */
@Controller
@RequestMapping("/Formal")
public class FormalController {

	@Autowired
	private FormalService formalService;

	/**
	 * 前台
	 * 
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	// 正式考试存在多条记录
	@GetMapping(value = "/monikss")
	public String monikss(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// 獲得ID
		String id = request.getParameter("mw");
		String ids = JsBase64.decode(id, "Unicode");
		// 先判断自己是否有剩余考试次数
		int c = formalService.kscssy(ids, request);
		if (0 < c) {
			List<SimulationTopic> l = formalService.miniList(ids, request);// 隨機查出題
			ExSetting tm = formalService.miniListTm(ids);
			List<ExSettingN> tmn = formalService.miniListTmn(ids);
			map.put("tm", tm);
			map.put("t", JSON.toJSONString(l));
			map.put("tmn", JSON.toJSONString(tmn));
			map.put("syks", "");
		} else {
			ExSetting tm = formalService.miniListTm(ids);
			map.put("tm", tm);
			map.put("t", "");
			map.put("tmn", "");
			map.put("syks", "考试次数使用完毕!");
		}

		return "/reception/test/FormalTest";
	}

	// 正式考试时只存在一条记录
	@GetMapping(value = "/moniks")
	public String moniks(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		String ids = formalService.queryid(sessionInfo.getUsercode());
		int c = formalService.kscssy(ids, request);
		// 獲得ID
		if (0 < c) {
			List<SimulationTopic> l = formalService.miniList(ids, request);// 隨機查出題
			ExSetting tm = formalService.miniListTm(ids);
			List<ExSettingN> tmn = formalService.miniListTmn(ids);
			map.put("tm", tm);
			map.put("t", JSON.toJSONString(l));
			System.out.println(JSON.toJSONString(tmn));
			map.put("tmn", JSON.toJSONString(tmn));
			map.put("syks", "");
		} else {
			ExSetting tm = formalService.miniListTm(ids);
			map.put("tm", tm);
			map.put("t", "");
			map.put("tmn", "");
			map.put("syks", "考试次数使用完毕!");
		}
		return "/reception/test/FormalTest";
	}

	// 正式考试提交数据
	@PostMapping(value = "/Submit")
	@ResponseBody
	public Result<Object> Submit(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String nr = request.getParameter("sj");
		int ids = formalService.Submit(nr);
		return ResultUtil.error(23, String.valueOf(ids));
	}

	@GetMapping(value = "/Testformal")
	@ResponseBody
	public ResultTable<Ex_Formal_Assessment> Testformal(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");
		String qt = request.getParameter("qt");
		ResultTable<Ex_Formal_Assessment> l = formalService.ksjl(page, qt, limit, f, request);
		return l;
	}

	@GetMapping(value = "/Testanswer")
	public String Testanswer(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String ids = request.getParameter("id");
		List<Ex_Formal_Answer> l = formalService.miniListjl(ids, request);//
		Ex_Formal_Assessment khjl = formalService.ksjlxx(ids, request);
		map.put("khjl", khjl);
		map.put("ksjl", JSON.toJSONString(l));
		return "/reception/test/Formalanswer";
	}

	/**
	 * 后台
	 * 
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */

	@GetMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/exam/Assessment/Assessment";
	}

	@GetMapping(value = "/FormalTest")
	@ResponseBody
	public ResultTable<Ex_Formal_Assessment> FormalTest(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");
		ResultTable<Ex_Formal_Assessment> l = formalService.kskhjl(page, limit, f, request);
		return l;
	}

	/**
	 * 工具类
	 * 
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/code")
	@ResponseBody
	public List<Select> code(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("testname")) {
			String name = request.getParameter("name");
			List<Select> s = formalService.testname(name);
			return s;
		}
		return null;
	}
}
