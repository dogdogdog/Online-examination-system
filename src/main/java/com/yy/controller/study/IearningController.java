package com.yy.controller.study;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.ResultTable;
import com.yy.entity.study.Study_learning;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.study.Study_learningService;

@Controller
@RequestMapping("learning")
public class IearningController {

	@Autowired
	private Study_learningService study_learningService;

	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/Study/learning/index";
	}

	// 读取数据库内容
	@GetMapping(value = "/list")
	@ResponseBody
	public ResultTable<Study_learning> List(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");
		ResultTable<Study_learning> l = study_learningService.getAll(page, limit, f, request);
		return l;
	}

	// 添加
	@RequestMapping(value = "/addpage")
	public String addpage(Map<String, Object> map, HttpServletRequest request) {
		return "/Study/learning/add";
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("add")) {
			String f = request.getParameter("form");
			int j = study_learningService.setInsert(f, request);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
		return null;
	}

	// 修改
	@RequestMapping(value = "/editpage")
	public String editpage(Map<String, Object> map, HttpServletRequest request) {
		String id = request.getParameter("id");
		Study_learning l = study_learningService.editpage(id);
		map.put("o", l);
		return "/Study/learning/edit";
	}

	@RequestMapping("/edit")
	public String edit(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("edit")) {
			String f = request.getParameter("form");
			int j = study_learningService.setUpdate(f, request);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
		return null;
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/del")
	public void del(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("dels")) {
			String id = request.getParameter("id");
			int r = study_learningService.dels(id, request);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}
}
