package com.yy.controller.study;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.ResultTable;
import com.yy.entity.study.Study_Video;
import com.yy.entity.study.Video_Comments;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.study.VideoStudyService;

/**
 * @ClassName: VideoStudyController
 * @Description: TODO 视频学习管理
 * @author 李盈盈
 * @date 2017年10月5日 下午3:35:30
 *
 */
@Controller
@RequestMapping("/videoStudy")
public class StudyVideoController {

	@Autowired
	private VideoStudyService videoStudyService;

	@RequestMapping(value = "/index")
	public String index(Map<String, Object> map, HttpServletRequest request) {
		return "/Study/video/video";
	}

	// 读取数据库内容
	@GetMapping(value = "/list")
	@ResponseBody
	public ResultTable<Study_Video> List(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String f = request.getParameter("f");
		ResultTable<Study_Video> l = videoStudyService.getAll(page, limit, f, request);
		return l;
	}

	// 读取视频总数
	@GetMapping(value = "/spym")
	@ResponseBody
	public ResultTable<Study_Video> spym(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String f = request.getParameter("f");
		ResultTable<Study_Video> l = videoStudyService.getAllcount(f, request);
		return l;
	}

	// 添加
	@RequestMapping(value = "/addpage")
	public String addpage(Map<String, Object> map, HttpServletRequest request) {
		return "/Study/video/videoAdd";
	}

	@RequestMapping("/add")
	public String add(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("add")) {
			String f = request.getParameter("form");
			int j = videoStudyService.setInsert(f, request);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
		return null;
	}

	// 根据ID查找视频然后播放
	@RequestMapping(value = "/play")
	public String play(Map<String, Object> map, HttpServletRequest request) {
		String id = request.getParameter("id");
		Study_Video j = videoStudyService.playquery(id);
		// 获取视频路径
		// String path = j.getRoute();
		// 播放视频
		map.put("v", j);
		return "/tools/video/plays";
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping("/del")
	public void del(HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("dels")) {
			String id = request.getParameter("id");
			int r = videoStudyService.dels(id, request);
			if (r > 0) {
				throw new YyException(ResultEnum.sucess_delete_failed);
			} else {
				throw new YyException(ResultEnum.error_delete_failed);
			}
		}
	}

	// 下面为前台管理中心使用
	@RequestMapping(value = "/RpIndex")
	public String RpIndex(Map<String, Object> map, HttpServletRequest request) {
		return "/reception/study/video/index";
	}

	// 视频分类查询
	@GetMapping(value = "/sortlist")
	@ResponseBody
	public ResultTable<Study_Video> SortList(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		String major_code = request.getParameter("major_code");
		String type = request.getParameter("type");
		String label = request.getParameter("label");
		String createtime = request.getParameter("createtime");
		ResultTable<Study_Video> l = videoStudyService.getSortAll(page, limit, major_code, type, label, createtime,
				request);
		return l;
	}

	// 新增视频评论
	@RequestMapping("/addpl")
	public String addpl(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		String code = request.getParameter("code");
		if (code.equals("add")) {
			String comments = request.getParameter("comments");
			String id = request.getParameter("id");
			int j = videoStudyService.setInsertpl(comments, id, request);
			if (j > 0) {
				throw new YyException(ResultEnum.sucess_create_failed);
			} else {
				throw new YyException(ResultEnum.error_create_failed);
			}
		}
		return null;
	}

	// 视频评论查询
	@GetMapping(value = "/querypl")
	@ResponseBody
	public ResultTable<Video_Comments> querypl(Map<String, Object> map, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String code = request.getParameter("code");
		if (code.equals("queryid")) {
			String id = request.getParameter("id");
			ResultTable<Video_Comments> l = videoStudyService.getPlAllId(id, request);
			return l;
		}

		return null;
	}

}
