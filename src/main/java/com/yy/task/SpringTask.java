package com.yy.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
* 类描述：    定时器启动
* 修改人：liyingying  
* 修改时间：2017年8月14日 下午7:25:10   
* 修改备注：   
* @version  V1.0
 */
@Component
public class SpringTask {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Scheduled(cron = "0 0/10 * * * ?") // 每分钟执行一次
	public void statusCheck() {
//		logger.info("每分钟执行一次。开始……");
		// statusTask.healthCheck();
//		logger.info("每分钟执行一次。结束。");
	}
}
