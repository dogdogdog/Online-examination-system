package com.yy.configuration;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.github.pagehelper.PageHelper;

/**
* 类描述：    注册MyBatis分页插件PageHelper
* 修改人：liyingying  
* 修改时间：2017年8月19日 下午6:51:54   
* 修改备注：   
* @version  V1.0
 */
@Configuration
public class MybatisConfig {
	private static Logger logger = Logger.getLogger(MybatisConfig.class);
	@Bean
	public PageHelper pageHelper() {
		logger.info("启动MyBatis分页插件");
		PageHelper pageHelper = new PageHelper();
		Properties p = new Properties();
		p.setProperty("offsetAsPageNum", "true");
		p.setProperty("rowBoundsWithCount", "true");
		p.setProperty("reasonable", "true");
		pageHelper.setProperties(p);
		return pageHelper;
	}
}
