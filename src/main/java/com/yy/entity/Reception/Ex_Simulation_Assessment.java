package com.yy.entity.Reception;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Ex_Simulation_Assessment implements Serializable {

	private int id;
	private String username;
	private String usercode;
	private String professional_name;
	private String professional_code;
	private String department_name;
	private String department_code;
	private String actual_examination_items;
	private String wrong_questions;
	private String correct_questions;
	private String accuracy_rate;
	private String personality;
	private Date createtime;
	private String testname;

	public String getTestname() {
		return testname;
	}

	public void setTestname(String testname) {
		this.testname = testname;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Ex_Simulation_Assessment() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public String getProfessional_name() {
		return professional_name;
	}

	public void setProfessional_name(String professional_name) {
		this.professional_name = professional_name;
	}

	public String getProfessional_code() {
		return professional_code;
	}

	public void setProfessional_code(String professional_code) {
		this.professional_code = professional_code;
	}

	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	public String getDepartment_code() {
		return department_code;
	}

	public void setDepartment_code(String department_code) {
		this.department_code = department_code;
	}

	public String getActual_examination_items() {
		return actual_examination_items;
	}

	public void setActual_examination_items(String actual_examination_items) {
		this.actual_examination_items = actual_examination_items;
	}

	public String getWrong_questions() {
		return wrong_questions;
	}

	public void setWrong_questions(String wrong_questions) {
		this.wrong_questions = wrong_questions;
	}

	public String getCorrect_questions() {
		return correct_questions;
	}

	public void setCorrect_questions(String correct_questions) {
		this.correct_questions = correct_questions;
	}

	public String getAccuracy_rate() {
		return accuracy_rate;
	}

	public void setAccuracy_rate(String accuracy_rate) {
		this.accuracy_rate = accuracy_rate;
	}

	public String getPersonality() {
		return personality;
	}

	public void setPersonality(String personality) {
		this.personality = personality;
	}

}
