package com.yy.entity.Reception;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Ex_Simulation_Answer implements Serializable {

	private int id;
	private String right_key;
	private String correct_mistake;
	private int ex_questions_id;
	private String factory_name;
	private String factory_code;
	private String professional_name;
	private String professional_code;
	private String type_name;
	private String type_code;
	private String topic;
	private String choice;
	private String answer;
	private String remarks;
	private String note;
	private Date createtime;
	private String username;
	private String usercode;
	private int ex_s_a_id;// 考核ID
	private String fraction;

	public String getFraction() {
		return fraction;
	}

	public void setFraction(String fraction) {
		this.fraction = fraction;
	}

	public Ex_Simulation_Answer() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRight_key() {
		return right_key;
	}

	public void setRight_key(String right_key) {
		this.right_key = right_key;
	}

	public String getCorrect_mistake() {
		return correct_mistake;
	}

	public void setCorrect_mistake(String correct_mistake) {
		this.correct_mistake = correct_mistake;
	}

	public int getEx_questions_id() {
		return ex_questions_id;
	}

	public void setEx_questions_id(int ex_questions_id) {
		this.ex_questions_id = ex_questions_id;
	}

	public String getFactory_name() {
		return factory_name;
	}

	public void setFactory_name(String factory_name) {
		this.factory_name = factory_name;
	}

	public String getFactory_code() {
		return factory_code;
	}

	public void setFactory_code(String factory_code) {
		this.factory_code = factory_code;
	}

	public String getProfessional_name() {
		return professional_name;
	}

	public void setProfessional_name(String professional_name) {
		this.professional_name = professional_name;
	}

	public String getProfessional_code() {
		return professional_code;
	}

	public void setProfessional_code(String professional_code) {
		this.professional_code = professional_code;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	public String getType_code() {
		return type_code;
	}

	public void setType_code(String type_code) {
		this.type_code = type_code;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public int getEx_s_a_id() {
		return ex_s_a_id;
	}

	public void setEx_s_a_id(int ex_s_a_id) {
		this.ex_s_a_id = ex_s_a_id;
	}

}
