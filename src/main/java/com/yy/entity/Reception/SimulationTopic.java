package com.yy.entity.Reception;

/**
 * @ClassName: SimulationTopic
 * @Description: TODO 模擬考試題
 * @author 李盈盈
 * @date 2017年9月22日 上午1:13:56
 *
 */
public class SimulationTopic {

	private int id;
	private String type_name;// 考試類型
	private String type_code;
	private String topic;// 題內容
	private String choice;// 選擇題
	private String answer;// 標準答案
	private String note;// 備註
	private String remarks;
	private String fraction;// 分数
	private int pid;// 对应的自己考试项目
	private int khid;// 考核id

	public int getKhid() {
		return khid;
	}

	public void setKhid(int khid) {
		this.khid = khid;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	public String getType_code() {
		return type_code;
	}

	public void setType_code(String type_code) {
		this.type_code = type_code;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFraction() {
		return fraction;
	}

	public void setFraction(String fraction) {
		this.fraction = fraction;
	}

}
