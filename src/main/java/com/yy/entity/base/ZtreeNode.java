package com.yy.entity.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/**
 * Ztree节点数据封装
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ZtreeNode implements Serializable {
	/**
	 * 节点id
	 */
	private String id;

	private String name;

	private String pId;

	private String t;
	private Boolean open;
	private Boolean isParent;

	private Boolean checked;
	/**
	 * 可在项目自由拓展属性类
	 */
//	private Map<String, Object> attributes = new HashMap<String, Object>();

//	public void addAttribute(String key, String value) {
//		attributes = attributes == null ? new HashMap<String, Object>() : attributes;
//		attributes.put(key, value);
//	}
//
//	public void removeAttribute(String key) {
//		if (attributes != null)
//			attributes.remove(key);
//	}
//
//	public Map<String, Object> getAttributes() {
//		return attributes;
//	}
//
//	public void setAttributes(Map<String, Object> attributes) {
//		this.attributes = attributes;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getT() {
		return t;
	}

	public void setT(String t) {
		this.t = t;
	}

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	public Boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
}
