package com.yy.entity.base;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class SessionInfo implements Serializable {
	private Long id;// 用户ID
	private String usercode;// 登录名
	private String username;// 姓名
	private String factory_name;
	private String factory_code;
	// private String bmmc;
	private String department_name;
	private String department_code;
	private String major_n;
	private String major_c;
	private String jgid;
	private String page;
	private String ip;// 用户IP
	private String permissions;// 用户权限，1为全局，2为局部

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public String getMajor_n() {
		return major_n;
	}

	public void setMajor_n(String major_n) {
		this.major_n = major_n;
	}

	public String getMajor_c() {
		return major_c;
	}

	public void setMajor_c(String major_c) {
		this.major_c = major_c;
	}

	private List<String> resourceList;// 用户可以访问的资源地址列表

	private List<String> resourceAllList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFactory_name() {
		return factory_name;
	}

	public void setFactory_name(String factory_name) {
		this.factory_name = factory_name;
	}

	public String getFactory_code() {
		return factory_code;
	}

	public void setFactory_code(String factory_code) {
		this.factory_code = factory_code;
	}

	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	public String getDepartment_code() {
		return department_code;
	}

	public void setDepartment_code(String department_code) {
		this.department_code = department_code;
	}

	public String getJgid() {
		return jgid;
	}

	public void setJgid(String jgid) {
		this.jgid = jgid;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public List<String> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<String> resourceList) {
		this.resourceList = resourceList;
	}

	public List<String> getResourceAllList() {
		return resourceAllList;
	}

	public void setResourceAllList(List<String> resourceAllList) {
		this.resourceAllList = resourceAllList;
	}

}
