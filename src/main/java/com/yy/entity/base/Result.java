package com.yy.entity.base;

/**
* 类描述：HTTP请求返回的最外层对象    
* 修改人：liyingying  
* 修改时间：2017年8月18日 下午1:35:42   
* 修改备注：   
* @version  V1.0
 */
public class Result<T> {

	// 错误码
	private Integer code;
	// 提示信息
	private String msg;
	// 具体的内容
	private T data;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
