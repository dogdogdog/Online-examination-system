package com.yy.entity.base;

import java.util.HashMap;
import java.util.List;

/**
 * Echart
 * 
 * @author 李盈盈
 *
 * @param <T>
 */
public class Series<T> {
	public String name;

	public String type;
	public List<T> data;// 这里要用int 不能用String 不然前台显示不正常（特别是在做数学运算的时候）
	public HashMap<String, Object> markLine;

	public Series(String name, String type, List<T> data, HashMap<String, Object> markLine) {
		super();
		this.name = name;
		this.type = type;
		this.data = data;
		this.markLine = markLine;
	}
}
