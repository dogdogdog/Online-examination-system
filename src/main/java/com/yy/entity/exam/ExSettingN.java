package com.yy.entity.exam;
/**
* @ClassName: ExSettingN
* @Description: TODO考試题型添加
* @author 李盈盈
* @date 2017年9月16日 下午3:27:30
*
 */
public class ExSettingN {

	private Integer id;
	private Integer setting_id;
	private Integer p_number;
	private Integer p_score;
	private String questions_n;
	private String questions_c;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSetting_id() {
		return setting_id;
	}
	public void setSetting_id(Integer setting_id) {
		this.setting_id = setting_id;
	}
	public Integer getP_number() {
		return p_number;
	}
	public void setP_number(Integer p_number) {
		this.p_number = p_number;
	}
	public Integer getP_score() {
		return p_score;
	}
	public void setP_score(Integer p_score) {
		this.p_score = p_score;
	}
	public String getQuestions_n() {
		return questions_n;
	}
	public void setQuestions_n(String questions_n) {
		this.questions_n = questions_n;
	}
	public String getQuestions_c() {
		return questions_c;
	}
	public void setQuestions_c(String questions_c) {
		this.questions_c = questions_c;
	}
	
	
}
