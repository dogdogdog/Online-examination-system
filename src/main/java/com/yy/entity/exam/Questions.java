package com.yy.entity.exam;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Questions implements Serializable {

	private Integer id;
	private String factory_name;
	private String factory_code;
	private String professional_name;
	private String professional_code;
	private String type_name;
	private String type_code;
	private String topic;
	private String choice;
	private String answer;
	private String remarks;// 其他
	private String note;// 备注
	private Date createtime;
	
	
	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFactory_name() {
		return factory_name;
	}

	public void setFactory_name(String factory_name) {
		this.factory_name = factory_name;
	}

	public String getFactory_code() {
		return factory_code;
	}

	public void setFactory_code(String factory_code) {
		this.factory_code = factory_code;
	}

	public String getProfessional_name() {
		return professional_name;
	}

	public void setProfessional_name(String professional_name) {
		this.professional_name = professional_name;
	}

	public String getProfessional_code() {
		return professional_code;
	}

	public void setProfessional_code(String professional_code) {
		this.professional_code = professional_code;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	public String getType_code() {
		return type_code;
	}

	public void setType_code(String type_code) {
		this.type_code = type_code;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
