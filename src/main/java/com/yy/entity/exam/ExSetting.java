package com.yy.entity.exam;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class ExSetting implements Serializable {
	private Integer id;
	private String n_type;
	private String c_type;
	private Date start_ex;
	private Date end_ex;
	private String name;
	private Integer ex_frequency;
	private String ex_brief;
	private String formal_n;
	private String formal_c;
	private Date q_start;
	private Date e_end;

	public Date getQ_start() {
		return q_start;
	}

	public void setQ_start(Date q_start) {
		this.q_start = q_start;
	}

	public Date getE_end() {
		return e_end;
	}

	public void setE_end(Date e_end) {
		this.e_end = e_end;
	}

	public String getFormal_n() {
		return formal_n;
	}

	public void setFormal_n(String formal_n) {
		this.formal_n = formal_n;
	}

	public String getFormal_c() {
		return formal_c;
	}

	public void setFormal_c(String formal_c) {
		this.formal_c = formal_c;
	}

	public String getEx_brief() {
		return ex_brief;
	}

	public void setEx_brief(String ex_brief) {
		this.ex_brief = ex_brief;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getEx_frequency() {
		return ex_frequency;
	}

	public void setEx_frequency(Integer ex_frequency) {
		this.ex_frequency = ex_frequency;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getN_type() {
		return n_type;
	}

	public void setN_type(String n_type) {
		this.n_type = n_type;
	}

	public String getC_type() {
		return c_type;
	}

	public void setC_type(String c_type) {
		this.c_type = c_type;
	}

	public Date getStart_ex() {
		return start_ex;
	}

	public void setStart_ex(Date start_ex) {
		this.start_ex = start_ex;
	}

	public Date getEnd_ex() {
		return end_ex;
	}

	public void setEnd_ex(Date end_ex) {
		this.end_ex = end_ex;
	}

}
