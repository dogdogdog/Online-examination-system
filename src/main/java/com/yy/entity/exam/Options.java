package com.yy.entity.exam;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Options implements Serializable{

	private Integer id;
	private Integer questions_id;
	private String options;
	private String value;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getQuestions_id() {
		return questions_id;
	}
	public void setQuestions_id(Integer questions_id) {
		this.questions_id = questions_id;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
