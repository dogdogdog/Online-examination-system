package com.yy.entity.sys;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Dict implements Serializable{

	private Integer id;
	private Integer pid;
	private String name;//字典类型名称
	private String code;//编码
	private Integer status;//状态
	private Integer isfixed; //0默认为不固定，1固定
	private Integer seq;//状态
	
	
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIsfixed() {
		return isfixed;
	}
	public void setIsfixed(Integer isfixed) {
		this.isfixed = isfixed;
	}
	
	
}
