package com.yy.entity.sys;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: Notice
 * @Description: TODO公告發佈類
 * @author 李盈盈
 * @date 2017年10月6日 上午11:57:38
 *
 */
@SuppressWarnings("serial")
public class Notice implements Serializable {

	private int id;
	private String information;
	private String other;
	private Date createtime;
	private String type;
	private String type_name;
	private String identification;
	private Date ictime;
	private String subject;
	
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	public Notice() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public Date getIctime() {
		return ictime;
	}

	public void setIctime(Date ictime) {
		this.ictime = ictime;
	}

}
