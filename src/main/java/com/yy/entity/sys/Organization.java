package com.yy.entity.sys;

import java.io.Serializable;

import com.yy.entity.base.IdEntity;
/**
 * 组织机构表
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class Organization  extends IdEntity implements Serializable{

	private String name;
	private String code;
	private String address;
	private String icon;
	private Integer pid;
	private String seq;
	public Organization() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	
	
}
