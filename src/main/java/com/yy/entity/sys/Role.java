package com.yy.entity.sys;

import java.io.Serializable;

import com.yy.entity.base.IdEntity;
/**
 * 角色
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class Role  extends IdEntity implements Serializable{
	private String factory_name;
	private String factory_code;
	private String name;
	private String code;
	private Integer seq;
	private String description;
	private Integer isdefault;
	public Role() {
		super();
	}
	public String getFactory_name() {
		return factory_name;
	}
	public void setFactory_name(String factory_name) {
		this.factory_name = factory_name;
	}
	public String getFactory_code() {
		return factory_code;
	}
	public void setFactory_code(String factory_code) {
		this.factory_code = factory_code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(Integer isdefault) {
		this.isdefault = isdefault;
	}
	
	
}
