package com.yy.entity.sys;

import java.io.Serializable;

import com.yy.entity.base.IdEntity;
/**
 * 菜单管理列表
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class Resource extends IdEntity implements Serializable{
	private String name;
	private String url;
	private String description;
	private String icon;
	private Integer pid;
	private Integer seq;
	private Integer state;
	private Integer resourcetype;//类型菜单还是按钮，0属于菜单，1属于按钮
	private String belong;//true,false是否是一级菜单
	
	
	public Resource() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getResourcetype() {
		return resourcetype;
	}
	public void setResourcetype(Integer resourcetype) {
		this.resourcetype = resourcetype;
	}
	public String getBelong() {
		return belong;
	}
	public void setBelong(String belong) {
		this.belong = belong;
	}
	
	
	
}
