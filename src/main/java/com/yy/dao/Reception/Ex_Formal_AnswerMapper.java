package com.yy.dao.Reception;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.Reception.Ex_Formal_Answer;

@Repository
public interface Ex_Formal_AnswerMapper {

	List<Ex_Formal_Answer> getAllList(HashMap<String, Object> map);

	int add(Ex_Formal_Answer esa);

	int part(HashMap<String, Object> mapc);

	int count(HashMap<String, Object> map);

}
