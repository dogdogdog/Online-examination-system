package com.yy.dao.Reception;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.Reception.Ex_Simulation_Assessment;
import com.yy.entity.base.Select;

@Repository
public interface Ex_Simulation_AssessmentMapper {

	int add(Ex_Simulation_Assessment esat);

	int part(HashMap<String, Object> mapc);

	int count(HashMap<String, Object> map);

	List<Ex_Simulation_Assessment> getAllList(HashMap<String, Object> map);

	Ex_Simulation_Assessment getAllListl(HashMap<String, Object> mapa);

	int updatekh(Ex_Simulation_Assessment esat);

	List<Select> gettestname(HashMap<String, Object> map);

	List<Ex_Simulation_Assessment> getAllListwy(HashMap<String, Object> map);

	int countwy(HashMap<String, Object> map);

}
