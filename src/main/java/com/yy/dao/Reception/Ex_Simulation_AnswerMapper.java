package com.yy.dao.Reception;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.Reception.Ex_Simulation_Answer;

@Repository
public interface Ex_Simulation_AnswerMapper {

	int add(Ex_Simulation_Answer esa);

	int part(HashMap<String, Object> mapc);

	List<Ex_Simulation_Answer> getAllList(HashMap<String, Object> map);

}
