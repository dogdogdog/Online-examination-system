package com.yy.dao.Reception;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.entity.Reception.Ex_Formal_Assessment_wy;
import com.yy.entity.base.Select;

@Repository
public interface Ex_Formal_AssessmentMapper {

	int add(Ex_Formal_Assessment esat);

	int part(HashMap<String, Object> mapc);

	int updatekh(Ex_Formal_Assessment esat);

	int count(HashMap<String, Object> map);

	List<Ex_Formal_Assessment> getAllList(HashMap<String, Object> map);

	Ex_Formal_Assessment getAllListl(HashMap<String, Object> mapa);

	List<Select> gettestname(HashMap<String, Object> map);

	int countwy(HashMap<String, Object> map);

	List<Ex_Formal_Assessment_wy> getAllListwy(HashMap<String, Object> map);

	Ex_Formal_Assessment_wy getAllListwyl(HashMap<String, Object> map1);

	int addwy(Ex_Formal_Assessment_wy esatxz);

	int updatwy(Ex_Formal_Assessment_wy esatxz);

	List<Ex_Formal_Assessment> getAllListwycx(HashMap<String, Object> map);

}
