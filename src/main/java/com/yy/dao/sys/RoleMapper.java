package com.yy.dao.sys;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.base.Select;
import com.yy.entity.sys.Role;

@Repository
public interface RoleMapper {

	int countByExample(HashMap<String, Object> map);

	List<Role> getAllList(HashMap<String, Object> map);

	int setInsert(Role r);

	int setUpdate(Role r);

	Role getFindById(HashMap<String, Object> map);


	int getDelete(Integer valueOf);

	List<Select> getRoleName(HashMap<String, Object> map);

	int getDeleteUser(Integer valueOf);

	int getDeleteResource(Integer valueOf);

}
