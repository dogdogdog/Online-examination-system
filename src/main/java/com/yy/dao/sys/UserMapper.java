package com.yy.dao.sys;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.sys.Users;


@Repository
public interface UserMapper {

	List<Users> getUserByName(HashMap<String, Object> map);

	int countByExample(HashMap<String, Object> map);

	List<Users> getAllList(HashMap<String, Object> map);

	int getUserDeletes(List<Integer> l);

	Users getFindById(HashMap<String, Object> map);

	int setInsert(Users u);

	int setUpdate(Users u);

	int UserRole(Integer id);

	int UserRoleIdDelete(Integer id);

	int UserRoleIdInsert(HashMap<String, Object> map);

	int UserRoleIdUpdate(HashMap<String, Object> map);

	int getFindByIdUser(List<Integer> l);

	Users getUserByNameDl(HashMap<String, Object> map);

	int editid(HashMap<String, Object> map);





}
