package com.yy.dao.sys;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.sys.Notice;

@Repository
public interface NoticeMapper {

	int count(HashMap<String, Object> map);

	List<Notice> getAllList(HashMap<String, Object> map);

	int add(Notice nc);

	int codeedit(HashMap<String, Object> map);

	int getdelete(List<Integer> l);

	Notice getById(Integer valueOf);

}
