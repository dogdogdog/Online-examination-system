package com.yy.dao.sys;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.sys.Resource;

@Repository
public interface ResourceMapper {

	int countByExample(HashMap<String, Object> map);

	List<Resource> getResourcesList(HashMap<String, Object> map);

	int setinsert(Resource r);

	int setupdate(Resource r);

	Resource getFindById(HashMap<String, Object> map);

	int deleteResourcesById(Integer valueOf);

	int deleteResourcesByPId(Integer valueOf);

	int RoleMenu(HashMap<String, Object> map1);

	int RoleMenuID(HashMap<String, Object> map1);

	int RoleMenuIdDelete(Integer valueOf);

	int RoleMenuIdInsert(HashMap<String, Object> map);

	int RoleMenuIdUpdate(HashMap<String, Object> map2);

	List<Resource> getShiroResource(Integer organization_id);

	List<Resource> getShiroResourceall(HashMap<String, Object> map);

}
