package com.yy.dao.sys;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.sys.Dict;

@Repository
public interface DictMapper {

	List<Dict> getAlllist(HashMap<String, Object> map);

	int getAlllistcount(HashMap<String, Object> map1);

	int setadd(Dict dict);

	Dict getById(Integer valueOf);

	int setedit(Dict dict);

	int getdelete(List<Integer> l);

}
