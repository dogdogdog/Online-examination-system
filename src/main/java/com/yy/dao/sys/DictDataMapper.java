package com.yy.dao.sys;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.base.Select;
import com.yy.entity.sys.DictData;

@Repository
public interface DictDataMapper {

	int getAlllistcount(HashMap<String, Object> map);

	List<DictData> getAlllist(HashMap<String, Object> map);

	int setadd(DictData dict);

	DictData getById(Integer valueOf);

	int setedit(DictData dict);

	int getdelete(List<Integer> l);

	int getdeletep(List<Integer> l);

	List<Select> getProfessional(HashMap<String, Object> map);






}
