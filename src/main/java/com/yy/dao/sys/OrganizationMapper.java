package com.yy.dao.sys;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.sys.Organization;

@Repository
public interface OrganizationMapper {

	List<Organization> getAlllist(HashMap<String, Object> map);

	int getAlllistcount(HashMap<String, Object> map);

	int setadd(Organization o);

	int getdelete(List<Integer> l);

	Organization getById(Integer valueOf);

	int setedit(Organization o);

}
