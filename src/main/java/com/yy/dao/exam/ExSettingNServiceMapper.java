package com.yy.dao.exam;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.exam.ExSettingN;

@Repository
public interface ExSettingNServiceMapper {

	int pldel(List<Integer> l);

	int pladd(List<ExSettingN> list);

	int countByExample(HashMap<String, Object> map);

	List<ExSettingN> getAllList(HashMap<String, Object> map);

	ExSettingN getById(Integer valueOf);

	int setedit(ExSettingN n);

	int pldelid(List<Integer> l);

}
