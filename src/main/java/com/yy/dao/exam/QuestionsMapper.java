package com.yy.dao.exam;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.Reception.Ex_Simulation_Answer;
import com.yy.entity.exam.Questions;

@Repository
public interface QuestionsMapper {

	int addpart(Questions r);

	List<Questions> getAlllist(HashMap<String, Object> map);

	int getAlllistcount(HashMap<String, Object> map);

	int editid(HashMap<String, Object> map);

	int getdelete(List<Integer> l);

	List<Questions> kstcx(HashMap<String, Object> map1);

	List<Ex_Simulation_Answer> getAllList(HashMap<String, Object> map);

}
