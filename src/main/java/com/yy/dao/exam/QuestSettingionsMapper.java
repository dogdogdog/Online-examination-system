package com.yy.dao.exam;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.base.Select;
import com.yy.entity.exam.ExSetting;

@Repository
public interface QuestSettingionsMapper {

	int countByExample(HashMap<String, Object> map);

	List<ExSetting> getAllList(HashMap<String, Object> map);

	int add(ExSetting es);

	ExSetting getById(Integer valueOf);

	int setedit(ExSetting es);

	int getdelete(List<Integer> l);

	int tcontent(HashMap<String, Object> map);

	List<Select> tcontents(HashMap<String, Object> map);

}
