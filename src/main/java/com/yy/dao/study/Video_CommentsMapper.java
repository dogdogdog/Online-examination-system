package com.yy.dao.study;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.study.Video_Comments;

@Repository
public interface Video_CommentsMapper {

	int add(Video_Comments vc);

	List<Video_Comments> getAllList(HashMap<String, Object> map);

	int getdelete(List<Integer> l);

}
