package com.yy.dao.study;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.study.Study_Video;

@Repository
public interface Study_VideoMapper {

	int count(HashMap<String, Object> map);

	List<Study_Video> getAllList(HashMap<String, Object> map);

	int add(Study_Video v);

	Study_Video getById(Integer valueOf);

	int getdelete(List<Integer> l);

	List<Study_Video> getAllListpl(List<Integer> l);

}
