package com.yy.dao.study;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yy.entity.study.Study_learning;

@Repository
public interface Study_learningMapper {

	int count(HashMap<String, Object> map);

	List<Study_learning> getAllList(HashMap<String, Object> map);

	List<Study_learning> getAllListpl(List<Integer> l);

	int getdelete(List<Integer> l);

	int add(Study_learning v);

	int Update(Study_learning v);

	Study_learning getById(Integer valueOf);

}
