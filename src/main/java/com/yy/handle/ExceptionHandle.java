package com.yy.handle;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yy.entity.base.Result;
import com.yy.utils.ResultUtil;

/**
* 类描述：    捕获异常类
* 修改人：liyingying  
* 修改时间：2017年8月18日 下午2:13:26   
* 修改备注：   
* @version  V1.0
 */
@ControllerAdvice
@ResponseBody
public class ExceptionHandle {

	private final static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

	@ExceptionHandler(value = Exception.class)
//	@ExceptionHandler(value=MethodArgumentNotValidException.class)
	public Result<Object> handle(HttpServletRequest req,Exception e) throws Exception{
		if (e instanceof YyException) {
			YyException lyyException = (YyException) e;
			return ResultUtil.error(lyyException.getCode(), lyyException.getMessage());
		}else if (e instanceof org.springframework.web.servlet.NoHandlerFoundException) {
			logger.error("【系统异常】 ", e);
			return ResultUtil.error(-1, "未知错误" + e.getMessage());
		}else{
			logger.error("【系统异常】 ", e);
			if(e instanceof DataIntegrityViolationException){
				return ResultUtil.error(-1, "违反了惟一性限制" + e.getMessage());
			}else {
				return ResultUtil.error(-1, "未知错误" + e.getMessage());
			}
			
		}

	}
}
