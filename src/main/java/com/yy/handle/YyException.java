package com.yy.handle;


/**
* 类描述：    自定义异常
* 修改人：liyingying  RuntimeException
* 修改时间：2017年8月18日 下午2:16:42   
* 修改备注：   
* @version  V1.0
 */
public class YyException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1321166842906287631L;
	private Integer code;

	public YyException(ResultEnum resultEnum) {
		super(resultEnum.getMsg());
		this.code = resultEnum.getCode();
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
