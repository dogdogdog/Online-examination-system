package com.yy.utils;

import com.yy.entity.base.Result;

/**
* 类描述：  封装返回结果  
* 修改人：liyingying  
* 修改时间：2017年8月18日 下午1:48:05   
* 修改备注：   
* @version  V1.0
 */
public class ResultUtil {

	public static Result<Object> success(Object object) {
		Result<Object> result = new Result<Object>();
		result.setCode(200);
		result.setMsg("操作成功");
		result.setData(object);
		return result;
	}

	public static Result<Object> success() {
		return success(null);
	}

	public static Result<Object> error(Integer code, String msg) {
		Result<Object> result = new Result<Object>();
		result.setCode(code);
		result.setMsg(msg);
		// result.setData(object);
		return result;
	}
}
