package com.yy.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @类功能说明：编码等工具类 @类修改者： @修改日期： @修改说明：
 * @公司名称：liyingying
 * @作者：李盈盈
 * @创建时间：2016年12月21日 下午6:52:01 @版本：V1.0
 */
public class Tools {

	/**
	 * 检测字符串是否不为空(null,"","null")
	 * 
	 * @param s
	 * @return 不为空则返回true，否则返回false
	 */
	public static boolean notEmpty(String s) {
		return s != null && !"".equals(s) && !"null".equals(s) && !"undefined".equals(s);
	}

	/**
	 * 检测字符串是否为空(null,"","null")
	 * 
	 * @param s
	 * @return 为空则返回true，不否则返回false
	 */
	public static boolean isEmpty(String s) {
		return s == null || "".equals(s) || "null".equals(s) || "undefined".equals(s);
	}

	/**
	 * 测试是否具有指定编码的权限
	 * 
	 * @param sum
	 * @param targetRights
	 * @return
	 */
	public static boolean testRights(BigInteger sum, int targetRights) {
		return sum.testBit(targetRights);
	}

	public static boolean compare(String s1, String s2) {
		byte[] b1 = s1.getBytes();
		byte[] b2 = s2.getBytes();
		Arrays.sort(b1);
		Arrays.sort(b2);
		s1 = new String(b1);
		s2 = new String(b2);
		if (s1.equals(s2)) {
			return true;
			// System.out.println("equal");
		} else {
			// System.out.println("not equal");
			return false;
		}
	}

	// 文件上传
	public static String uploadFile(MultipartFile file, HttpServletRequest request, String name, String path,
			String filePath, String excl) throws IOException {
		File tempFile = new File(path + filePath + name, excl);
		if (!tempFile.getParentFile().exists()) {
			tempFile.getParentFile().mkdir();
			// tempFile.getParentFile().delete();
		}
		if (!tempFile.exists()) {
			tempFile.createNewFile();
		}

		file.transferTo(tempFile);
		return path + filePath + name + "/" + excl;
	}

	public static int doutwoleave(double dou, int i) {
		DecimalFormat doubleFormat = new DecimalFormat(".00");
		return (int) (Double.valueOf(doubleFormat.format(dou)) * i);
	}
}
