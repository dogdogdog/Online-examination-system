package com.yy.service.exam;

import com.yy.entity.base.ResultTable;
import com.yy.entity.exam.Questions;

public interface QuestionsService {

	int add(String form);

	int del(String id);

	Questions editpage(String id);

	int edit(String id);

	ResultTable<Questions> getLists(String page, String limit, String f);

	int addyi(String rt, String zy, String lx);

	int editId(String id, String key, String value);

	String choicelist(String id);

	int choiceedit(String id);

}
