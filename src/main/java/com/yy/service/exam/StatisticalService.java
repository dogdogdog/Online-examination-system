package com.yy.service.exam;

import java.util.List;

import com.yy.entity.Reception.Ex_Formal_Assessment;

public interface StatisticalService {

	List<Ex_Formal_Assessment> findAll(String name, String sj);

}
