package com.yy.service.exam;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.yy.entity.Reception.Ex_Simulation_Assessment;
import com.yy.entity.Reception.SimulationTopic;
import com.yy.entity.base.ResultTable;
import com.yy.entity.exam.ExSetting;
import com.yy.entity.exam.ExSettingN;

public interface QuestSettingionsService {

	ResultTable<ExSetting> getListsSetting(String page, String limit, String f);

	int add(String form);

	ResultTable<ExSettingN> getListsSettings(String page, String limit, String id);

	ExSetting editpage(String id);

	ExSettingN editpages(String id);

	int editId(String id);

	int edit(String id);

	int del(String id);

	int ldels(String id);

	int tcontent(String username, String formal);

	String tcontents(String usercode, String formal);

	List<SimulationTopic> miniList(String id, HttpServletRequest request);

	ExSetting miniListTm(String id);

	List<ExSettingN> miniListTmn(String id);

	String queryid(String usercode);

	int Submit(String nr);

	int kscssy(String id, HttpServletRequest request);

	ResultTable<Ex_Simulation_Assessment> ksjl(String page, String limit, String qt, String f,
			HttpServletRequest request);

}
