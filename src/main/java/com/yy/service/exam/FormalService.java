package com.yy.service.exam;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.yy.entity.Reception.Ex_Formal_Answer;
import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.entity.Reception.SimulationTopic;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.exam.ExSetting;
import com.yy.entity.exam.ExSettingN;

public interface FormalService {

	ExSetting miniListTm(String ids);

	List<SimulationTopic> miniList(String ids, HttpServletRequest request);

	List<ExSettingN> miniListTmn(String ids);

	String queryid(String usercode);

	int Submit(String nr);

	int kscssy(String id, HttpServletRequest request);

	ResultTable<Ex_Formal_Assessment> ksjl(String page, String qt, String limit, String f, HttpServletRequest request);

	ResultTable<Ex_Formal_Answer> kssjjl(String id, HttpServletRequest request, String page, String limit);

	List<Ex_Formal_Answer> miniListjl(String ids, HttpServletRequest request);

	Ex_Formal_Assessment ksjlxx(String ids, HttpServletRequest request);

	ResultTable<Ex_Formal_Assessment> kskhjl(String page, String limit, String f, HttpServletRequest request);

	List<Select> testname(String name);

}
