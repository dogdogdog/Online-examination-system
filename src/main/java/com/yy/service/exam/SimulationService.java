package com.yy.service.exam;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.yy.entity.Reception.Ex_Simulation_Answer;
import com.yy.entity.Reception.Ex_Simulation_Assessment;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;

public interface SimulationService {

	List<Ex_Simulation_Answer> miniListjl(String ids, HttpServletRequest request);

	Ex_Simulation_Assessment ksjlxx(String ids, HttpServletRequest request);

	int Submit(String nr);

	ResultTable<Ex_Simulation_Assessment> kskhjl(String page, String limit, String f, HttpServletRequest request);

	List<Select> testname(String name);

}
