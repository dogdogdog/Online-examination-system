package com.yy.service.study;

import javax.servlet.http.HttpServletRequest;

import com.yy.entity.base.ResultTable;
import com.yy.entity.study.Study_Video;
import com.yy.entity.study.Video_Comments;

public interface VideoStudyService {

	ResultTable<Study_Video> getAll(String page, String limit, String f, HttpServletRequest request);

	int setInsert(String f, HttpServletRequest request);

	Study_Video playquery(String id);

	int dels(String id, HttpServletRequest request);

	ResultTable<Study_Video> getAllcount(String f, HttpServletRequest request);

	ResultTable<Study_Video> getSortAll(String page, String limit, String major_code, String type, String label,
			String createtime, HttpServletRequest request);

	int setInsertpl(String comments, String id, HttpServletRequest request);

	ResultTable<Video_Comments> getPlAllId(String id, HttpServletRequest request);

}
