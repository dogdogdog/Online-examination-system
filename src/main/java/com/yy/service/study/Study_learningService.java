package com.yy.service.study;

import javax.servlet.http.HttpServletRequest;

import com.yy.entity.base.ResultTable;
import com.yy.entity.study.Study_learning;

public interface Study_learningService {

	ResultTable<Study_learning> getAll(String page, String limit, String f, HttpServletRequest request);

	int setInsert(String f, HttpServletRequest request);

	int dels(String id, HttpServletRequest request);

	int setUpdate(String f, HttpServletRequest request);

	Study_learning editpage(String id);

}
