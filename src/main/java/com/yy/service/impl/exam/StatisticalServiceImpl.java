package com.yy.service.impl.exam;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yy.dao.Reception.Ex_Formal_AssessmentMapper;
import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.service.exam.StatisticalService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.Tools;

@Service
public class StatisticalServiceImpl implements StatisticalService {

	@Autowired
	private Ex_Formal_AssessmentMapper ex_Formal_AssessmentMapper;

	@Override
	public List<Ex_Formal_Assessment> findAll(String name, String sj) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(name)) {
			map.put("testname", name);
		}
		if (Tools.notEmpty(sj)) {
			String[] aa = sj.split(" - ");
			map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
			map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
		}
		return ex_Formal_AssessmentMapper.getAllListwycx(map);
	}
}
