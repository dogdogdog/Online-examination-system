package com.yy.service.impl.exam;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.Reception.Ex_Simulation_AnswerMapper;
import com.yy.dao.Reception.Ex_Simulation_AssessmentMapper;
import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.entity.Reception.Ex_Simulation_Answer;
import com.yy.entity.Reception.Ex_Simulation_Assessment;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.service.exam.SimulationService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.Tools;

@Service
public class SimulationServiceImpl implements SimulationService {

	@Autowired
	private Ex_Simulation_AnswerMapper ex_Simulation_AnswerMapperMapper;
	@Autowired
	private Ex_Simulation_AssessmentMapper ex_Simulation_AssessmentMapperMapper;

	@Override
	public List<Ex_Simulation_Answer> miniListjl(String ids, HttpServletRequest request) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa.put("ex_s_a_id", Integer.valueOf(ids));
		List<Ex_Simulation_Answer> qt1 = ex_Simulation_AnswerMapperMapper.getAllList(mapa);
		return qt1;
	}

	@Override
	public Ex_Simulation_Assessment ksjlxx(String ids, HttpServletRequest request) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa.put("id", Integer.valueOf(ids));
		Ex_Simulation_Assessment qt1 = ex_Simulation_AssessmentMapperMapper.getAllListl(mapa);
		return qt1;
	}

	@Override
	public int Submit(String nr) {
		JSONObject jsonObject = JSONObject.parseObject(nr);
		String khid = "";
		int ksfs = 0;
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("khid")) {
					khid = (String) entry.getValue();
				}

			}
		}
		if (Tools.isEmpty(khid)) {
			return 0;
		} else {
			// 根據考核ID查詢出相應的項目
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ex_s_a_id", Integer.valueOf(khid));
			List<Ex_Simulation_Answer> qt = ex_Simulation_AnswerMapperMapper.getAllList(map);
			String Result = "";
			for (Ex_Simulation_Answer o : qt) {
				int id = o.getId();
				// System.out.println("id: " + id);
				for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
					if (Tools.notEmpty((String) entry.getValue())) {
						if (entry.getKey().equals("daxzt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("dajdt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("datkt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("dapdt" + id)) {
							Result = (String) entry.getValue();
						}
						if (o.getType_code().equals("XZTS")) {
							for (int i = 0; i < 10; i++) {
								if (entry.getKey().equals("daxzts" + id + "," + i)) {
									Result = Result + (String) entry.getValue() + ",";
								}
							}
						}

					}

				}
				if (o.getType_code().equals("XZTS")) {
					if (Result.length() > 1) {
						Result = Result.substring(0, Result.length() - 1);
					}
				}
				// 根據ID插入結果
				HashMap<String, Object> mapc = new HashMap<String, Object>();
				mapc.put("id", Integer.valueOf(o.getId()));
				mapc.put("key", "right_key");
				mapc.put("value", String.valueOf(Result));
				Result = "";
				int a = ex_Simulation_AnswerMapperMapper.part(mapc);
			}
			// 重新去查詢
			HashMap<String, Object> mapa = new HashMap<String, Object>();
			mapa.put("ex_s_a_id", Integer.valueOf(khid));
			List<Ex_Simulation_Answer> qt1 = ex_Simulation_AnswerMapperMapper.getAllList(mapa);
			int c = 0;
			int z = 0;
			// 計算考試分數
			for (Ex_Simulation_Answer o : qt1) {
				HashMap<String, Object> mapc1 = new HashMap<String, Object>();
				if (Tools.compare(o.getAnswer(), o.getRight_key())) {
					z++;
					ksfs = (ksfs + Integer.valueOf(o.getFraction()));
					mapc1.put("id", Integer.valueOf(o.getId()));
					mapc1.put("key", "correct_mistake");
					mapc1.put("value", "1");
					ex_Simulation_AnswerMapperMapper.part(mapc1);
				} else {
					c++;
					mapc1.put("id", Integer.valueOf(o.getId()));
					mapc1.put("key", "correct_mistake");
					mapc1.put("value", "0");
					ex_Simulation_AnswerMapperMapper.part(mapc1);
				}

			}
			// 更改考核信息
			Ex_Simulation_Assessment esat = new Ex_Simulation_Assessment();
			esat.setId(Integer.valueOf(khid));
			esat.setCorrect_questions(String.valueOf(z));
			esat.setWrong_questions(String.valueOf(c));
			System.out.println("ksfs: " + ksfs);
			esat.setAccuracy_rate(String.valueOf(ksfs));
			esat.setCreatetime(new Date());
			ex_Simulation_AssessmentMapperMapper.updatekh(esat);
		}
		return ksfs;
	}

	@Override
	public ResultTable<Ex_Simulation_Assessment> kskhjl(String page, String limit, String f,
			HttpServletRequest request) {
		String username = "", zydm = "", testname = "", kssj = "", bj = "";
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("username")) {
						username = (String) entry.getValue();
					} else if (entry.getKey().equals("professional_code")) {
						zydm = (String) entry.getValue();
					} else if (entry.getKey().equals("testname")) {
						testname = (String) entry.getValue();
					} else if (entry.getKey().equals("kssj")) {
						kssj = (String) entry.getValue();
					} else if (entry.getKey().equals("bj")) {
						bj = (String) entry.getValue();
					}
				}

			}
		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(username)) {
			map.put("username", username);
		}
		if (Tools.notEmpty(zydm)) {
			map.put("professional_code", zydm);
		}
		if (Tools.notEmpty(testname)) {
			map.put("testname", testname);
		}
		if (Tools.notEmpty(kssj)) {
			String[] aa = kssj.split(" - ");
			map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
			map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
		}
		int count = 0;
		List<Ex_Simulation_Assessment> d = null;
		// System.out.println("bj:" + bj);
		if (bj.equals("1")) {
			count = ex_Simulation_AssessmentMapperMapper.countwy(map);
			PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
			d = ex_Simulation_AssessmentMapperMapper.getAllListwy(map);
		} else {
			count = ex_Simulation_AssessmentMapperMapper.count(map);
			PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
			d = ex_Simulation_AssessmentMapperMapper.getAllList(map);
		}
		ResultTable<Ex_Simulation_Assessment> r = new ResultTable<Ex_Simulation_Assessment>();
		System.out.println("count :"+count);
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public List<Select> testname(String name) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		List<Select> l = ex_Simulation_AssessmentMapperMapper.gettestname(map);
		return l;
	}

}
