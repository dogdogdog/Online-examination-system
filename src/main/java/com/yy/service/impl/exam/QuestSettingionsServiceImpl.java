package com.yy.service.impl.exam;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.Reception.Ex_Simulation_AnswerMapper;
import com.yy.dao.Reception.Ex_Simulation_AssessmentMapper;
import com.yy.dao.exam.ExSettingNServiceMapper;
import com.yy.dao.exam.QuestSettingionsMapper;
import com.yy.dao.exam.QuestionsMapper;
import com.yy.entity.Reception.Ex_Simulation_Answer;
import com.yy.entity.Reception.Ex_Simulation_Assessment;
import com.yy.entity.Reception.SimulationTopic;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.exam.ExSetting;
import com.yy.entity.exam.ExSettingN;
import com.yy.entity.exam.Questions;
import com.yy.service.exam.QuestSettingionsService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.GlobalConstant;
import com.yy.utils.Tools;

@Service
public class QuestSettingionsServiceImpl implements QuestSettingionsService {

	@Autowired
	private QuestSettingionsMapper questSettingions1Mapper;

	@Autowired
	private ExSettingNServiceMapper exSettingNServiceMapper;
	@Autowired
	private QuestionsMapper questionsMapper;
	@Autowired
	private Ex_Simulation_AnswerMapper ex_Simulation_AnswerMapperMapper;
	@Autowired
	private Ex_Simulation_AssessmentMapper ex_Simulation_AssessmentMapperMapper;

	@Override
	public ResultTable<ExSetting> getListsSetting(String page, String limit, String f) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		String professional_code = "", formal = "", rqsz = "";
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("formal")) {
						formal = (String) entry.getValue();
					} else if (entry.getKey().equals("professional_code")) {
						professional_code = (String) entry.getValue();
					} else if (entry.getKey().equals("rqsz")) {
						rqsz = (String) entry.getValue();
					}
				}
			}
			if (Tools.notEmpty(professional_code)) {
				map.put("c_type", professional_code);
			}
			if (Tools.notEmpty(formal)) {
				map.put("formal_n", formal);
			}
			if (Tools.notEmpty(rqsz)) {
				String[] aa = rqsz.split(" - ");
				map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
				map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
			}
		}

		int count = questSettingions1Mapper.countByExample(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<ExSetting> l = questSettingions1Mapper.getAllList(map);
		ResultTable<ExSetting> rt = new ResultTable<ExSetting>();
		rt.setCode(0);
		rt.setCount(count);
		rt.setMsg("加载成功");
		rt.setData(l);
		return rt;
	}

	@Override
	public int add(String form) {
		JSONObject jsonObject = JSONObject.parseObject(form);
		String json = "";
		int rs = 0;
		String formal = "", formalc = "", professional_name = "", professional_code = "", name = "", start_ex = "",
				end_ex = "", ex_frequency = "", ex_brief = "", q_start = "", e_end = "", rqsz = "";
		String questions_c1 = "", questions_c2 = "", questions_c3 = "", questions_c4 = "", questions_c5 = "";
		String questions_n1 = "", questions_n2 = "", questions_n3 = "", questions_n4 = "", questions_n5 = "";
		String p_number1 = "0", p_number2 = "0", p_number3 = "0", p_number4 = "0", p_number5 = "0";
		String p_score1 = "0", p_score2 = "0", p_score3 = "0", p_score4 = "0", p_score5 = "0";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("formal")) {
					formal = (String) entry.getValue();
				} else if (entry.getKey().equals("professional_code")) {
					professional_code = (String) entry.getValue();
				} else if (entry.getKey().equals("professional_name")) {
					professional_name = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("start_ex")) {
					start_ex = (String) entry.getValue();
				} else if (entry.getKey().equals("end_ex")) {
					end_ex = (String) entry.getValue();
				} else if (entry.getKey().equals("ex_frequency")) {
					ex_frequency = (String) entry.getValue();
				} else if (entry.getKey().equals("ex_brief")) {
					ex_brief = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_c1")) {
					questions_c1 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_c2")) {
					questions_c2 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_c3")) {
					questions_c3 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_c4")) {
					questions_c4 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_c5")) {
					questions_c5 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_n1")) {
					questions_n1 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_n2")) {
					questions_n2 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_n3")) {
					questions_n3 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_n4")) {
					questions_n4 = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_n5")) {
					questions_n5 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_number1")) {
					p_number1 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_number2")) {
					p_number2 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_number3")) {
					p_number3 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_number4")) {
					p_number4 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_number5")) {
					p_number5 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_score1")) {
					p_score1 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_score2")) {
					p_score2 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_score3")) {
					p_score3 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_score4")) {
					p_score4 = (String) entry.getValue();
				} else if (entry.getKey().equals("p_score5")) {
					p_score5 = (String) entry.getValue();
				} else if (entry.getKey().equals("rqsz")) {
					rqsz = (String) entry.getValue();
				}
			}
		}
		if (Tools.notEmpty(rqsz)) {
			String[] aa = rqsz.split(" - ");
			q_start = aa[0];
			e_end = aa[1];
		}
		if (p_score1 != "0") {
			int t = Integer.valueOf(p_score1) / Integer.valueOf(p_number1);
			int y = t * Integer.valueOf(p_number1);
			if (t != y) {
				rs = -2;
			}
		}
		if (p_score2 != "0") {
			int t = Integer.valueOf(p_score2) / Integer.valueOf(p_number2);
			int y = t * Integer.valueOf(p_number2);
			if (t != y) {
				rs = -2;
			}
		}
		if (p_score3 != "0") {
			int t = Integer.valueOf(p_score3) / Integer.valueOf(p_number3);
			int y = t * Integer.valueOf(p_number3);
			if (t != y) {
				rs = -2;
			}
		}
		if (p_score4 != "0") {
			int t = Integer.valueOf(p_score4) / Integer.valueOf(p_number4);
			int y = t * Integer.valueOf(p_number4);
			if (t != y) {
				rs = -2;
			}
		}
		if (p_score5 != "0") {
			int t = Integer.valueOf(p_score5) / Integer.valueOf(p_number5);
			int y = t * Integer.valueOf(p_number5);
			if (t != y) {
				rs = -2;
			}
		}
		if (formal.equals("正式考试")) {
			formalc = "1";
		} else if (formal.equals("模拟考试")) {
			formalc = "0";
		}
		// 计算考试分数是否为100分
		int fs = Integer.valueOf(p_score1) + Integer.valueOf(p_score2) + Integer.valueOf(p_score3)
				+ Integer.valueOf(p_score4) + Integer.valueOf(p_score5);
		if (fs == 100) {

			ExSetting es = new ExSetting();
			es.setName(name);
			es.setEx_brief(ex_brief);
			es.setFormal_n(formal);
			es.setFormal_c(formalc);
			es.setC_type(professional_code);
			es.setEx_frequency(Integer.valueOf(ex_frequency));
			es.setEnd_ex(DateUtil.StringToDate(end_ex, DateStyle.YYYY_MM_DD_HH_MM_SS));
			es.setStart_ex(DateUtil.StringToDate(start_ex, DateStyle.YYYY_MM_DD_HH_MM_SS));
			es.setN_type(professional_name);
			es.setQ_start(DateUtil.StringToDate(q_start, DateStyle.YYYY_MM_DD_HH_MM_SS));
			es.setE_end(DateUtil.StringToDate(e_end, DateStyle.YYYY_MM_DD_HH_MM_SS));
			rs = questSettingions1Mapper.add(es);
			if (rs > 0) {
				List<Integer> l = new ArrayList<Integer>();
				l.add(es.getId());
				// 删除现在已有的信息
				int x = exSettingNServiceMapper.pldel(l);
				json = "[{\"setting_id\":\"" + es.getId() + "\",\"questions_c\":\"" + questions_c1
						+ "\",\"questions_n\":\"" + questions_n1 + "\",\"p_number\":\"" + p_number1
						+ "\",\"p_score\":\"" + p_score1 + "\"}";
				if (Tools.notEmpty(questions_c2)) {
					json += ",{\"setting_id\":\"" + es.getId() + "\",\"questions_c\":\"" + questions_c2
							+ "\",\"questions_n\":\"" + questions_n2 + "\",\"p_number\":\"" + p_number2
							+ "\",\"p_score\":\"" + p_score2 + "\"}";
				}
				if (Tools.notEmpty(questions_c3)) {
					json += ",{\"setting_id\":\"" + es.getId() + "\",\"questions_c\":\"" + questions_c3
							+ "\",\"questions_n\":\"" + questions_n3 + "\",\"p_number\":\"" + p_number3
							+ "\",\"p_score\":\"" + p_score3 + "\"}";
				}
				if (Tools.notEmpty(questions_c4)) {
					json += ",{\"setting_id\":\"" + es.getId() + "\",\"questions_c\":\"" + questions_c4
							+ "\",\"questions_n\":\"" + questions_n4 + "\",\"p_number\":\"" + p_number4
							+ "\",\"p_score\":\"" + p_score4 + "\"}";
				}
				if (Tools.notEmpty(questions_c5)) {
					json += ",{\"setting_id\":\"" + es.getId() + "\",\"questions_c\":\"" + questions_c5
							+ "\",\"questions_n\":\"" + questions_n5 + "\",\"p_number\":\"" + p_number5
							+ "\",\"p_score\":\"" + p_score5 + "\"}";
				}
				json += "]";
				List<ExSettingN> list = new ArrayList<ExSettingN>(JSONArray.parseArray(json, ExSettingN.class));
				int x1 = exSettingNServiceMapper.pladd(list);
			}

		} else {
			rs = -1;
		}
		return rs;
	}

	@Override
	public ResultTable<ExSettingN> getListsSettings(String page, String limit, String id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.isEmpty(id)) {
			id = "0";
		}
		map.put("setting_id", Integer.valueOf(id));
		int count = exSettingNServiceMapper.countByExample(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<ExSettingN> l = exSettingNServiceMapper.getAllList(map);
		ResultTable<ExSettingN> rt = new ResultTable<ExSettingN>();
		rt.setCode(0);
		rt.setCount(count);
		rt.setMsg("加载成功");
		rt.setData(l);
		return rt;
	}

	@Override
	public ExSetting editpage(String id) {
		return questSettingions1Mapper.getById(Integer.valueOf(id));
	}

	@Override
	public ExSettingN editpages(String id) {
		// TODO Auto-generated method stub
		return exSettingNServiceMapper.getById(Integer.valueOf(id));
	}

	@Override
	public int editId(String key) {
		JSONObject jsonObject = JSONObject.parseObject(key);
		String id = null, questions_c = null, questions_n = null, p_number = "0", p_score = "0";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					id = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_c")) {
					questions_c = (String) entry.getValue();
				} else if (entry.getKey().equals("questions_n")) {
					questions_n = (String) entry.getValue();
				} else if (entry.getKey().equals("p_number")) {
					p_number = (String) entry.getValue();
				} else if (entry.getKey().equals("p_score")) {
					p_score = (String) entry.getValue();
				}
			}
		}
		ExSettingN n = new ExSettingN();
		n.setId(Integer.valueOf(id));
		n.setP_number(Integer.valueOf(p_number));
		n.setP_score(Integer.valueOf(p_score));
		n.setQuestions_c(questions_c);
		n.setQuestions_n(questions_n);
		return exSettingNServiceMapper.setedit(n);
	}

	@Override
	public int edit(String ids) {
		JSONObject jsonObject = JSONObject.parseObject(ids);
		String id = null, formal = null, formalc = "", professional_code = null, professional_name = "", name = "",
				start_ex = "", end_ex = "", ex_frequency = "", ex_brief = "", q_start = "", e_end = "", rqsz = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					id = (String) entry.getValue();
				} else if (entry.getKey().equals("formal")) {
					formal = (String) entry.getValue();
				} else if (entry.getKey().equals("professional_code")) {
					professional_code = (String) entry.getValue();
				} else if (entry.getKey().equals("professional_name")) {
					professional_name = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("start_ex")) {
					start_ex = (String) entry.getValue();
				} else if (entry.getKey().equals("end_ex")) {
					end_ex = (String) entry.getValue();
				} else if (entry.getKey().equals("ex_frequency")) {
					ex_frequency = (String) entry.getValue();
				} else if (entry.getKey().equals("ex_brief")) {
					ex_brief = (String) entry.getValue();
				} else if (entry.getKey().equals("rqsz")) {
					rqsz = (String) entry.getValue();
				}
			}
		}
		// System.out.println(rqsz);
		if (formal.equals("正式考试")) {
			formalc = "1";
		} else if (formal.equals("模拟考试")) {
			formalc = "0";
		}
		if (Tools.notEmpty(rqsz)) {
			String[] aa = rqsz.split(" - ");
			q_start = aa[0];
			e_end = aa[1];
		}
		ExSetting es = new ExSetting();
		es.setId(Integer.valueOf(id));
		es.setName(name);
		es.setEx_brief(ex_brief);
		es.setFormal_n(formal);
		es.setFormal_c(formalc);
		es.setC_type(professional_code);
		es.setEx_frequency(Integer.valueOf(ex_frequency));
		es.setEnd_ex(DateUtil.StringToDate(end_ex, DateStyle.YYYY_MM_DD_HH_MM_SS));
		es.setStart_ex(DateUtil.StringToDate(start_ex, DateStyle.YYYY_MM_DD_HH_MM_SS));
		es.setN_type(professional_name);
		es.setQ_start(DateUtil.StringToDate(q_start, DateStyle.YYYY_MM_DD_HH_MM_SS));
		es.setE_end(DateUtil.StringToDate(e_end, DateStyle.YYYY_MM_DD_HH_MM_SS));
		return questSettingions1Mapper.setedit(es);
	}

	@Override
	public int del(String id) {
		// List<ExSetting> list = new
		// ArrayList<ExSetting>(JSONArray.parseArray(ids, ExSetting.class));
		// List<Integer> l = new ArrayList<Integer>();
		// for (ExSetting u : list) {
		// l.add(Integer.valueOf(u.getId()));
		// }
		List<Integer> l = new ArrayList<Integer>();
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			l.add(Integer.valueOf(ids[i]));
		}
		exSettingNServiceMapper.pldel(l);
		return questSettingions1Mapper.getdelete(l);
	}

	@Override
	public int ldels(String id) {
		List<Integer> l = new ArrayList<Integer>();
		l.add(Integer.valueOf(id));
		return exSettingNServiceMapper.pldelid(l);
	}

	@Override
	public int tcontent(String username, String formal) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("usercode", String.valueOf(username));
		// 獲得系統當前時間
		// map.put("dqrq", DateUtil.StringToDate(DateUtil.DQDate() + "
		// 00:00:00", DateStyle.YYYY_MM_DD_HH_MM_SS));
		// System.out.println(DateUtil.StringToDate(DateUtil.DQTime(),
		// DateStyle.YYYY_MM_DD_HH_MM_SS));
		map.put("dqsj", DateUtil.StringToDate(DateUtil.DQTime(), DateStyle.YYYY_MM_DD_HH_MM_SS));
		map.put("formal_c", formal);
		return questSettingions1Mapper.tcontent(map);
	}

	@Override
	public String tcontents(String usercode, String formal) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("usercode", String.valueOf(usercode));
		// 獲得系統當前時間
		map.put("dqsj", DateUtil.StringToDate(DateUtil.DQTime(), DateStyle.YYYY_MM_DD_HH_MM_SS));
		map.put("formal_c", formal);
		List<Select> r = questSettingions1Mapper.tcontents(map);
		return JSON.toJSONString(r, true);
	}

	// 此方法要使用隨機函數
	@Override
	public List<SimulationTopic> miniList(String id, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		// 根據ID查詢出考試設置
		ExSetting es = questSettingions1Mapper.getById(Integer.valueOf(id));
		// 再根據設置查詢出設置的題項
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("setting_id", es.getId());
		List<ExSettingN> esn = exSettingNServiceMapper.getAllList(map);
		List<SimulationTopic> l = new ArrayList<SimulationTopic>();
		if (esn.size() > 0) {
			Ex_Simulation_Assessment esat = new Ex_Simulation_Assessment();
			esat.setUsername("");
			esat.setProfessional_code(sessionInfo.getMajor_c());
			esat.setProfessional_name(sessionInfo.getMajor_n());
			esat.setUsercode(sessionInfo.getUsercode());
			esat.setUsername(sessionInfo.getUsername());
			esat.setAccuracy_rate("0");
			esat.setWrong_questions("0");
			esat.setAccuracy_rate("0");
			esat.setCreatetime(new Date());
			esat.setTestname(es.getName());
			// esat.setProfessional_name(b.getProfessional_name());
			// esat.setProfessional_code(b.getProfessional_code());
			int a = ex_Simulation_AssessmentMapperMapper.add(esat);
			int sjxms = 0;
			// 根据考试设置题查询出应该考试的内容
			for (ExSettingN o : esn) {
				// 專業類型
				String zylx = es.getC_type();
				// 題的個數
				int gs = o.getP_number();
				// 題的分數
				int fs = o.getP_score();
				// 獲得開始時間和結束時間
				// 題的類型
				String tlx = o.getQuestions_c();
				HashMap<String, Object> map1 = new HashMap<String, Object>();
				map1.clear();
				map1.put("type_code", tlx);
				map1.put("gs", gs);
				map1.put("professional_code", zylx);
				map1.put("kssj", DateUtil.StringToDate(String.valueOf(es.getQ_start()), DateStyle.YYYY_MM_DD_HH_MM_SS));
				map1.put("jssj", DateUtil.StringToDate(String.valueOf(es.getE_end()), DateStyle.YYYY_MM_DD_HH_MM_SS));
				// System.out.println(tlx + ":" + gs + ":" + zylx);
				// 根據條件查詢出相應的內容
				List<Questions> qt = questionsMapper.kstcx(map1);
				if (qt.size() > 0) {
					// 添加一条考核信息
					for (Questions b : qt) {
						sjxms++;
						Ex_Simulation_Answer esa = new Ex_Simulation_Answer();
						esa.setEx_questions_id(b.getId());
						esa.setFactory_name(b.getFactory_name());
						esa.setFactory_code(b.getFactory_code());
						esa.setProfessional_name(b.getProfessional_name());
						esa.setProfessional_code(b.getProfessional_code());
						esa.setType_code(b.getType_code());
						esa.setType_name(b.getType_name());
						esa.setTopic(b.getTopic());
						esa.setChoice(b.getChoice());
						esa.setAnswer(b.getAnswer());
						esa.setRemarks(b.getRemarks());
						esa.setNote(b.getNote());
						esa.setCreatetime(new Date());
						esa.setFraction(String.valueOf(fs / gs));
						esa.setEx_s_a_id(esat.getId());
						esa.setUsercode(sessionInfo.getUsercode());
						esa.setUsername(sessionInfo.getUsername());
						ex_Simulation_AnswerMapperMapper.add(esa);
						SimulationTopic st = new SimulationTopic();
						st.setId(b.getId());
						st.setAnswer(b.getAnswer());
						st.setChoice(b.getChoice());
						st.setFraction(String.valueOf(fs / gs));
						st.setNote(b.getNote());
						st.setRemarks(b.getRemarks());
						st.setTopic(b.getTopic());
						st.setType_code(b.getType_code());
						st.setType_name(b.getType_name());
						st.setPid(esa.getId());
						st.setKhid(esat.getId());
						l.add(st);
					}
				}
			}
			HashMap<String, Object> mapc = new HashMap<String, Object>();
			mapc.put("id", Integer.valueOf(esat.getId()));
			mapc.put("key", "actual_examination_items");
			mapc.put("value", String.valueOf(sjxms));
			int b = ex_Simulation_AssessmentMapperMapper.part(mapc);

		}
		return l;
	}

	@Override
	public ExSetting miniListTm(String id) {
		// TODO Auto-generated method stub
		return questSettingions1Mapper.getById(Integer.valueOf(id));
	}

	@Override
	public List<ExSettingN> miniListTmn(String id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("setting_id", Integer.valueOf(id));
		List<ExSettingN> esn = exSettingNServiceMapper.getAllList(map);
		return esn;
	}

	@Override
	public String queryid(String usercode) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("usercode", String.valueOf(usercode));
		// 獲得系統當前時間
		map.put("dqsj", DateUtil.StringToDate(DateUtil.DQTime(), DateStyle.YYYY_MM_DD_HH_MM_SS));
		map.put("formal_c", "0");
		List<Select> r = questSettingions1Mapper.tcontents(map);
		if (r.size() > 0) {
			for (Select s : r) {
				return s.getKey();
			}
		}

		return "0";
	}

	@Override
	public int Submit(String nr) {
		// System.out.println(nr);
		JSONObject jsonObject = JSONObject.parseObject(nr);
		String khid = "";
		int ksfs = 0;
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				// if (Tools.isEmpty(khid)) {
				// break;
				// }
				if (entry.getKey().equals("khid")) {
					khid = (String) entry.getValue();
				}

			}
		}
		// System.out.println("khid :" + khid);
		if (Tools.isEmpty(khid)) {
			return 0;
		} else {
			// 根據考核ID查詢出相應的項目
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ex_s_a_id", Integer.valueOf(khid));
			List<Ex_Simulation_Answer> qt = ex_Simulation_AnswerMapperMapper.getAllList(map);
			// String dajg = "daxzt";
			String Result = "";
			for (Ex_Simulation_Answer o : qt) {
				int id = o.getId();
				// System.out.println("id: " + id);
				for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
					if (Tools.notEmpty((String) entry.getValue())) {
						if (entry.getKey().equals("daxzt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("dajdt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("datkt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("dapdt" + id)) {
							Result = (String) entry.getValue();
						}
						if (o.getType_code().equals("XZTS")) {
							for (int i = 0; i < 10; i++) {
								if (entry.getKey().equals("daxzts" + id + "," + i)) {
									Result = Result + (String) entry.getValue() + ",";
								}
							}
						}
						// else if (entry.getKey().equals("daxzts" + id)) {
						// Result = (String) entry.getValue();
						// }
					}

				}
				if (o.getType_code().equals("XZTS")) {
					Result = Result.substring(0, Result.length() - 1);
				}
				// if (Tools.notEmpty(Result)) {
				// Result = Result.substring(0, Result.length() - 1);
				// }
				// 根據ID插入結果
				HashMap<String, Object> mapc = new HashMap<String, Object>();
				mapc.put("id", Integer.valueOf(o.getId()));
				mapc.put("key", "right_key");
				mapc.put("value", String.valueOf(Result));
				Result = "";
				ex_Simulation_AnswerMapperMapper.part(mapc);
			}
			// 重新去查詢
			HashMap<String, Object> mapa = new HashMap<String, Object>();
			mapa.put("ex_s_a_id", Integer.valueOf(khid));
			List<Ex_Simulation_Answer> qt1 = ex_Simulation_AnswerMapperMapper.getAllList(mapa);
			int c = 0;
			int z = 0;
			// 計算考試分數
			for (Ex_Simulation_Answer o : qt1) {
				HashMap<String, Object> mapc1 = new HashMap<String, Object>();
				// System.out.println("o.getAnswer() :" + o.getAnswer());
				// System.out.println("o.getRight_key() :" + o.getRight_key());
				// if (o.getType_code().equals("XZTS")) {
				//
				// }
				if (Tools.compare(o.getAnswer(), o.getRight_key())) {
					z++;
					ksfs = (ksfs + Integer.valueOf(o.getFraction()));
					mapc1.put("id", Integer.valueOf(o.getId()));
					mapc1.put("key", "correct_mistake");
					mapc1.put("value", "1");
					ex_Simulation_AnswerMapperMapper.part(mapc1);
				} else {
					c++;
					mapc1.put("id", Integer.valueOf(o.getId()));
					mapc1.put("key", "correct_mistake");
					mapc1.put("value", "0");
					ex_Simulation_AnswerMapperMapper.part(mapc1);
				}

				// if (o.getAnswer().equals(o.getRight_key())) {
				//
				// } else {
				//
				// }

			}
			HashMap<String, Object> mapc = new HashMap<String, Object>();
			mapc.put("id", Integer.valueOf(khid));
			mapc.put("key", "wrong_questions");
			mapc.put("value", String.valueOf(c));
			ex_Simulation_AssessmentMapperMapper.part(mapc);
			HashMap<String, Object> mapc1 = new HashMap<String, Object>();
			mapc1.put("id", Integer.valueOf(khid));
			mapc1.put("key", "correct_questions");
			mapc1.put("value", String.valueOf(z));
			ex_Simulation_AssessmentMapperMapper.part(mapc1);
			HashMap<String, Object> mapc2 = new HashMap<String, Object>();
			mapc2.put("id", Integer.valueOf(khid));
			mapc2.put("key", "accuracy_rate");
			mapc2.put("value", String.valueOf(ksfs));
			ex_Simulation_AssessmentMapperMapper.part(mapc2);

		}
		return ksfs;
	}

	@Override
	public int kscssy(String id, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		// 1.取得考试时间以及考试次数
		ExSetting es = questSettingions1Mapper.getById(Integer.valueOf(id));
		Date dataks = es.getStart_ex();// 考试开始时间
		Date datajs = es.getEnd_ex();// 考试结束时间
		int cs = es.getEx_frequency();// 考试次数
		// 2.获得当前的考试次数
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("start_ex", dataks);
		map.put("end_ex", datajs);
		map.put("professional_code", es.getC_type());
		map.put("testname", es.getName());
		map.put("usercode", sessionInfo.getUsercode());
		int count = ex_Simulation_AssessmentMapperMapper.count(map);
		return (cs - count);
	}

	@Override
	public ResultTable<Ex_Simulation_Assessment> ksjl(String page, String limit, String f, String qt,
			HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		map.put("usercode", sessionInfo.getUsercode());
		int count = ex_Simulation_AssessmentMapperMapper.count(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Ex_Simulation_Assessment> d = ex_Simulation_AssessmentMapperMapper.getAllList(map);
		ResultTable<Ex_Simulation_Assessment> r = new ResultTable<Ex_Simulation_Assessment>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

}
