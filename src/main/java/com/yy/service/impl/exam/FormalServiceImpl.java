package com.yy.service.impl.exam;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.Reception.Ex_Formal_AnswerMapper;
import com.yy.dao.Reception.Ex_Formal_AssessmentMapper;
import com.yy.dao.exam.ExSettingNServiceMapper;
import com.yy.dao.exam.QuestSettingionsMapper;
import com.yy.dao.exam.QuestionsMapper;
import com.yy.entity.Reception.Ex_Formal_Answer;
import com.yy.entity.Reception.Ex_Formal_Assessment;
import com.yy.entity.Reception.Ex_Formal_Assessment_wy;
import com.yy.entity.Reception.SimulationTopic;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.exam.ExSetting;
import com.yy.entity.exam.ExSettingN;
import com.yy.entity.exam.Questions;
import com.yy.service.exam.FormalService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.GlobalConstant;
import com.yy.utils.Tools;

@Service
public class FormalServiceImpl implements FormalService {
	@Autowired
	private Ex_Formal_AnswerMapper ex_Formal_AnswerMapper;
	@Autowired
	private Ex_Formal_AssessmentMapper ex_Formal_AssessmentMapper;
	@Autowired
	private QuestSettingionsMapper questSettingions1Mapper;
	@Autowired
	private ExSettingNServiceMapper exSettingNServiceMapper;
	@Autowired
	private QuestionsMapper questionsMapper;

	@Override
	public ExSetting miniListTm(String ids) {
		// TODO Auto-generated method stub
		return questSettingions1Mapper.getById(Integer.valueOf(ids));
	}

	// 此方法要使用随机函数查询出随机内容
	@Override
	public List<SimulationTopic> miniList(String ids, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		// 根据ID查詢出考试設置
		ExSetting es = questSettingions1Mapper.getById(Integer.valueOf(ids));
		// 再根據設置查詢出設置的題項
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("setting_id", es.getId());
		List<ExSettingN> esn = exSettingNServiceMapper.getAllList(map);
		List<SimulationTopic> l = new ArrayList<SimulationTopic>();
		if (esn.size() > 0) {
			Ex_Formal_Assessment esat = new Ex_Formal_Assessment();
			// esat.setUsername("");
			esat.setProfessional_code(sessionInfo.getMajor_c());
			esat.setProfessional_name(sessionInfo.getMajor_n());
			esat.setUsercode(sessionInfo.getUsercode());
			esat.setUsername(sessionInfo.getUsername());
			esat.setCorrect_questions("0");
			esat.setWrong_questions("0");
			esat.setAccuracy_rate("0");
			esat.setCreatetime(new Date());
			esat.setTestname(es.getName());
			int a = ex_Formal_AssessmentMapper.add(esat);
			int sjxms = 0;
			// 根据考试设置题查询出应该考试的内容
			for (ExSettingN o : esn) {
				// 專業類型
				String zylx = es.getC_type();
				// 題的個數
				int gs = o.getP_number();
				// 題的分數
				int fs = o.getP_score();
				// 獲得開始時間和結束時間
				// 題的類型
				String tlx = o.getQuestions_c();
				HashMap<String, Object> map1 = new HashMap<String, Object>();
				map1.clear();
				map1.put("type_code", tlx);
				map1.put("gs", gs);
				map1.put("professional_code", zylx);
				map1.put("kssj", DateUtil.StringToDate(String.valueOf(es.getQ_start()), DateStyle.YYYY_MM_DD_HH_MM_SS));
				map1.put("jssj", DateUtil.StringToDate(String.valueOf(es.getE_end()), DateStyle.YYYY_MM_DD_HH_MM_SS));
				// 根據條件查詢出相應的內容
				List<Questions> qt = questionsMapper.kstcx(map1);
				if (qt.size() > 0) {
					// 添加一条考核信息
					for (Questions b : qt) {
						sjxms++;
						Ex_Formal_Answer esa = new Ex_Formal_Answer();
						esa.setEx_questions_id(b.getId());
						esa.setFactory_name(b.getFactory_name());
						esa.setFactory_code(b.getFactory_code());
						esa.setProfessional_name(b.getProfessional_name());
						esa.setProfessional_code(b.getProfessional_code());
						esa.setType_code(b.getType_code());
						esa.setType_name(b.getType_name());
						esa.setTopic(b.getTopic());
						esa.setChoice(b.getChoice());
						esa.setAnswer(b.getAnswer());
						esa.setRemarks(b.getRemarks());
						esa.setNote(b.getNote());
						esa.setCreatetime(new Date());
						esa.setFraction(String.valueOf(fs / gs));
						esa.setEx_s_a_id(esat.getId());
						esa.setUsercode(sessionInfo.getUsercode());
						esa.setUsername(sessionInfo.getUsername());
						ex_Formal_AnswerMapper.add(esa);
						SimulationTopic st = new SimulationTopic();
						st.setId(b.getId());
						st.setAnswer(b.getAnswer());
						st.setChoice(b.getChoice());
						st.setFraction(String.valueOf(fs / gs));
						st.setNote(b.getNote());
						st.setRemarks(b.getRemarks());
						st.setTopic(b.getTopic());
						st.setType_code(b.getType_code());
						st.setType_name(b.getType_name());
						st.setPid(esa.getId());
						st.setKhid(esat.getId());
						l.add(st);
					}
				}
			}
			HashMap<String, Object> mapc = new HashMap<String, Object>();
			mapc.put("id", Integer.valueOf(esat.getId()));
			mapc.put("key", "actual_examination_items");
			mapc.put("value", String.valueOf(sjxms));
			@SuppressWarnings("unused")
			int b = ex_Formal_AssessmentMapper.part(mapc);
		}
		return l;
	}

	@Override
	public List<ExSettingN> miniListTmn(String ids) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("setting_id", Integer.valueOf(ids));
		List<ExSettingN> esn = exSettingNServiceMapper.getAllList(map);
		return esn;
	}

	@Override
	public String queryid(String usercode) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("usercode", String.valueOf(usercode));
		// 獲得系統當前時間
		map.put("dqsj", DateUtil.StringToDate(DateUtil.DQTime(), DateStyle.YYYY_MM_DD_HH_MM_SS));
		map.put("formal_c", "1");
		List<Select> r = questSettingions1Mapper.tcontents(map);
		if (r.size() > 0) {
			for (Select s : r) {
				return s.getKey();
			}
		}

		return "0";
	}

	@Override
	public int Submit(String nr) {
		JSONObject jsonObject = JSONObject.parseObject(nr);
		String khid = "";
		int ksfs = 0;
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("khid")) {
					khid = (String) entry.getValue();
				}

			}
		}
		if (Tools.isEmpty(khid)) {
			return 0;
		} else {
			// 根據考核ID查詢出相應的項目
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ex_s_a_id", Integer.valueOf(khid));
			List<Ex_Formal_Answer> qt = ex_Formal_AnswerMapper.getAllList(map);
			String Result = "";
			for (Ex_Formal_Answer o : qt) {
				int id = o.getId();
				// System.out.println("id: " + id);
				for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
					if (Tools.notEmpty((String) entry.getValue())) {
						if (entry.getKey().equals("daxzt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("dajdt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("datkt" + id)) {
							Result = (String) entry.getValue();
						} else if (entry.getKey().equals("dapdt" + id)) {
							Result = (String) entry.getValue();
						}
						if (o.getType_code().equals("XZTS")) {
							for (int i = 0; i < 10; i++) {
								if (entry.getKey().equals("daxzts" + id + "," + i)) {
									Result = Result + (String) entry.getValue() + ",";
								}
							}
						}

					}

				}
				if (o.getType_code().equals("XZTS")) {
					if (Result.length() > 1) {
						Result = Result.substring(0, Result.length() - 1);
					}
				}
				// 根據ID插入結果
				HashMap<String, Object> mapc = new HashMap<String, Object>();
				mapc.put("id", Integer.valueOf(o.getId()));
				mapc.put("key", "right_key");
				mapc.put("value", String.valueOf(Result));
				Result = "";
				int a = ex_Formal_AnswerMapper.part(mapc);
			}
			// 重新去查詢
			HashMap<String, Object> mapa = new HashMap<String, Object>();
			mapa.put("ex_s_a_id", Integer.valueOf(khid));
			List<Ex_Formal_Answer> qt1 = ex_Formal_AnswerMapper.getAllList(mapa);
			int c = 0;
			int z = 0;
			// 計算考試分數
			for (Ex_Formal_Answer o : qt1) {
				HashMap<String, Object> mapc1 = new HashMap<String, Object>();
				if (Tools.compare(o.getAnswer(), o.getRight_key())) {
					z++;
					ksfs = (ksfs + Integer.valueOf(o.getFraction()));
					mapc1.put("id", Integer.valueOf(o.getId()));
					mapc1.put("key", "correct_mistake");
					mapc1.put("value", "1");
					ex_Formal_AnswerMapper.part(mapc1);
				} else {
					c++;
					mapc1.put("id", Integer.valueOf(o.getId()));
					mapc1.put("key", "correct_mistake");
					mapc1.put("value", "0");
					ex_Formal_AnswerMapper.part(mapc1);
				}

			}
			// 更改考核信息
			Ex_Formal_Assessment esat = new Ex_Formal_Assessment();
			esat.setId(Integer.valueOf(khid));
			esat.setCorrect_questions(String.valueOf(z));
			esat.setWrong_questions(String.valueOf(c));
			esat.setAccuracy_rate(String.valueOf(ksfs));
			esat.setCreatetime(new Date());
			ex_Formal_AssessmentMapper.updatekh(esat);
			// 查询考核的唯一结果是否存在
			// 根据ID查询考核项目
			HashMap<String, Object> mapid = new HashMap<String, Object>();
			mapid.put("id", Integer.valueOf(khid));
			Ex_Formal_Assessment efawyname = ex_Formal_AssessmentMapper.getAllListl(mapid);
			HashMap<String, Object> map1 = new HashMap<String, Object>();
			// System.out.println(efawyname.getUsercode());
			map1.put("usercode", efawyname.getUsercode());
			map1.put("testname", efawyname.getTestname());
			int count1 = ex_Formal_AssessmentMapper.countwy(map);
			if (count1 >= 1) {// 唯一考核存在
				// 1、查询当前系统中已经存在的分数
				List<Ex_Formal_Assessment_wy> efawy = ex_Formal_AssessmentMapper.getAllListwy(map1);
				String wyfs = "0";
				int wyid = 0;
				for (Ex_Formal_Assessment_wy o : efawy) {
					wyid = o.getId();
					wyfs = o.getAccuracy_rate();
					break;
				}
				if (Integer.valueOf(ksfs) >= Integer.valueOf(wyfs)) {
					// 2.更新当前唯一的数据
					Ex_Formal_Assessment_wy esatxz = new Ex_Formal_Assessment_wy();
					esatxz.setId(wyid);
					esatxz.setCorrect_questions(String.valueOf(z));
					esatxz.setWrong_questions(String.valueOf(c));
					esatxz.setAccuracy_rate(String.valueOf(ksfs));
					esatxz.setPersonality(khid);
					// esatxz.setCreatetime(new Date());
					int d = ex_Formal_AssessmentMapper.updatwy(esatxz);
				}
			} else {
				// 唯一考核不存在，插入一条新的记录
				Ex_Formal_Assessment_wy esatxz = new Ex_Formal_Assessment_wy();
				esatxz.setUsername(efawyname.getUsername());
				esatxz.setProfessional_code(efawyname.getProfessional_code());
				esatxz.setProfessional_name(efawyname.getProfessional_name());
				esatxz.setUsercode(efawyname.getUsercode());
				esatxz.setUsername(efawyname.getUsername());
				esatxz.setActual_examination_items(efawyname.getActual_examination_items());
				esatxz.setCorrect_questions(String.valueOf(z));
				esatxz.setWrong_questions(String.valueOf(c));
				esatxz.setAccuracy_rate(String.valueOf(ksfs));
				esatxz.setCreatetime(efawyname.getCreatetime());
				esatxz.setTestname(efawyname.getTestname());
				esatxz.setPersonality(khid);
				int a = ex_Formal_AssessmentMapper.addwy(esatxz);
			}

		}
		return ksfs;
	}

	@Override
	public int kscssy(String id, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		// 1.取得考试时间以及考试次数
		ExSetting es = questSettingions1Mapper.getById(Integer.valueOf(id));
		Date dataks = es.getStart_ex();// 考试开始时间
		Date datajs = es.getEnd_ex();// 考试结束时间
		int cs = es.getEx_frequency();// 考试次数
		// String testname = es.getName();// 考试名称
		// 2.获得当前的考试次数
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("start_ex", dataks);
		map.put("end_ex", datajs);
		map.put("professional_code", es.getC_type());
		map.put("testname", es.getName());
		map.put("usercode", sessionInfo.getUsercode());
		int count = ex_Formal_AssessmentMapper.count(map);
		return (cs - count);
	}

	@Override
	public ResultTable<Ex_Formal_Assessment> ksjl(String page, String qt, String limit, String f,
			HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		map.put("usercode", sessionInfo.getUsercode());
		int count = ex_Formal_AssessmentMapper.count(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Ex_Formal_Assessment> d = ex_Formal_AssessmentMapper.getAllList(map);
		ResultTable<Ex_Formal_Assessment> r = new ResultTable<Ex_Formal_Assessment>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public ResultTable<Ex_Formal_Answer> kssjjl(String id, HttpServletRequest request, String page, String limit) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ex_s_a_id", Integer.valueOf(id));
		int count = ex_Formal_AnswerMapper.count(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Ex_Formal_Answer> qt = ex_Formal_AnswerMapper.getAllList(map);
		ResultTable<Ex_Formal_Answer> r = new ResultTable<Ex_Formal_Answer>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(qt);
		return r;
	}

	@Override
	public List<Ex_Formal_Answer> miniListjl(String ids, HttpServletRequest request) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa.put("ex_s_a_id", Integer.valueOf(ids));
		List<Ex_Formal_Answer> qt1 = ex_Formal_AnswerMapper.getAllList(mapa);
		return qt1;
	}

	@Override
	public Ex_Formal_Assessment ksjlxx(String ids, HttpServletRequest request) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa.put("id", Integer.valueOf(ids));
		Ex_Formal_Assessment qt1 = ex_Formal_AssessmentMapper.getAllListl(mapa);
		return qt1;
	}

	@Override
	public ResultTable<Ex_Formal_Assessment> kskhjl(String page, String limit, String f, HttpServletRequest request) {
		String username = "", zydm = "", testname = "", kssj = "", bj = "";
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("username")) {
						username = (String) entry.getValue();
					} else if (entry.getKey().equals("professional_code")) {
						zydm = (String) entry.getValue();
					} else if (entry.getKey().equals("testname")) {
						testname = (String) entry.getValue();
					} else if (entry.getKey().equals("kssj")) {
						kssj = (String) entry.getValue();
					} else if (entry.getKey().equals("bj")) {
						bj = (String) entry.getValue();
					}
				}

			}
		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(username)) {
			map.put("username", username);
		}
		if (Tools.notEmpty(zydm)) {
			map.put("professional_code", zydm);
		}
		if (Tools.notEmpty(testname)) {
			map.put("testname", testname);
		}
		if (Tools.notEmpty(kssj)) {
			String[] aa = kssj.split(" - ");
			map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
			map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
		}
		int count = 0;
		List<Ex_Formal_Assessment> d = null;
		// System.out.println("bj:" + bj);
		if (bj.equals("1")) {
			count = ex_Formal_AssessmentMapper.countwy(map);
			PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
			d = ex_Formal_AssessmentMapper.getAllListwycx(map);
		} else {
			count = ex_Formal_AssessmentMapper.count(map);
			PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
			d = ex_Formal_AssessmentMapper.getAllList(map);
		}
		ResultTable<Ex_Formal_Assessment> r = new ResultTable<Ex_Formal_Assessment>();
		System.out.println("count :" + count);
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public List<Select> testname(String name) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		List<Select> l = ex_Formal_AssessmentMapper.gettestname(map);
		return l;
	}

}
