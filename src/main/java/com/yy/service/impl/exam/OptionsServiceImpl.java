package com.yy.service.impl.exam;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.yy.dao.exam.OptionsMapper;
import com.yy.service.exam.OptionsService;
import com.yy.utils.Tools;

@Service
public class OptionsServiceImpl implements OptionsService {

	@Autowired
	private OptionsMapper optionsMapper;

	@Override
	public int add(String form) {
		String questions_id = "", options = "", value = "";
		int jg = 1;
		//无序遍历
		// JSONObject jsonObject = JSONObject.parseObject(form);
		// for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
		// if (Tools.notEmpty((String) entry.getValue())) {
		// if (entry.getKey().equals("options_id")) {
		// questions_id = (String) entry.getValue();
		// } else if (entry.getKey().equals("daxx"+jg)) {
		// options = (String) entry.getValue();
		// } else if (entry.getKey().equals("nr"+jg)) {
		// value = (String) entry.getValue();
		// }
		// }
		// if(Tools.notEmpty(questions_id)&&Tools.notEmpty(questions_id))
		// }
		//有序遍历
		LinkedHashMap<String, String> jsonMap = JSON.parseObject(form,
				new TypeReference<LinkedHashMap<String, String>>() {
				});
		for (Map.Entry<String, String> entry : jsonMap.entrySet()) {
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
		return 0;
	}

}
