package com.yy.service.impl.exam;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.exam.OptionsMapper;
import com.yy.dao.exam.QuestionsMapper;
import com.yy.entity.base.ResultTable;
import com.yy.entity.exam.Questions;
import com.yy.service.exam.QuestionsService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.Tools;

@Service
public class QuestionsServiceImpl implements QuestionsService {

	@Autowired
	private QuestionsMapper questionsMapper;
	@Autowired
	private OptionsMapper optionsMapper;

	@Override
	public int add(String form) {
		return 0;
	}

	@Override
	public int del(String id) {
		// List<Questions> list = new
		// ArrayList<Questions>(JSONArray.parseArray(id, Questions.class));
		List<Integer> l = new ArrayList<Integer>();
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			l.add(Integer.valueOf(ids[i]));
		}
		// for (Questions u : list) {
		// l.add(Integer.valueOf(u.getId()));
		// }
		return questionsMapper.getdelete(l);
	}

	@Override
	public Questions editpage(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int edit(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ResultTable<Questions> getLists(String page, String limit, String f) {
		String professional_code = "", type_code = "", topic = "", rqsz = "";
		System.out.println(f);
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			// 遍历表单的内容
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("professional_code")) {
						professional_code = (String) entry.getValue();
					} else if (entry.getKey().equals("type_code")) {
						type_code = (String) entry.getValue();
					} else if (entry.getKey().equals("topic")) {
						topic = (String) entry.getValue();
					} else if (entry.getKey().equals("rqsz")) {
						rqsz = (String) entry.getValue();
					}
				}
			}

		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(professional_code)) {
			map.put("professional_code", professional_code);
		}
		if (Tools.notEmpty(type_code)) {
			map.put("type_code", type_code);
		}
		if (Tools.notEmpty(topic)) {
			map.put("topic", topic);
		}
		if (Tools.notEmpty(rqsz)) {
			String[] aa = rqsz.split(" - ");
			map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
			map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
		}
		int count = questionsMapper.getAlllistcount(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Questions> oz = questionsMapper.getAlllist(map);
		ResultTable<Questions> l = new ResultTable<Questions>();
		l.setCode(0);
		l.setCount(count);
		l.setMsg("获取成功");
		l.setData(oz);
		return l;
	}

	@Override
	public int addyi(String rt, String zy, String lx) {
		JSONObject jsonObject = JSONObject.parseObject(rt);
		String factory_name = "", factory_code = "", professional_code = "", type_code = "", topic = "", choice = "",
				answer = "", remarks = "", note = "";
		String daxx1 = "", daxx2 = "", daxx3 = "", daxx4 = "", daxx5 = "", nr5 = "", nr4 = "", nr3 = "", nr2 = "",
				nr1 = "";
		String json = "", jj = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("factory_name")) {
					factory_name = (String) entry.getValue();
				} else if (entry.getKey().equals("factory_code")) {
					factory_code = (String) entry.getValue();
				} else if (entry.getKey().equals("professional_code")) {
					professional_code = (String) entry.getValue();
				} else if (entry.getKey().equals("type_code")) {
					type_code = (String) entry.getValue();
				} else if (entry.getKey().equals("topic")) {
					topic = (String) entry.getValue();
				} else if (entry.getKey().equals("choice")) {
					choice = (String) entry.getValue();
				} else if (entry.getKey().equals("answer")) {
					answer = (String) entry.getValue();
				} else if (entry.getKey().equals("remarks")) {
					remarks = (String) entry.getValue();
				} else if (entry.getKey().equals("note")) {
					note = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx1")) {
					daxx1 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx2")) {
					daxx2 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx3")) {
					daxx3 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx4")) {
					daxx4 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx5")) {
					daxx5 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr5")) {
					nr5 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr4")) {
					nr4 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr3")) {
					nr3 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr2")) {
					nr2 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr1")) {
					nr1 = (String) entry.getValue();
				}
			}
		}
		if (type_code.equals("XZT") || type_code.equals("XZTS")) {
			// int rc = r.getId();
			// List<Integer> l=new ArrayList<Integer>();
			// l.add(rc);
			// //删除现在已有的答案
			// int x=optionsMapper.pldel(l);
			// 组合JSON
			json = "[{\"options\":\"" + daxx1 + "\",\"value\":\"" + nr1 + "\"}";
			if (Tools.notEmpty(nr2)) {
				json += ",{\"options\":\"" + daxx2 + "\",\"value\":\"" + nr2 + "\"}";
			}
			if (Tools.notEmpty(nr3)) {
				json += ",{\"options\":\"" + daxx3 + "\",\"value\":\"" + nr3 + "\"}";
			}
			if (Tools.notEmpty(nr4)) {
				json += ",{\"options\":\"" + daxx4 + "\",\"value\":\"" + nr4 + "\"}";
			}
			if (Tools.notEmpty(nr5)) {
				json += ",{\"options\":\"" + daxx5 + "\",\"value\":\"" + nr5 + "\"}";
			}
			json += "]";
			// System.out.println(json);
			// JSONObject json1 = JSON.parseObject(json);
			// HashMap json1 = JSON.parseObject(json, HashMap.class);
			// jj=JSON.toJSONString(json1);
			// List<Options> list = new
			// ArrayList<Options>(JSONArray.parseArray(json, Options.class));
			// Options s= new Options();
			// int x1 = optionsMapper.pladd(list);
			// return rc;
		}

		Questions r = new Questions();
		r.setAnswer(answer);
		r.setChoice(json);
		r.setFactory_code(factory_code);
		r.setFactory_name(factory_name);
		r.setNote(note);
		r.setProfessional_code(professional_code);
		r.setRemarks(remarks);
		r.setTopic(topic);
		r.setType_code(type_code);
		r.setType_name(lx);
		r.setProfessional_name(zy);
		r.setCreatetime(new Date());
		int rs = questionsMapper.addpart(r);
		// if (type_code.equals("XZT")||type_code.equals("XZTS")) {
		// int rc = r.getId();
		// List<Integer> l=new ArrayList<Integer>();
		// l.add(rc);
		// //删除现在已有的答案
		// int x=optionsMapper.pldel(l);
		// //组合JSON
		// String
		// json="[{'questions_id':"+rc+",'options':'"+daxx1+"','value':'"+nr1+"'},{'questions_id':"+rc+",'options':'"+daxx2+"','value':'"+nr2+"'}"
		// +
		// ",{'questions_id':"+rc+",'options':'"+daxx3+"','value':'"+nr3+"'},{'questions_id':"+rc+",'options':'"+daxx4+"','value':'"+nr4+"'}"
		// +
		// ",{'questions_id':"+rc+",'options':'"+daxx5+"','value':'"+nr5+"'}]";
		// List<Options> list = new
		// ArrayList<Options>(JSONArray.parseArray(json, Options.class));
		//// Options s= new Options();
		// int x1=optionsMapper.pladd(list);
		// //return rc;
		// }
		return rs;
	}

	@Override
	public int editId(String id, String key, String value) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(id));
		map.put("key", key);
		map.put("value", value);
		return questionsMapper.editid(map);
	}

	@Override
	public String choicelist(String id) {
		String li = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(id));
		List<Questions> l = questionsMapper.getAlllist(map);
		for (Questions o : l) {
			li = o.getChoice();
			break;
		}
		return li;
	}

	@Override
	public int choiceedit(String id) {
		JSONObject jsonObject = JSONObject.parseObject(id);
		String json = "", ids = "";
		String daxx1 = "", daxx2 = "", daxx3 = "", daxx4 = "", daxx5 = "", nr5 = "", nr4 = "", nr3 = "", nr2 = "",
				nr1 = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					ids = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx1")) {
					daxx1 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx2")) {
					daxx2 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx3")) {
					daxx3 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx4")) {
					daxx4 = (String) entry.getValue();
				} else if (entry.getKey().equals("daxx5")) {
					daxx5 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr5")) {
					nr5 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr4")) {
					nr4 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr3")) {
					nr3 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr2")) {
					nr2 = (String) entry.getValue();
				} else if (entry.getKey().equals("nr1")) {
					nr1 = (String) entry.getValue();
				}
			}
		}
		json = "[{\"options\":\"" + daxx1 + "\",\"value\":\"" + nr1 + "\"}";
		if (Tools.notEmpty(nr2)) {
			json += ",{\"options\":\"" + daxx2 + "\",\"value\":\"" + nr2 + "\"}";
		}
		if (Tools.notEmpty(nr3)) {
			json += ",{\"options\":\"" + daxx3 + "\",\"value\":\"" + nr3 + "\"}";
		}
		if (Tools.notEmpty(nr4)) {
			json += ",{\"options\":\"" + daxx4 + "\",\"value\":\"" + nr4 + "\"}";
		}
		if (Tools.notEmpty(nr5)) {
			json += ",{\"options\":\"" + daxx5 + "\",\"value\":\"" + nr5 + "\"}";
		}
		json += "]";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(ids));
		map.put("key", "choice");
		map.put("value", json);
		return questionsMapper.editid(map);
	}
}
