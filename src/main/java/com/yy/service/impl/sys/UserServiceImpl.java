package com.yy.service.impl.sys;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.sys.UserMapper;
import com.yy.entity.base.Result;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.sys.Users;
import com.yy.service.sys.UserService;
import com.yy.utils.GlobalConstant;
import com.yy.utils.MyDES;
import com.yy.utils.Tools;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	@Override
	public Result<Users> signin(String kaptchaExpected, String name, HttpServletRequest request) {
		JSONObject jsonObject = JSONObject.parseObject(name);
		String username = "", usercode = "", userpass = "";
		Result<Users> r = new Result<Users>();
		try {
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("username")) {
						username = (String) entry.getValue();
					} else if (entry.getKey().equals("password")) {
						userpass = (String) entry.getValue();
					} else if (entry.getKey().equals("code")) {
						usercode = (String) entry.getValue();
					}
				}
			}
			// if (usercode == null ||
			// !usercode.equalsIgnoreCase(kaptchaExpected)) {
			// r.setCode(400);
			// r.setMsg("验证码不正确");
			// } else {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("u", username);
			String ps = MyDES.encryptBasedDes(userpass);
			map.put("p", ps);
			if (username.equals("yy") && userpass.equals("yy")) {
				SessionInfo si = new SessionInfo();
				si.setUsername("超级管理员");
				si.setUsercode("yy");
				si.setMajor_n("yy");
				si.setMajor_c("yy");
				si.setPermissions("1");
				request.getSession().setAttribute(GlobalConstant.SESSION_INFO, si);
				r.setCode(200);
				r.setMsg("1");
			} else {
				List<Users> l = userMapper.getUserByName(map);
				String qxgl = "";
				// Subject subject = SecurityUtils.getSubject();
				if (l.size() > 0) {
					for (Users s : l) {
						SessionInfo si = new SessionInfo();
						si.setId(Long.valueOf(s.getId()));
						si.setUsername(s.getUsername());
						si.setUsercode(s.getUsercode());
						si.setMajor_n(s.getMajor_n());
						si.setMajor_c(s.getMajor_c());
						si.setPermissions(s.getPermissions());
						qxgl = s.getPermissions();
						request.getSession().setAttribute(GlobalConstant.SESSION_INFO, si);
						break;
					}
					// subject.login(usernamePasswordToken); //完成登录
					// Users u=userMapper.getUserByNameDl(map);
					// Users user=(Users) subject.getPrincipal();
					// request.getSession().setAttribute("user", user);

					r.setCode(200);
					r.setMsg(qxgl);
				} else {
					r.setCode(400);
					r.setMsg("登录失败");
				}
			}
			UsernamePasswordToken token = new UsernamePasswordToken(username, userpass);
			SecurityUtils.getSubject().login(token);
		} catch (Exception e) {
			try {
				throw new Exception(e);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return r;
	}

	@Override
	public ResultTable<Users> getLists(String page, String limit, String id, String username) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(username)) {
			map.put("username", username);
		} else if (Tools.notEmpty(id)) {
			map.put("organization_id", Integer.valueOf(id));
		}
		int count = userMapper.countByExample(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Users> l = userMapper.getAllList(map);
		ResultTable<Users> rt = new ResultTable<Users>();
		rt.setCode(0);
		rt.setCount(count);
		rt.setMsg("加载成功");
		rt.setData(l);
		return rt;
	}

	@Override
	public int setInsert(String data) {
		System.out.println(data);
		JSONObject jsonObject = JSONObject.parseObject(data);
		String id = "", username = "", usercode = "", userpass = "8888", sex = "", age = "", phone = "", address = "",
				wechat = "", qq = "", status = "", level = "", permissions = "", note = "", organization_id = "",
				major_n = "", major_c = "", usertype = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					id = (String) entry.getValue();
				} else if (entry.getKey().equals("username")) {
					username = (String) entry.getValue();
				} else if (entry.getKey().equals("usercode")) {
					usercode = (String) entry.getValue();
				} else if (entry.getKey().equals("userpass")) {
					userpass = (String) entry.getValue();
				} else if (entry.getKey().equals("sex")) {
					sex = (String) entry.getValue();
				} else if (entry.getKey().equals("age")) {
					age = (String) entry.getValue();
				} else if (entry.getKey().equals("phone")) {
					phone = (String) entry.getValue();
				} else if (entry.getKey().equals("address")) {
					address = (String) entry.getValue();
				} else if (entry.getKey().equals("wechat")) {
					wechat = (String) entry.getValue();
				} else if (entry.getKey().equals("qq")) {
					qq = (String) entry.getValue();
				} else if (entry.getKey().equals("status")) {
					status = (String) entry.getValue();
				} else if (entry.getKey().equals("level")) {
					level = (String) entry.getValue();
				} else if (entry.getKey().equals("permissions")) {
					permissions = (String) entry.getValue();
				} else if (entry.getKey().equals("note")) {
					note = (String) entry.getValue();
				} else if (entry.getKey().equals("organization_id")) {
					organization_id = (String) entry.getValue();
				} else if (entry.getKey().equals("usertype")) {
					usertype = (String) entry.getValue();
				} else if (entry.getKey().equals("major_n")) {
					major_n = (String) entry.getValue();
				} else if (entry.getKey().equals("major_c")) {
					major_c = (String) entry.getValue();
				}
			}
		}
		Users u = new Users();
		u.setAddress(address);
		u.setAge(Integer.valueOf(age));
		u.setCreatetime(new Date());
		u.setLastlogintime(new Date());
		u.setLevel(level);
		u.setNote(note);
		u.setOrganization_id(Integer.valueOf(organization_id));
		u.setPermissions(permissions);
		u.setPhone(Integer.valueOf(phone));
		u.setQq(qq);
		u.setSex(sex);
		u.setStatus(Integer.valueOf(status));
		u.setWechat(wechat);
		u.setUsertype(usertype);
		u.setUserpass(MyDES.encryptBasedDes(userpass));
		u.setUsername(username);
		u.setUsercode(usercode);
		u.setMajor_n(major_n);
		u.setMajor_c(major_c);
		u.setRole("");
		return userMapper.setInsert(u);
	}

	@Override
	public int getDictDelete(String id) {
		// List<Users> list = new ArrayList<Users>(JSONArray.parseArray(ids,
		// Users.class));
		// List<Integer> l = new ArrayList<Integer>();
		// for (Users u : list) {
		// l.add(Integer.valueOf(u.getId()));
		// }
		List<Integer> l = new ArrayList<Integer>();
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			l.add(Integer.valueOf(ids[i]));
		}
		int yh = userMapper.getFindByIdUser(l);
		return userMapper.getUserDeletes(l);
	}

	@Override
	public int getUpdate(String f) {
		JSONObject jsonObject = JSONObject.parseObject(f);
		String id = "", username = "", usercode = "", userpass = "", sex = "", age = "", phone = "", address = "",
				wechat = "", qq = "", status = "", level = "", permissions = "", note = "", organization_id = "",
				usertype = "", major_n = "", major_c = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					id = (String) entry.getValue();
				} else if (entry.getKey().equals("username")) {
					username = (String) entry.getValue();
				} else if (entry.getKey().equals("usercode")) {
					usercode = (String) entry.getValue();
				} else if (entry.getKey().equals("userpass")) {
					userpass = (String) entry.getValue();
				} else if (entry.getKey().equals("sex")) {
					sex = (String) entry.getValue();
				} else if (entry.getKey().equals("age")) {
					age = (String) entry.getValue();
				} else if (entry.getKey().equals("phone")) {
					phone = (String) entry.getValue();
				} else if (entry.getKey().equals("address")) {
					address = (String) entry.getValue();
				} else if (entry.getKey().equals("wechat")) {
					wechat = (String) entry.getValue();
				} else if (entry.getKey().equals("qq")) {
					qq = (String) entry.getValue();
				} else if (entry.getKey().equals("status")) {
					status = (String) entry.getValue();
				} else if (entry.getKey().equals("level")) {
					level = (String) entry.getValue();
				} else if (entry.getKey().equals("permissions")) {
					permissions = (String) entry.getValue();
				} else if (entry.getKey().equals("note")) {
					note = (String) entry.getValue();
				} else if (entry.getKey().equals("organization_id")) {
					organization_id = (String) entry.getValue();
				} else if (entry.getKey().equals("usertype")) {
					usertype = (String) entry.getValue();
				} else if (entry.getKey().equals("major_n")) {
					major_n = (String) entry.getValue();
				} else if (entry.getKey().equals("major_c")) {
					major_c = (String) entry.getValue();
				}
			}
		}
		// if (Tools.notEmpty(userpass)) {
		// userpass=MyDES.encryptBasedDes(userpass);
		// }
		Users u = new Users();
		u.setId(Integer.valueOf(id));
		u.setAddress(address);
		u.setAge(Integer.valueOf(age));
		u.setCreatetime(new Date());
		u.setLastlogintime(new Date());
		u.setLevel(level);
		u.setNote(note);
		u.setOrganization_id(Integer.valueOf(organization_id));
		u.setPermissions(permissions);
		u.setPhone(Integer.valueOf(phone));
		u.setQq(qq);
		u.setSex(sex);
		u.setStatus(Integer.valueOf(status));
		u.setWechat(wechat);
		u.setUsertype(usertype);
		u.setUserpass(MyDES.encryptBasedDes(userpass));
		u.setUsername(username);
		u.setMajor_n(major_n);
		u.setMajor_c(major_c);
		u.setUsercode(usercode);
		return userMapper.setUpdate(u);
	}

	@Override
	public Users findById(String id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(id));
		return userMapper.getFindById(map);
	}

	@Override
	public int execute(String userid1, String roleid, String uname) {
		// List<Integer> l = new ArrayList<Integer>();
		int sa1 = 0;
		String[] ids = userid1.split(",");
		for (int i = 0; i < ids.length; i++) {
			int yhid = userMapper.UserRole(Integer.valueOf(ids[i]));
			if (yhid > 0) {
				// 删除已经存在的对应系
				int syjcz = userMapper.UserRoleIdDelete(Integer.valueOf(ids[i]));
			}
			String js = (String) JSONObject.parseObject(roleid).get("jsid");
			// 添加用户所对应的角色
			if (Tools.notEmpty(js)) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("user_id", Integer.valueOf(Integer.valueOf(ids[i])));
				map.put("role_id", Integer.valueOf(js));
				map.put("role", uname);
				int sa = userMapper.UserRoleIdInsert(map);
				// 修改用户在显示的角色
				sa1 = userMapper.UserRoleIdUpdate(map);
			}
			// l.add(Integer.valueOf(ids[i]));
		}
		return sa1;
		// List<Users> list = new ArrayList<Users>(JSONArray.parseArray("[" +
		// userid1 + "]", Users.class));
		// if (list.size() > 0) {
		// for (Users u : list) {
		// int yhid = userMapper.UserRole(u.getId());
		// if (yhid > 0) {
		// // 删除已经存在的对应系
		// int syjcz = userMapper.UserRoleIdDelete(u.getId());
		// }
		// String js = (String) JSONObject.parseObject(roleid).get("jsid");
		// // 添加用户所对应的角色
		// if (Tools.notEmpty(js)) {
		// HashMap<String, Object> map = new HashMap<String, Object>();
		// map.put("user_id", Integer.valueOf(u.getId()));
		// map.put("role_id", Integer.valueOf(js));
		// map.put("role", uname);
		// int sa = userMapper.UserRoleIdInsert(map);
		// // 修改用户在显示的角色
		// int sa1 = userMapper.UserRoleIdUpdate(map);
		// }
		// }
		// return 1;
		// } else {
		// return 0;
		// }
	}

	@Override
	public Users findUserByUserName(String username, String encryptBasedDes) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("u", username);
		map.put("p", encryptBasedDes);
		return userMapper.getUserByNameDl(map);
	}

	@Override
	public int getUpdatepass(String id, String f) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(id));
		map.put("key", "userpass");
		map.put("value", MyDES.encryptBasedDes(f));
		return userMapper.editid(map);
	}

	// @Override
	// public Result<Users> signin(String kaptchaExpected, String name,
	// HttpServletRequest request, String username,
	// String password, String code) {
	// Result<Users> r = new Result<Users>();
	// HashMap<String, Object> map = new HashMap<String, Object>();
	// map.put("u", username);
	// String ps = MyDES.encryptBasedDes(password);
	// map.put("p", ps);
	// List<Users> l = userMapper.getUserByName(map);
	// if (l.size() > 0) {
	// for (Users s : l) {
	// SessionInfo si = new SessionInfo();
	// si.setId(Long.valueOf(s.getId()));
	// si.setUsername(s.getUsername());
	// si.setUsercode(s.getUsercode());
	// request.getSession().setAttribute(GlobalConstant.SESSION_INFO, si);
	// break;
	// }
	// r.setCode(200);
	// r.setMsg("登录成功");
	// } else {
	// r.setCode(400);
	// r.setMsg("登录失败");
	// }
	// return r;
	// }

}
