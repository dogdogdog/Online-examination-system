package com.yy.service.impl.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.sys.RoleMapper;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.sys.Role;
import com.yy.service.sys.RoleService;
import com.yy.utils.Tools;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleMapper roleMapper;

	@Override
	public int setInsert(String f) {
		JSONObject jsonObject = JSONObject.parseObject(f);
		String factory_name = "", factory_code = "", name = "", code = "", seq = "", description = "", isdefault = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("factory_name")) {
					factory_name = (String) entry.getValue();
				} else if (entry.getKey().equals("factory_code")) {
					factory_code = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("code")) {
					code = (String) entry.getValue();
				} else if (entry.getKey().equals("seq")) {
					seq = (String) entry.getValue();
				} else if (entry.getKey().equals("description")) {
					description = (String) entry.getValue();
				} else if (entry.getKey().equals("isdefault")) {
					isdefault = (String) entry.getValue();
				}
			}
		}
		Role r = new Role();
		r.setCode(code);
		r.setSeq(Integer.valueOf(seq));
		r.setDescription(description);
		r.setFactory_code(factory_code);
		r.setFactory_name(factory_name);
		r.setName(name);
		r.setIsdefault(1);
		return roleMapper.setInsert(r);
	}

	@Override
	public int getUpdate(String f) {
		JSONObject jsonObject = JSONObject.parseObject(f);
		String id = "", factory_name = "", factory_code = "", name = "", code = "", seq = "", description = "",
				isdefault = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					id = (String) entry.getValue();
				} else if (entry.getKey().equals("factory_name")) {
					factory_name = (String) entry.getValue();
				} else if (entry.getKey().equals("factory_code")) {
					factory_code = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("code")) {
					code = (String) entry.getValue();
				} else if (entry.getKey().equals("seq")) {
					seq = (String) entry.getValue();
				} else if (entry.getKey().equals("description")) {
					description = (String) entry.getValue();
				} else if (entry.getKey().equals("isdefault")) {
					isdefault = (String) entry.getValue();
				}
			}
		}
		Role r = new Role();
		r.setId(Integer.valueOf(id));
		r.setCode(code);
		r.setSeq(Integer.valueOf(seq));
		r.setDescription(description);
		r.setFactory_code(factory_code);
		r.setFactory_name(factory_name);
		r.setName(name);
		r.setIsdefault(1);
		return roleMapper.setUpdate(r);
	}

	@Override
	public Role findById(String id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(id));
		return roleMapper.getFindById(map);
	}

	@Override
	public int getDictDelete(String ids) {
		// TODO Auto-generated method stub
		// 删除用户对应信息
		int l = roleMapper.getDeleteUser(Integer.valueOf(ids));
		// 删除菜单对应信息
		int y = roleMapper.getDeleteResource(Integer.valueOf(ids));
		return roleMapper.getDelete(Integer.valueOf(ids));
	}

	@Override
	public ResultTable<Role> getLists(String page, String limit, String name) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(name)) {
			map.put("name", name);
		}
		int count = roleMapper.countByExample(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Role> l = roleMapper.getAllList(map);
		ResultTable<Role> rt = new ResultTable<Role>();
		rt.setCode(0);
		rt.setCount(count);
		rt.setMsg("加载成功");
		rt.setData(l);
		return rt;
	}

	@Override
	public String listjs(String s) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		List<Select> r = roleMapper.getRoleName(map);
		return JSON.toJSONString(r, true);
	}
}
