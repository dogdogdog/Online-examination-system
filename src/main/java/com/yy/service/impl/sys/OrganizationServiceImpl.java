package com.yy.service.impl.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.yy.dao.sys.OrganizationMapper;
import com.yy.entity.base.Result;
import com.yy.entity.base.ZtreeNode;
import com.yy.entity.sys.Organization;
import com.yy.service.sys.OrganizationService;
import com.yy.utils.Tools;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	private OrganizationMapper organizationMapper;
	private ZtreeNode o;

	// 获取树形机构
	@Override
	public List<ZtreeNode> ztree(String id, String t, String pid) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(id)) {
			map.put("pid", Integer.valueOf(id));
		} else {
			map.put("pid", 0);
		}
		List<ZtreeNode> treeList = new ArrayList<ZtreeNode>();
		List<Organization> oz = organizationMapper.getAlllist(map);
		// ZtreeNode zn = new ZtreeNode();
		// zn.setId("0");
		// zn.setName("根目录");
		// zn.setOpen(true);
		// zn.setpId("0");
		// zn.setIsParent(false);
		// treeList.add(zn);
		for (Organization o : oz) {
			ZtreeNode zn1 = new ZtreeNode();
			zn1.setId(String.valueOf(o.getId()));
			if (t.equals("yy")) {
				zn1.setName(o.getName());
			}else {
				zn1.setName(o.getName() + "[" + o.getCode() + "]");
			}
			zn1.setOpen(true);
			zn1.setpId(String.valueOf(o.getPid()));
			HashMap<String, Object> map1 = new HashMap<String, Object>();
			map1.put("pid", o.getId());
			int s = organizationMapper.getAlllistcount(map1);
			if (s == 0) {
				zn1.setIsParent(false);
			} else {
				zn1.setIsParent(true);
			}
			treeList.add(zn1);
		}
		return treeList;
	}

	@Override
	public int add(String name) {
		JSONObject jsonObject = JSONObject.parseObject(name);
		String name1 = "", code1 = "", pid = "0", seq = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("code")) {
					code1 = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name1 = (String) entry.getValue();
				} else if (entry.getKey().equals("pid")) {
					pid = (String) entry.getValue();
				} else if (entry.getKey().equals("seq")) {
					seq = (String) entry.getValue();
				}
			}
		}
		Organization o = new Organization();
		o.setCode(code1);
		o.setIcon("");
		o.setName(name1);
		o.setSeq(seq);
		o.setPid(Integer.valueOf(pid));
		return organizationMapper.setadd(o);
	}

	

	@Override
	public int del(String id) {
		// List<Integer> l = new ArrayList<Integer>();
		// String[] ids = id.split(",");
		// for (int i = 0; i < ids.length; i++) {
		// l.add(Integer.valueOf(ids[i]));
		// }
		return jsbl(id);
	}

	private int jsbl(String id) {
		List<Integer> l = new ArrayList<Integer>();
		l.clear();
		// 先查询自己节点下是否还有节点；
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("pid", Integer.valueOf(id));
		l.add(Integer.valueOf(id));
		List<Organization> oz = organizationMapper.getAlllist(map);
		if (oz.size() > 0) {
			for (Organization o : oz) {
				l.add(o.getId());
				jsbl(String.valueOf(o.getId()));
			}
		}
		return organizationMapper.getdelete(l);
	}

	@Override
	public int edit(String id) {
		JSONObject jsonObject = JSONObject.parseObject(id);
		String name1 = "", code1 = "", pid = "0", seq = "",id1="";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("code")) {
					code1 = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name1 = (String) entry.getValue();
				} else if (entry.getKey().equals("pid")) {
					pid = (String) entry.getValue();
				} else if (entry.getKey().equals("id")) {
					id1 = (String) entry.getValue();
				}else if (entry.getKey().equals("seq")) {
					seq = (String) entry.getValue();
				}
			}
		}
		Organization o = new Organization();
		o.setId(Integer.valueOf(id1));
		o.setCode(code1);
		o.setIcon("");
		o.setName(name1);
		o.setSeq(seq);
		return organizationMapper.setedit(o);
	}

	@Override
	public Organization editpage(String id) {
		return organizationMapper.getById(Integer.valueOf(id));
	}

}
