package com.yy.service.impl.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.sys.DictDataMapper;
import com.yy.dao.sys.DictMapper;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.base.ZtreeNode;
import com.yy.entity.sys.Dict;
import com.yy.entity.sys.DictData;
import com.yy.service.sys.DictService;
import com.yy.utils.Tools;

@Service
public class DictServiceImpl implements DictService {

	@Autowired
	private DictMapper dictMapper;
	@Autowired
	private DictDataMapper dictDataMapper;

	@Override
	public List<ZtreeNode> ztree(String id, String pid) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(id)) {
			map.put("pid", Integer.valueOf(id));
		} else {
			map.put("pid", 0);
		}
		List<ZtreeNode> treeList = new ArrayList<ZtreeNode>();
		List<Dict> oz = dictMapper.getAlllist(map);
		for (Dict o : oz) {
			ZtreeNode zn1 = new ZtreeNode();
			zn1.setId(String.valueOf(o.getId()));
			zn1.setName(o.getName());
			zn1.setOpen(true);
			zn1.setpId(String.valueOf(o.getPid()));
			HashMap<String, Object> map1 = new HashMap<String, Object>();
			map1.put("pid", o.getId());
			int s = dictMapper.getAlllistcount(map1);
			if (s == 0) {
				zn1.setIsParent(false);
			} else {
				zn1.setIsParent(true);
			}
			treeList.add(zn1);
		}
		return treeList;
	}

	@Override
	public int add(String form) {
		JSONObject jsonObject = JSONObject.parseObject(form);
		String pid = "", name = "", code = "0", status = "", isfixed = "0", seq = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("pid")) {
					pid = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("code")) {
					code = (String) entry.getValue();
				} else if (entry.getKey().equals("seq")) {
					seq = (String) entry.getValue();
				} else if (entry.getKey().equals("status")) {
					status = (String) entry.getValue();
				} else if (entry.getKey().equals("isfixed")) {
					isfixed = (String) entry.getValue();
				}
			}
		}
		Dict dict = new Dict();
		dict.setCode(code);
		dict.setIsfixed(Integer.valueOf(isfixed));
		dict.setName(name);
		dict.setPid(Integer.valueOf(pid));
		dict.setSeq(Integer.valueOf(seq));
		dict.setStatus(Integer.valueOf(status));
		return dictMapper.setadd(dict);
	}

	@Override
	public int del(String id) {
		return jsbl(id);
	}

	private int jsbl(String id) {
		List<Integer> l = new ArrayList<Integer>();
		l.clear();
		// 先查询自己节点下是否还有节点；
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("pid", Integer.valueOf(id));
		l.add(Integer.valueOf(id));
		List<Dict> oz = dictMapper.getAlllist(map);
		if (oz.size() > 0) {
			for (Dict o : oz) {
				l.add(o.getId());
				jsbl(String.valueOf(o.getId()));
			}
		}
		int i = dictDataMapper.getdeletep(l);
		int b = dictMapper.getdelete(l);
		return i + b;
	}

	@Override
	public Dict editpage(String id) {
		// TODO Auto-generated method stub
		return dictMapper.getById(Integer.valueOf(id));
	}

	@Override
	public int edit(String id) {
		JSONObject jsonObject = JSONObject.parseObject(id);
		String iid = "", pid = "0", name = "", code = "", status = "", isfixed = "0", seq = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("pid")) {
					pid = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("code")) {
					code = (String) entry.getValue();
				} else if (entry.getKey().equals("seq")) {
					seq = (String) entry.getValue();
				} else if (entry.getKey().equals("status")) {
					status = (String) entry.getValue();
				} else if (entry.getKey().equals("isfixed")) {
					isfixed = (String) entry.getValue();
				} else if (entry.getKey().equals("id")) {
					iid = (String) entry.getValue();
				}
			}
		}
		Dict dict = new Dict();
		dict.setId(Integer.valueOf(iid));
		dict.setCode(code);
		dict.setIsfixed(Integer.valueOf(isfixed));
		dict.setName(name);
		dict.setPid(Integer.valueOf(pid));
		dict.setSeq(Integer.valueOf(seq));
		dict.setStatus(Integer.valueOf(status));
		return dictMapper.setedit(dict);
	}

	@Override
	public ResultTable<DictData> getLists(String page, String limit, String pid) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(pid)) {
			map.put("dict_id", Integer.valueOf(pid));
		}
		int count = dictDataMapper.getAlllistcount(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<DictData> d = dictDataMapper.getAlllist(map);
		ResultTable<DictData> r = new ResultTable<DictData>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public int adddata(String form) {
		JSONObject jsonObject = JSONObject.parseObject(form);
		String dict_id = "", name = "", code = "0", note = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("pid")) {
					dict_id = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("code")) {
					code = (String) entry.getValue();
				} else if (entry.getKey().equals("note")) {
					note = (String) entry.getValue();
				}
			}
		}
		DictData dict = new DictData();
		dict.setCode(code);
		dict.setDict_id(Integer.valueOf(dict_id));
		dict.setName(name);
		dict.setNote(note);
		return dictDataMapper.setadd(dict);
	}

	@Override
	public DictData editdatapage(String id) {
		// TODO Auto-generated method stub
		return dictDataMapper.getById(Integer.valueOf(id));
	}

	@Override
	public int editdata(String id) {
		JSONObject jsonObject = JSONObject.parseObject(id);
		String iid = "", dict_id = "", name = "", code = "0", note = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					iid = (String) entry.getValue();
				} else if (entry.getKey().equals("pid")) {
					dict_id = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("code")) {
					code = (String) entry.getValue();
				} else if (entry.getKey().equals("note")) {
					note = (String) entry.getValue();
				}
			}
		}
		DictData dict = new DictData();
		dict.setId(Integer.valueOf(iid));
		dict.setCode(code);
		// dict.setDict_id(Integer.valueOf(dict_id));
		dict.setName(name);
		dict.setNote(note);
		return dictDataMapper.setedit(dict);
	}

	@Override
	public int deldata(String id) {
		// List<DictData> list = new
		// ArrayList<DictData>(JSONArray.parseArray(id, DictData.class));
		// List<Integer> l = new ArrayList<Integer>();
		// for (DictData u : list) {
		// l.add(Integer.valueOf(u.getId()));
		// }
		List<Integer> l = new ArrayList<Integer>();
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			l.add(Integer.valueOf(ids[i]));
		}
		return dictDataMapper.getdelete(l);
	}

	@Override
	public List<Select> professional(String name) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("code", name);
		return dictDataMapper.getProfessional(map);
	}
}
