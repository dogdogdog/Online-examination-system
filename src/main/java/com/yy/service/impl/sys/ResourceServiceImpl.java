package com.yy.service.impl.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.sys.ResourceMapper;
import com.yy.entity.base.Menu;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.base.ZtreeNode;
import com.yy.entity.sys.Resource;
import com.yy.service.sys.ResourceService;
import com.yy.utils.Tools;

@Service
public class ResourceServiceImpl implements ResourceService {

	@Autowired
	private ResourceMapper resourceMapper;

	@Override
	public ResultTable<Resource> getLists(String page, String limit, String id, String username) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(id)) {
			map.put("pid", Integer.valueOf(id));
		} else {
			map.put("pid", 0);
		}
		// if (Tools.notEmpty(username)) {
		// map.put("username", username);
		// }
		int count = resourceMapper.countByExample(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Resource> l = resourceMapper.getResourcesList(map);
		ResultTable<Resource> rt = new ResultTable<Resource>();
		rt.setCode(0);
		rt.setCount(count);
		if (l.size() > 0) {
			rt.setMsg("加载成功");
		} else {
			rt.setMsg("未加载到数据");
		}
		rt.setData(l);
		return rt;
	}

	@Override
	public int setInsert(String f) {
		JSONObject jsonObject = JSONObject.parseObject(f);
		String pid = "", name = "", icon = "", url = "", state = "0", resourcetype = "", bz = "", seq = "",
				belong = "false";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("sjcdid")) {
					pid = (String) entry.getValue();
				} else if (entry.getKey().equals("cdmc")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("cdtb")) {
					icon = (String) entry.getValue();
				} else if (entry.getKey().equals("cddz")) {
					url = (String) entry.getValue();
				} else if (entry.getKey().equals("cdzt")) {
					state = (String) entry.getValue();
				} else if (entry.getKey().equals("cdlx")) {
					resourcetype = (String) entry.getValue();
				} else if (entry.getKey().equals("bz")) {
					bz = (String) entry.getValue();
				} else if (entry.getKey().equals("cdsx")) {
					seq = (String) entry.getValue();
				} else if (entry.getKey().equals("belong")) {
					belong = (String) entry.getValue();// true为一级菜单
				}
			}
		}
		if (pid.equals("0")) {
			url = "";
		}
		Resource r = new Resource();
		r.setBelong(belong);
		r.setDescription(bz);
		r.setIcon(icon);
		r.setName(name);
		r.setResourcetype(Integer.valueOf(resourcetype));
		r.setUrl(url);
		r.setState(Integer.valueOf(state));
		r.setSeq(Integer.valueOf(seq));
		r.setPid(Integer.valueOf(pid));
		return resourceMapper.setinsert(r);
	}

	@Override
	public String tree(String id, String name, String pid) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("resourcetype", Integer.valueOf(0));
		List<Resource> l = resourceMapper.getResourcesList(map);
		List<ZtreeNode> z = new ArrayList<ZtreeNode>();
		ZtreeNode zn1 = new ZtreeNode();
		zn1.setId("0");
		zn1.setName("根目录");
		zn1.setOpen(true);
		zn1.setpId("0");
		zn1.setT("请注意选择的项，请勿乱点击");
		z.add(zn1);
		for (Resource rl : l) {
			ZtreeNode zn = new ZtreeNode();
			zn.setId(String.valueOf(rl.getId()));
			zn.setName(rl.getName());
			zn.setOpen(false);
			zn.setpId(String.valueOf(rl.getPid()));
			zn.setT("请注意选择的项，请勿乱点击");
			z.add(zn);
		}
		String jstostring = JSON.toJSONString(z);
		return jstostring;
	}

	@Override
	public Resource findById(String id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(id));
		return resourceMapper.getFindById(map);
	}

	@Override
	public int getUpdate(String f) {
		JSONObject jsonObject = JSONObject.parseObject(f);
		String id = "", pid = "", name = "", icon = "", url = "", state = "0", resourcetype = "", bz = "", seq = "",
				belong = "false";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("id")) {
					id = (String) entry.getValue();
				} else if (entry.getKey().equals("sjcdid")) {
					pid = (String) entry.getValue();
				} else if (entry.getKey().equals("cdmc")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("cdtb")) {
					icon = (String) entry.getValue();
				} else if (entry.getKey().equals("cddz")) {
					url = (String) entry.getValue();
				} else if (entry.getKey().equals("cdzt")) {
					state = (String) entry.getValue();
				} else if (entry.getKey().equals("cdlx")) {
					resourcetype = (String) entry.getValue();
				} else if (entry.getKey().equals("bz")) {
					bz = (String) entry.getValue();
				} else if (entry.getKey().equals("cdsx")) {
					seq = (String) entry.getValue();
				} else if (entry.getKey().equals("belong")) {
					belong = (String) entry.getValue();// true为一级菜单
				}
			}
		}
		if (pid.equals("0")) {
			url = "";
		}
		Resource r = new Resource();
		r.setId(Integer.valueOf(id));
		r.setBelong(belong);
		r.setDescription(bz);
		r.setIcon(icon);
		r.setName(name);
		r.setResourcetype(Integer.valueOf(resourcetype));
		r.setUrl(url);
		r.setState(Integer.valueOf(state));
		r.setSeq(Integer.valueOf(seq));
		r.setPid(Integer.valueOf(pid));
		return resourceMapper.setupdate(r);
	}

	/**
	 * 菜单删除未开发完毕
	 */
	@Override
	public int getDictDelete(String ids) {
		// TODO Auto-generated method stub
		int i = resourceMapper.deleteResourcesByPId(Integer.valueOf(ids));
		return resourceMapper.deleteResourcesById(Integer.valueOf(ids));
	}

	@Override
	public String list(SessionInfo sessionInfo) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("resourcetype", Integer.valueOf(0));
		List<Resource> l = resourceMapper.getResourcesList(map);
		List<Menu> menuList = new ArrayList<Menu>();
		for (Resource o : l) {
			if (o.getPid() == 0 || o.getPid().equals("")) {
				Menu m1 = new Menu();
				m1.setId(String.valueOf(o.getId()));
				m1.setP_id(String.valueOf(o.getPid()));
				m1.setTitle(o.getName());
				m1.setSpread(false);
				m1.setIcon(o.getIcon());
				m1.setUrl(o.getUrl());
				menuList.add(m1);
			}
		}
		// 为一级菜单设置子菜单，getChild是递归调用的
		for (Menu menu : menuList) {
			menu.setChildren(getChild(String.valueOf(menu.getId()), l));
		}
		return JSON.toJSONString(menuList);
	}

	/**
	 * 递归查找子菜单
	 *
	 * @param id
	 *            当前菜单id
	 * @param rootMenu
	 *            要查找的列表
	 * @return
	 */
	private List<Menu> getChild(String id, List<Resource> rootMenu) {
		// 子菜单
		List<Menu> childList = new ArrayList<>();
		for (Resource menu : rootMenu) {
			// 遍历所有节点，将父菜单id与传过来的id比较
			if (StringUtils.isNotBlank(String.valueOf(menu.getId()))) {
				if (id.equals(String.valueOf(menu.getPid()))) {
					Menu m1 = new Menu();
					m1.setId(String.valueOf(menu.getId()));
					m1.setP_id(String.valueOf(menu.getPid()));
					m1.setTitle(menu.getName());
					m1.setSpread(false);
					m1.setIcon(menu.getIcon());
					m1.setUrl(menu.getUrl());
					childList.add(m1);
				}
			}
		}
		// 递归退出条件
		if (childList.size() == 0) {
			return null;
		}
		return childList;
	}

	@Override
	public String grantquery(String jsid) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("state", 1);
		List<Resource> l = resourceMapper.getResourcesList(map);
		List<ZtreeNode> z = new ArrayList<ZtreeNode>();
		for (Resource o : l) {
			ZtreeNode zn = new ZtreeNode();
			zn.setId(String.valueOf(o.getId()));
			zn.setName(o.getName());
			zn.setOpen(false);
			if (Tools.notEmpty(jsid) && Tools.notEmpty(String.valueOf(o.getId()))) {
				HashMap<String, Object> map1 = new HashMap<String, Object>();
				map1.put("role_id", Integer.valueOf(jsid));
				map1.put("resource_id", Integer.valueOf(o.getId()));
				int d = resourceMapper.RoleMenu(map1);
				if (d > 0) {
					zn.setChecked(true);
				} else {
					zn.setChecked(false);
				}
			}
			zn.setpId(String.valueOf(o.getPid()));
			zn.setT("请注意选择的项，请勿乱点击");
			z.add(zn);
		}
		String jstostring = JSON.toJSONString(z);
		return jstostring;
	}

	@Override
	public int grantsave(String jsid, String cdid, String cdname) {
		int ss = 0;
		if (Tools.notEmpty(jsid) && Tools.notEmpty(cdid)) {
			HashMap<String, Object> map1 = new HashMap<String, Object>();
			map1.put("role_id", Integer.valueOf(jsid));
			// 查询授权信息是否存在
			int rid = resourceMapper.RoleMenuID(map1);
			if (rid > 0) {
				// 大于0表示存在，删除当前已有的信息
				int s1 = resourceMapper.RoleMenuIdDelete(Integer.valueOf(jsid));
			}
			String[] str = cdid.split(",");
			for (int i = 0; i < str.length; i++) {
				// 新增角色和菜单的对应关系
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("role_id", Integer.valueOf(jsid));
				map.put("resource_id", Integer.valueOf(str[i]));
				int l = resourceMapper.RoleMenuIdInsert(map);
			}
			if (Tools.notEmpty(cdname)) {
				HashMap<String, Object> map2 = new HashMap<String, Object>();
				map2.put("id", Integer.valueOf(jsid));
				map2.put("description", cdname);
				ss = resourceMapper.RoleMenuIdUpdate(map2);
			}
		}
		return ss;
	}

	@Override
	public List<Resource> shiroresource(Integer organization_id) {
		List<Resource> list1 = resourceMapper.getShiroResource(organization_id);
		return list1;
	}

	@Override
	public List<Resource> shiroresourceall() {
		HashMap<String, Object> map2 = new HashMap<String, Object>();
		List<Resource> list1 = resourceMapper.getShiroResourceall(map2);
		return list1;
	}
}
