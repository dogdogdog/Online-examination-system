package com.yy.service.impl.sys;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.sys.NoticeMapper;
import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Notice;
import com.yy.service.sys.NoticeService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.Tools;

@Service
public class NoticeServiceImpl implements NoticeService {

	@Autowired
	private NoticeMapper noticeMapper;

	@Override
	public ResultTable<Notice> getAll(String page, String limit, String f, HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("type")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("type", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("information")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("information", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("createtime")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							String c = (String) entry.getValue();
							String[] aa = c.split(" - ");
							map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
							map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
						}
					}
				}

			}
		}
		int count = noticeMapper.count(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Notice> d = noticeMapper.getAllList(map);
		ResultTable<Notice> r = new ResultTable<Notice>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public int add(String form, String fbnr) {
		JSONObject jsonObject = JSONObject.parseObject(form);
		String type = "", type_name = "", identification = "0", other = "", information = "",subject="";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("type")) {
					type = (String) entry.getValue();
				} else if (entry.getKey().equals("type_name")) {
					type_name = (String) entry.getValue();
				} else if (entry.getKey().equals("identification")) {
					identification = (String) entry.getValue();
				} else if (entry.getKey().equals("other")) {
					other = (String) entry.getValue();
				} else if (entry.getKey().equals("information")) {
					information = (String) entry.getValue();
				} else if (entry.getKey().equals("subject")) {
					subject = (String) entry.getValue();
				}
			}
		}
		Notice nc = new Notice();
		nc.setIdentification(identification);
		nc.setOther(other);
		nc.setCreatetime(new Date());
		nc.setType(type);
		nc.setType_name(type_name);
		nc.setInformation(fbnr);
		nc.setSubject(subject);
		if (identification.equals("1")) {
			nc.setIctime(new Date());
		}
		return noticeMapper.add(nc);
	}

	@Override
	public int codeedit(String id, String name, String code) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.valueOf(id));
		map.put("key", name);
		map.put("value", code);
		if(name.equals("identification")){
			map.put("times", new Date());
		}
		return noticeMapper.codeedit(map);
	}

	@Override
	public int del(String id) {
		List<Integer> l = new ArrayList<Integer>();
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			l.add(Integer.valueOf(ids[i]));
		}
		return noticeMapper.getdelete(l);
	}

	@Override
	public ResultTable<Notice> getAllGg(String code, HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		//if(code.equals("QBGG"))//全部消息
		map.put("start_ex1", DateUtil.StringToDate(DateTime.now().minusDays(10).toString("yyyy-MM-dd HH:mm:ss"), DateStyle.YYYY_MM_DD_HH_MM_SS));//获取7天前的日期
		map.put("end_ex1",new Date());
		map.put("identification","1");
		List<Notice> d = noticeMapper.getAllList(map);
		ResultTable<Notice> r = new ResultTable<Notice>();
		r.setCode(0);
		r.setCount(1);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public Notice getAllXXJL(String id) {
		return noticeMapper.getById(Integer.valueOf(id));
	}

	
}
