package com.yy.service.impl.study;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.study.Study_learningMapper;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.study.Study_learning;
import com.yy.service.study.Study_learningService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.GlobalConstant;
import com.yy.utils.Tools;

@Service
public class Study_learningServiceImpl implements Study_learningService {

	@Autowired
	private Study_learningMapper study_learningMapper;

	@Override
	public ResultTable<Study_learning> getAll(String page, String limit, String f, HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("type")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("type", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("createtime")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							String c = (String) entry.getValue();
							String[] aa = c.split(" - ");
							map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
							map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
						}
					} else if (entry.getKey().equals("major_code")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("major_code", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("name")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("name", (String) entry.getValue());
						}
					}
				}

			}
		}
		int count = study_learningMapper.count(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Study_learning> d = study_learningMapper.getAllList(map);
		ResultTable<Study_learning> r = new ResultTable<Study_learning>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public int setInsert(String f, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		JSONObject jsonObject = JSONObject.parseObject(f);
		String major_code = "", major_name = "", name = "", type = "", route = "", desc = "", note = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("major_code")) {
					major_code = (String) entry.getValue();
				} else if (entry.getKey().equals("major_name")) {
					major_name = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("type")) {
					type = (String) entry.getValue();
				} else if (entry.getKey().equals("route")) {
					route = (String) entry.getValue();
				} else if (entry.getKey().equals("desc")) {
					desc = (String) entry.getValue();
				} else if (entry.getKey().equals("note")) {
					note = (String) entry.getValue();
				}
			}
		}
		Study_learning v = new Study_learning();
		v.setBrief(desc);
		v.setCreatetime(new Date());
		v.setMajor_code(major_code);
		v.setMajor_name(major_name);
		v.setName(name);
		v.setNote(note);
		v.setRoute(route);
		v.setType(type);
		v.setUsercode(sessionInfo.getUsercode());
		v.setUsername(sessionInfo.getUsername());
		return study_learningMapper.add(v);
	}

	@Override
	public int dels(String id, HttpServletRequest request) {
		List<Integer> l = new ArrayList<Integer>();
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			l.add(Integer.valueOf(ids[i]));
		}
		List<Study_learning> sv = study_learningMapper.getAllListpl(l);
		for (Study_learning o : sv) {
			// 删除相应文件
			try {
				// Resource resource = new
				// ClassPathResource("config/system.xml");
				// File filer = resource.getFile();
				// SAXReader reader = new SAXReader();
				// Document doc = reader.read(filer); // 加载xml文件
				// // List list = doc.selectNodes("//learning/picture");
				// Node node = doc.selectSingleNode("//learning/video");
				// String ps = node.getStringValue();
				// Node node1 = doc.selectSingleNode("//learning/picture");
				// String ps1 = node1.getStringValue();
				// File file = new File(ps + "\\" + o.getRoute());
				// file.delete();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		// int s = video_CommentsMapper.getdelete(l);
		return study_learningMapper.getdelete(l);
	}

	@Override
	public int setUpdate(String f, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		JSONObject jsonObject = JSONObject.parseObject(f);
		String major_code = "", major_name = "", name = "", type = "", route = "", desc = "", note = "", id = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("major_code")) {
					major_code = (String) entry.getValue();
				} else if (entry.getKey().equals("major_name")) {
					major_name = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("type")) {
					type = (String) entry.getValue();
				} else if (entry.getKey().equals("route")) {
					route = (String) entry.getValue();
				} else if (entry.getKey().equals("desc")) {
					desc = (String) entry.getValue();
				} else if (entry.getKey().equals("note")) {
					note = (String) entry.getValue();
				} else if (entry.getKey().equals("id")) {
					id = (String) entry.getValue();
				}
			}
		}
		Study_learning v = new Study_learning();
		v.setId(Integer.valueOf(id));
		v.setBrief(desc);
		v.setCreatetime(new Date());
		v.setMajor_code(major_code);
		v.setMajor_name(major_name);
		v.setName(name);
		v.setNote(note);
		v.setRoute(route);
		v.setType(type);
		v.setUsercode(sessionInfo.getUsercode());
		v.setUsername(sessionInfo.getUsername());
		return study_learningMapper.Update(v);
	}

	@Override
	public Study_learning editpage(String id) {
		// TODO Auto-generated method stub
		return study_learningMapper.getById(Integer.valueOf(id));
	}
}
