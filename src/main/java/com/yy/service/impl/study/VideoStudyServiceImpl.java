package com.yy.service.impl.study;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yy.dao.study.Study_VideoMapper;
import com.yy.dao.study.Video_CommentsMapper;
import com.yy.entity.base.ResultTable;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.study.Study_Video;
import com.yy.entity.study.Video_Comments;
import com.yy.service.study.VideoStudyService;
import com.yy.style.DateStyle;
import com.yy.utils.DateUtil;
import com.yy.utils.GlobalConstant;
import com.yy.utils.Tools;

@Service
public class VideoStudyServiceImpl implements VideoStudyService {

	@Autowired
	private Study_VideoMapper study_VideoMapper;
	@Autowired
	private Video_CommentsMapper video_CommentsMapper;

	@Override
	public ResultTable<Study_Video> getAll(String page, String limit, String f, HttpServletRequest request) {
		// String name = "", major_code = "", createtime = "", label = "", type
		// = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("type")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("type", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("label")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("label", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("createtime")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							String c = (String) entry.getValue();
							String[] aa = c.split(" - ");
							map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
							map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
						}
					} else if (entry.getKey().equals("major_code")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("major_code", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("name")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("name", (String) entry.getValue());
						}
					}
				}

			}
		}
		int count = study_VideoMapper.count(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Study_Video> d = study_VideoMapper.getAllList(map);
		ResultTable<Study_Video> r = new ResultTable<Study_Video>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public int setInsert(String f, HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		JSONObject jsonObject = JSONObject.parseObject(f);
		String major_code = "", major_name = "", name = "", type = "", label = "", route = "", desc = "", picture = "";
		for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
			if (Tools.notEmpty((String) entry.getValue())) {
				if (entry.getKey().equals("major_code")) {
					major_code = (String) entry.getValue();
				} else if (entry.getKey().equals("major_name")) {
					major_name = (String) entry.getValue();
				} else if (entry.getKey().equals("name")) {
					name = (String) entry.getValue();
				} else if (entry.getKey().equals("type")) {
					type = (String) entry.getValue();
				} else if (entry.getKey().equals("label")) {
					label = (String) entry.getValue();
				} else if (entry.getKey().equals("route")) {
					route = (String) entry.getValue();
				} else if (entry.getKey().equals("desc")) {
					desc = (String) entry.getValue();
				} else if (entry.getKey().equals("picture")) {
					picture = (String) entry.getValue();
				}
			}
		}
		Study_Video v = new Study_Video();
		v.setBrief(desc);
		v.setCreatetime(new Date());
		v.setLabel(label);
		v.setMajor_code(major_code);
		v.setMajor_name(major_name);
		v.setName(name);
		v.setPicture(picture);
		v.setRoute(route);
		v.setType(type);
		v.setUsercode(sessionInfo.getUsercode());
		v.setUsername(sessionInfo.getUsername());
		return study_VideoMapper.add(v);
	}

	@Override
	public Study_Video playquery(String id) {
		return study_VideoMapper.getById(Integer.valueOf(id));
	}

	@SuppressWarnings("unused")
	@Override
	public int dels(String id, HttpServletRequest request) {
		List<Integer> l = new ArrayList<Integer>();
		String[] ids = id.split(",");
		for (int i = 0; i < ids.length; i++) {
			l.add(Integer.valueOf(ids[i]));
		}
		List<Study_Video> sv = study_VideoMapper.getAllListpl(l);
		for (Study_Video o : sv) {
			// 删除相应文件
			try {
				Resource resource = new ClassPathResource("config/system.xml");
				File filer = resource.getFile();
				SAXReader reader = new SAXReader();
				Document doc = reader.read(filer); // 加载xml文件
				// List list = doc.selectNodes("//learning/picture");
				Node node = doc.selectSingleNode("//learning/video");
				String ps = node.getStringValue();
				Node node1 = doc.selectSingleNode("//learning/picture");
				String ps1 = node1.getStringValue();
				File file = new File(ps + "\\" + o.getRoute());
				file.delete();
				File file1 = new File(ps1 + "\\" + o.getPicture());
				file1.delete();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		int s = video_CommentsMapper.getdelete(l);
		return study_VideoMapper.getdelete(l);
	}

	@Override
	public ResultTable<Study_Video> getAllcount(String f, HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(f)) {
			JSONObject jsonObject = JSONObject.parseObject(f);
			for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
				if (Tools.notEmpty((String) entry.getValue())) {
					if (entry.getKey().equals("type")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("type", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("label")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("label", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("createtime")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							String c = (String) entry.getValue();
							String[] aa = c.split(" - ");
							map.put("start_ex", DateUtil.StringToDate(aa[0], DateStyle.YYYY_MM_DD_HH_MM_SS));
							map.put("end_ex", DateUtil.StringToDate(aa[1], DateStyle.YYYY_MM_DD_HH_MM_SS));
						}
					} else if (entry.getKey().equals("major_code")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("major_code", (String) entry.getValue());
						}
					} else if (entry.getKey().equals("name")) {
						if (Tools.notEmpty((String) entry.getValue())) {
							map.put("name", (String) entry.getValue());
						}
					}
				}

			}
		}
		int count = study_VideoMapper.count(map);
		ResultTable<Study_Video> r = new ResultTable<Study_Video>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		return r;
	}

	@Override
	public ResultTable<Study_Video> getSortAll(String page, String limit, String major_code, String type, String label,
			String createtime, HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (Tools.notEmpty(type)) {
			map.put("type", type);
		}
		if (Tools.notEmpty(major_code)) {
			map.put("major_code", major_code);
		}
		if (Tools.notEmpty(label)) {
			map.put("label", label);
		}
		if (Tools.notEmpty(createtime)) {
			if (createtime.equals("zx")) {
				map.put("sj", "");
			}
		}
		int count = study_VideoMapper.count(map);
		PageHelper.startPage(Integer.valueOf(page), Integer.valueOf(limit));
		List<Study_Video> d = study_VideoMapper.getAllList(map);
		ResultTable<Study_Video> r = new ResultTable<Study_Video>();
		r.setCode(0);
		r.setCount(count);
		r.setMsg("加载成功");
		r.setData(d);
		return r;
	}

	@Override
	public int setInsertpl(String comments, String id, HttpServletRequest request) {
		// TODO Auto-generated method stub
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO);
		Video_Comments vc = new Video_Comments();
		vc.setVideo_id(Integer.valueOf(id));
		vc.setComments(comments);
		vc.setUsername(sessionInfo.getUsername());
		vc.setUsercode(sessionInfo.getUsercode());
		vc.setDate(new Date());
		return video_CommentsMapper.add(vc);
	}

	@Override
	public ResultTable<Video_Comments> getPlAllId(String id, HttpServletRequest request) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("video_id", Integer.valueOf(id));
		List<Video_Comments> vc = video_CommentsMapper.getAllList(map);
		ResultTable<Video_Comments> r = new ResultTable<Video_Comments>();
		r.setCode(0);
		r.setCount(1);
		r.setMsg("加载成功");
		r.setData(vc);
		return r;
	}
}
