package com.yy.service.sys;


import javax.servlet.http.HttpServletRequest;

import com.yy.entity.base.Result;
import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Resource;
import com.yy.entity.sys.Users;


public interface UserService {

	Result<Users> signin(String kaptchaExpected, String name, HttpServletRequest request);

	ResultTable<Users> getLists(String page, String limit, String id, String username);

	int setInsert(String f);

	int getDictDelete(String ids);

	int getUpdate(String f);

	Users findById(String id);

	int execute(String userid1, String roleid, String uname);

	Users findUserByUserName(String username, String encryptBasedDes);

	int getUpdatepass(String id, String f);


//	Result<Users> signin(String kaptchaExpected, String name, HttpServletRequest request, String username,
//			String password, String code);




}
