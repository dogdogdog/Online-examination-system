package com.yy.service.sys;

import javax.servlet.http.HttpServletRequest;

import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Notice;

public interface NoticeService {

	ResultTable<Notice> getAll(String page, String limit, String f, HttpServletRequest request);

	int add(String form, String fbnr);

	int codeedit(String id, String name, String code);

	int del(String id);

	ResultTable<Notice> getAllGg(String code, HttpServletRequest request);

	Notice getAllXXJL(String id);


}
