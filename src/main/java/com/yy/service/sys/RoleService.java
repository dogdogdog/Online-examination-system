package com.yy.service.sys;

import com.yy.entity.base.ResultTable;
import com.yy.entity.sys.Role;

public interface RoleService {

	int setInsert(String f);

	int getUpdate(String f);

	Role findById(String id);

	int getDictDelete(String ids);

	ResultTable<Role> getLists(String page, String limit, String name);

	String listjs(String s);

}
