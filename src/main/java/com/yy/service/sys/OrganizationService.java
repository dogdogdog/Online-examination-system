package com.yy.service.sys;

import java.util.List;

import com.yy.entity.base.Result;
import com.yy.entity.base.ZtreeNode;
import com.yy.entity.sys.Organization;

public interface OrganizationService {

	List<ZtreeNode> ztree(String id, String t, String pid);

	int add(String name);

	int del(String id);

	int edit(String id);

	Organization editpage(String id);

}
