package com.yy.service.sys;

import java.util.List;

import com.yy.entity.base.ResultTable;
import com.yy.entity.base.SessionInfo;
import com.yy.entity.sys.Resource;

public interface ResourceService {

	ResultTable<Resource> getLists(String page, String limit, String id, String username);

	int setInsert(String f);

	String tree(String id, String name, String pid);

	Resource findById(String id);

	int getUpdate(String f);

	int getDictDelete(String ids);

	String list(SessionInfo sessionInfo);

	String grantquery(String jsid);

	int grantsave(String jsid, String cdid, String cdname);

	List<Resource> shiroresource(Integer organization_id);

	List<Resource> shiroresourceall();

}
