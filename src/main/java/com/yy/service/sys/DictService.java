package com.yy.service.sys;

import java.util.List;

import com.yy.entity.base.ResultTable;
import com.yy.entity.base.Select;
import com.yy.entity.base.ZtreeNode;
import com.yy.entity.sys.Dict;
import com.yy.entity.sys.DictData;

public interface DictService {

	List<ZtreeNode> ztree(String id, String pid);

	int add(String form);

	int del(String id);

	Dict editpage(String id);

	int edit(String id);

	ResultTable<DictData> getLists(String page, String limit, String pid);

	int adddata(String form);

	DictData editdatapage(String id);

	int editdata(String id);

	int deldata(String id);

	List<Select> professional(String name);


}
