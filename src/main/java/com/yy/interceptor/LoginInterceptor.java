package com.yy.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
* 类描述：    自定义拦截器（登录拦截）
* 修改人：liyingying  HandlerInterceptorAdapter
* 修改时间：2017年8月14日 下午7:40:01   
* 修改备注：   
* @version  V1.0
 */
public class LoginInterceptor implements HandlerInterceptor {
	private Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

	// @Override
	// public boolean preHandle(HttpServletRequest request, HttpServletResponse
	// response, Object handler)
	// throws Exception {
	// // String authorization = request.getHeader("Authorization");
	// // System.out.println(authorization);
	// // logger.info("The authorization is: {}", authorization);
	// Object obj = request.getSession().getAttribute("cur_user");
	// if (obj == null || !(obj instanceof Info)) {
	// // response.sendRedirect(request.getContextPath() + "/login");
	// System.out.println("进入当前session的值是：" + 11);
	// } else {
	// System.out.println("当前session有值哦");
	// }
	// String s = (String) request.getSession().getAttribute("cur_user");
	// System.out.println("进入当前session的值是：" + s);
	//
	// String ip = request.getRemoteAddr();
	// String url = request.getRequestURL().toString();
	// String method = request.getMethod();
	// String uri = request.getRequestURI();
	// String queryString = request.getQueryString();
	//
	// // request.authenticate(arg0)
	// // System.out.println(request.getParameterMap());
	// logger.info(String.format("请求参数, ip: %s,url: %s, method: %s, uri: %s,
	// params: %s", ip, url, method, uri,
	// queryString));
	// return super.preHandle(request, response, handler);
	// }
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
//		String s = "";
//		try {
//			Object o = request.getSession().getAttribute("cur_user");
//			s = String.valueOf(o);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		if (s.equals("") || s == null || s.equals("null")) {
//			System.out.println("当前session没有值哦");
//		} else {
//			System.out.println("进入当前session的值是12：" + s);
//		}

		String ip = request.getRemoteAddr();
		String url = request.getRequestURL().toString();
		String method = request.getMethod();
		String uri = request.getRequestURI();
		String queryString = request.getQueryString();

		// request.authenticate(arg0)
		// System.out.println(request.getParameterMap());
		logger.info(String.format("请求参数, ip: %s,url: %s, method: %s, uri: %s, params: %s", ip, url, method, uri,
				queryString));
//		System.out.println(String.format("请求参数, ip: %s,url: %s, method: %s, uri: %s, params: %s", ip, url, method, uri,
//				queryString));
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
