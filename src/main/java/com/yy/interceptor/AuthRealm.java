package com.yy.interceptor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.yy.entity.sys.Resource;
import com.yy.entity.sys.Users;
import com.yy.handle.ResultEnum;
import com.yy.handle.YyException;
import com.yy.service.sys.ResourceService;
import com.yy.service.sys.UserService;
import com.yy.utils.MyDES;

public class AuthRealm extends AuthorizingRealm {
	@Autowired
	private UserService userService;
	@Autowired
	private ResourceService resourceService;

	// 授权
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
		Users user = (Users) principal.fromRealm(this.getClass().getName()).iterator().next();// 获取session中的用户
		// System.out.println("权限认证方法：MyShiroRealm.doGetAuthorizationInfo()");
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		Set<String> roles = new HashSet<String>();
		roles.add("role");
		info.setRoles(roles);
		Set<String> permissions = new HashSet<String>();
		// 在这里查询有哪些菜单权限
		// System.out.println(user.getUsercode());
		if (user.getUsercode().equals("yy") && user.getUserpass().equals("yy")) {
			List<Resource> r = resourceService.shiroresourceall();
			for (Resource o : r) {
				permissions.add(o.getUrl());
			}
			info.setStringPermissions(permissions);// 将权限放入shiro中.
		} else {
			List<Resource> r = resourceService.shiroresource(user.getOrganization_id());
			for (Resource o : r) {
				System.out.println(o.getUrl());
				permissions.add(o.getUrl());
			}
			info.setStringPermissions(permissions);// 将权限放入shiro中.
		}
		return info;
	}

	// 认证.登录
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken utoken = (UsernamePasswordToken) token;// 获取用户输入的token
		String username = utoken.getUsername();
		String password = String.valueOf(utoken.getPassword());
		Users user = null;
		if (username.equals("yy") && password.equals("yy")) {
			user = new Users();
			user.setUsercode("yy");
			user.setUsername("超级管理员");
			user.setUserpass("yy");
		} else {
			user = userService.findUserByUserName(username, MyDES.encryptBasedDes(password));
			if (user == null) {
				// throw new AccountException("帐号或密码不正确！");
				throw new YyException(ResultEnum.userpwd_not_exist);
			}
			Logger.getLogger(getClass()).info("身份认证成功，登录用户：" + username);
		}
		return new SimpleAuthenticationInfo(user, password, this.getClass().getName());// 放入shiro.调用CredentialsMatcher检验密码
	}

}
